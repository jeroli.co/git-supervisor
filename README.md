<div align="center" style="font-size:20px;">

<img src="git-supervisor-app/src/assets/images/logos/logo3.png"  width="40%" height="40%">

# Git-Supervisor

</div>
Many teachers evaluate their students works through the git activity of their projects (issues quality, commits consistency, efficient use of branches, etc.). This manual task, is very time-consuming and unpleasing.The goal of Git-Supervisor is to allow a simple, concise and automated view of the activities of a git repository.

[Deployed Application](https://git-supervisor.herokuapp.com)

## Features

* Create workspaces where you can gather several repositories
* Access informations about repositories activities
* Assign content modules to each workspace

## Usage

![Example](./git-supervisor-app/src/assets/images/readme_example.gif)

## Private Repositories / Organisation
If you have any problem to find a private GitHub repository, you have to follow the steps :
* Go to your GitHub account, sign in, click to your account on the top right & click on settings.
* Go to Applications, Authorized OAuth App & check if your permission is 

![Example](./git-supervisor-app/src/assets/screens/permissions.png)
  
* If not, please click on Revoke access, the next time you will use git supervisor, the application will provide you a token with the right scope.

⚠️ If you want to access to a private repository owned by an organisation, and you are the first to use Git Supervisor with it, you will have to request the organisation.
![Example](./git-supervisor-app/src/assets/screens/requests.png)  
That will send a request to the organisation's owner, once he's approved, you will be able to analyse your private repositories and the ones you are a member of.

## Contributors

[Wiki](https://gitlab.com/jeroli.co/git-supervisor/-/wikis/%5BDEV%5D-Getting-Started) *Get started on the project*

[Contributing](https://gitlab.com/jeroli.co/git-supervisor/-/blob/master/CONTRIBUTING.md)

## The Team

****Students at the University of Toulouse III Paul Sabatier :****

Management team :

* Corentin Boget (@BogetC)
* Mika Pons (@mikaponsprive)
* Imed (@)

Development Team :

* Louis Ayroles (@ImJustLouis)
* Damien Chapeau (@damien.chapeau)
* Florent Danna (@Waslide)


