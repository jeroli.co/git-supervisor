// MODULE
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ChartsModule } from 'ng2-charts';

// SERVICES
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { AuthService } from './authentication/services/auth.service';
import { AuthInterceptorService } from './authentication/services/auth-interceptor.service';

// DIRECTIVES

import { ImportDirective } from './dashboard/workspace-settings/import/directives/import.directive';

// PIPES
import { WorkspaceFilterPipe } from './dashboard/pipes/workspace-filter/workspace-filter.pipe';

// COMPONENTS
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './authentication/login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WorkspaceComponent } from './dashboard/workspace/workspace.component';
import { WorkspaceSettingsComponent } from './dashboard/workspace-settings/workspace-settings.component';
import { RepositoryComponent } from './dashboard/repository/repository.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRouterOutletComponent } from './dashboard/dashboard-router-outlet.component';
import { AddRepositoryComponent } from './dashboard/workspace/add-repository/add-repository.component';
import { NotificationsStackComponent } from './notifications/notifications-stack/notifications-stack.component';
import { InspectorModulesComponent } from './inspector-modules/inspector-modules.component';
import { BreadcrumbComponent } from './dashboard/breadcrumb/breadcrumb.component';
import { DeleteRepositoryModalComponent } from './dashboard/repository/delete-repository-modal/delete-repository-modal.component';
import { DeleteWorkspaceModalComponent } from './dashboard/workspace/delete-workspace-modal/delete-workspace-modal.component';
import { BasicModuleComponent } from './inspector-modules/modules/basic-module/basic-module.component';
import { ContributorsModuleComponent } from './inspector-modules/modules/contributors-module/contributors-module.component';
import { CommitsModalComponent } from './inspector-modules/modules/contributors-module/commits-modal/commits-modal.component';
import { InspectorModuleConfigComponent } from './inspector-modules/inspector-module-config/inspector-module-config.component';
import { CalendarDateFilterComponent } from './inspector-modules/modules-filters/calendar-date-filter/calendar-date-filter.component';
import { RepositoryItemAdderComponent } from './dashboard/workspace/add-repository/repository-item-adder/repository-item-adder.component';
import { ManageModuleViewModalComponent } from './dashboard/repository/manage-module-view-modal/manage-module-view-modal.component';
import { ContributorsChartComponent } from './inspector-modules/modules/contributors-module/contributors-chart/contributors-chart.component';
import { ContributorChartComponent } from './inspector-modules/modules/contributors-module/contributor-chart/contributor-chart.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { EvaluatorModuleComponent } from './inspector-modules/modules/evaluator-module/evaluator-module.component';
import { EvaluatorModuleConfigComponent } from './inspector-modules/inspector-module-config/evaluator-module-config/evaluator-module-config.component';
import { AddEvaluationCriteriaComponent } from './inspector-modules/inspector-module-config/evaluator-module-config/add-evaluation-criteria/add-evaluation-criteria.component';
import { AddModulesModalComponent } from './dashboard/workspace-settings/add-modules-modal/add-modules-modal.component';
import { ModulesFiltersComponent } from './inspector-modules/modules-filters/modules-filters.component';
import { EvaluatorDataModalComponent } from './inspector-modules/modules/evaluator-module/evaluator-data-modal/evaluator-data-modal.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { ExportToHtmlModalComponent } from './dashboard/repository/export-to-html-modal/export-to-html-modal.component';
import { ImportComponent } from './dashboard/workspace-settings/import/import.component';
import { ImportModalComponent } from './dashboard/workspace-settings/import/import-modal/import-modal.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavbarComponent,
        WorkspaceFilterPipe,
        LoginComponent,
        PageNotFoundComponent,
        WorkspaceComponent,
        WorkspaceSettingsComponent,
        RepositoryComponent,
        DashboardComponent,
        DashboardRouterOutletComponent,
        AddRepositoryComponent,
        NotificationsStackComponent,
        BreadcrumbComponent,
        DeleteRepositoryModalComponent,
        DeleteWorkspaceModalComponent,
        InspectorModulesComponent,
        BasicModuleComponent,
        ContributorsModuleComponent,
        CommitsModalComponent,
        InspectorModuleConfigComponent,
        CalendarDateFilterComponent,
        RepositoryItemAdderComponent,
        ContributorsChartComponent,
        ContributorChartComponent,
        ManageModuleViewModalComponent,
        TruncatePipe,
        EvaluatorModuleComponent,
        EvaluatorModuleConfigComponent,
        AddEvaluationCriteriaComponent,
        AddModulesModalComponent,
        ModulesFiltersComponent,
        EvaluatorDataModalComponent,
        TutorialComponent,
        ExportToHtmlModalComponent,
        ImportComponent,
        ImportDirective,
        ImportModalComponent,
    ],
    imports: [
        BrowserModule,
        ChartsModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        PDFExportModule,
    ],
    providers: [
        AuthGuardService,
        AuthService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
        ContributorsChartComponent,
        ContributorChartComponent,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
