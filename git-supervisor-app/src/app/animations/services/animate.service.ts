import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class AnimateService {
    animateCSS(element: string, animationName: string, callback: () => void): void {
        const node = document.querySelector(element);
        node.classList.add('animated', animationName);

        function handleAnimationEnd() {
            node.classList.remove('animated', animationName);
            node.removeEventListener('animationend', handleAnimationEnd);

            if (typeof callback === 'function') callback();
        }

        node.addEventListener('animationend', handleAnimationEnd);
    }
    animateCSS2(element: string, animation: string, prefix = 'animate__'): Promise<unknown> {
        // We create a Promise and return it
        return new Promise((resolve, reject) => {
            const animationName = `${prefix}${animation}`;
            const node = document.querySelector(element);

            node.classList.add(`${prefix}animated`, animationName);

            // When the animation ends, we clean the classes and resolve the Promise
            function handleAnimationEnd(event) {
                event.stopPropagation();
                node.classList.remove(`${prefix}animated`, animationName);
                resolve('Animation ended');
            }

            node.addEventListener('animationend', handleAnimationEnd, { once: true });
        });
    }
}
