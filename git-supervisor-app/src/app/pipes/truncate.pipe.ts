import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate',
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, args?: number): string {
    const limit = args ? args[0] : 40;
    const trail = '...';
    return value.length > limit ? value.substr(0, limit) + trail : value;
  }
}
