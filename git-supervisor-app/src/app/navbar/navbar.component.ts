import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authentication/services/auth.service';
import { environment } from '../../environments/environment';
import {VCSClient} from "../../enums/VCSClient";

const apiUrl = environment.api_addr;

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
    public apiDocUrl: string = apiUrl + '/apidocs';
    public gitlabUrl = 'https://gitlab.com/jeroli.co/git-supervisor';
    public vcsClients = { ...VCSClient };

    constructor(private authService: AuthService) {}

    ngOnInit(): void {
        const burger: HTMLElement = document.querySelector('.burger');
        const nav = document.querySelector('#' + burger.dataset.target);

        burger.addEventListener('click', function () {
            burger.classList.toggle('is-active');
            nav.classList.toggle('is-active');
        });
    }

    public isAuthenticated(): boolean {
        return this.authService.isAuthenticated();
    }

    public logout(): void {
        this.authService.logout();
    }
}
