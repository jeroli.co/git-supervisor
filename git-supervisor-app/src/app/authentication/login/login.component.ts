import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { VCSClient } from "../../../enums/VCSClient";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

    constructor(private authService: AuthService, private route: ActivatedRoute) {}


    ngOnInit(): void {
        const code = this.route.snapshot.queryParamMap.get('code');
        const state = this.route.snapshot.queryParamMap.get('state');

        if (code && state) {
          switch(this.authService.getVCS()){
            case VCSClient.GITLAB: {
              this.authService.loginCallbackGitlab(code, state);
              break;
            }

            case VCSClient.GITHUB: {
              this.authService.loginCallbackGithub(code, state);
              break;
            }
          }
        }
    }

    public loginToGitlabAccount(): void {
        this.authService.loginToGitlabAccount();
    }

    public loginToGithubAccount(): void {
        this.authService.loginToGithubAccount();
    }

    public isAuthenticated(): boolean {
        return this.authService.isAuthenticated();
    }
}
