import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        let interceptedReq: HttpRequest<unknown> = req;

        if (this.authService.isAuthenticated()) {
            const token = this.authService.getToken();
            interceptedReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + token),
            });
        }

        return next.handle(interceptedReq).pipe(
            map((event: HttpEvent<unknown>) => {
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (this.authService.isAuthenticated() && (error.status === 401 || error.status === 403)) {
                    this.authService.sessionExpired();
                }

                return throwError(error);
            })
        );
    }
}
