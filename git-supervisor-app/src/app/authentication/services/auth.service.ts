import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { NotificationsService } from '../../notifications/services/notifications.service';
import { VCSClient } from '../../../enums/VCSClient';
import { WorkspaceModel } from '../../dashboard/workspace/models/workspace.model';

const apiUrl: string = environment.api_addr;

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(private http: HttpClient, private router: Router, private notificationsService: NotificationsService) {}

    public loginToGitlabAccount(): void {
        this.loginToAnAccount('/gitlab_login', VCSClient.GITLAB);
    }

    public loginToGithubAccount(): void {
        this.loginToAnAccount('/github_login', VCSClient.GITHUB);
    }

    private loginToAnAccount(routeLogin: string, vcs: string): void {
        this.http.get<string>(apiUrl + routeLogin).subscribe((res) => {
            AuthService.setVCS(vcs);
            window.location.href = res;
            return;
        });
    }

    public loginCallbackGitlab(code: string, state: string): void {
        this.loginCallback(code, state, '/gitlab_callback');
    }

    public loginCallbackGithub(code: string, state: string): void {
        this.loginCallback(code, state, '/github_callback');
    }

    private loginCallback(code: string, state: string, routeCallBack: string): void {
        const params = new HttpParams({ fromObject: { code: code, state: state } });
        this.http
            .get<{ token: string }>(apiUrl + routeCallBack, { params: params })
            .subscribe((res) => {
                AuthService.setToken(res.token);
                const id_workspace = this.getWorkspace();
                AuthService.deleteWorkspace();
                if (id_workspace != null) {
                    this.router.navigate(['/dashboard/workspace', id_workspace]);
                } else {
                    this.router.navigate(['/dashboard']);
                }
            });
    }

    public logout(): void {
        AuthService.deleteToken();
        AuthService.deleteVCS();
        AuthService.deleteWorkspace();
        this.router.navigate(['/home']);
    }

    public sessionExpired(): void {
        this.notificationsService.pushError('Session expired');
        this.logout();
    }

    private static setToken(token: string): void {
        sessionStorage.setItem('token', token);
    }

    private static setVCS(vcs: string): void {
        sessionStorage.setItem('vcs', vcs);
    }

    private static setWorkspace(workspace: string): void {
        sessionStorage.setItem('workspace', workspace);
    }

    private static deleteToken(): void {
        sessionStorage.removeItem('token');
    }

    private static deleteVCS(): void {
        sessionStorage.removeItem('vcs');
    }

    private static deleteWorkspace(): void {
        sessionStorage.removeItem('workspace');
    }

    public getToken(): string {
        return sessionStorage.getItem('token');
    }

    public getVCS(): VCSClient {
        return <VCSClient>sessionStorage.getItem('vcs');
    }

    public getWorkspace(): string {
        return sessionStorage.getItem('workspace');
    }

    public isAuthenticated(): boolean {
        return this.getToken() !== null;
    }
}
