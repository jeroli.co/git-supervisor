import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { WorkspaceComponent } from './dashboard/workspace/workspace.component';
import { WorkspaceSettingsComponent } from './dashboard/workspace-settings/workspace-settings.component';
import { RepositoryComponent } from './dashboard/repository/repository.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRouterOutletComponent } from './dashboard/dashboard-router-outlet.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InspectorModuleConfigComponent } from './inspector-modules/inspector-module-config/inspector-module-config.component';
import { TutorialComponent } from './tutorial/tutorial.component';

const routes: Routes = [
    // None authenticated
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent},
    { path: 'tutorial', component: TutorialComponent },
    { path: 'oauth/redirect', component: HomeComponent },

    // Authenticated
    {
        path: 'dashboard',
        canActivate: [AuthGuardService],
        component: DashboardRouterOutletComponent,
        children: [
            { path: '', component: DashboardComponent, pathMatch: 'full' },
            { path: 'workspace/:workspaceId', component: WorkspaceComponent },
            { path: 'workspace/:workspaceId/modules', component: WorkspaceSettingsComponent },
            { path: 'workspace/:workspaceId/configurations/:moduleName', component: InspectorModuleConfigComponent },
            { path: 'workspace/:workspaceId/repository/:repositoryId', component: RepositoryComponent },
        ],
    },

    // Catch all over routes
    { path: 'tutorial/:id', component: TutorialComponent },
    { path: '404', component: PageNotFoundComponent },
    { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
    imports: [CommonModule, RouterModule.forRoot(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class AppRoutingModule {}
