import { Injectable } from '@angular/core';
import { InspectorModulesModel } from '../models/inspector-modules.model';
import { InspectorModuleConfigFieldModel } from '../models/inspector-module-config-field.model';

@Injectable({
    providedIn: 'root',
})
export class InspectorModuleService {
    public basicModuleName = 'BasicModule';
    public contributorsModuleName = 'ContributorsModule';
    public evaluatorModuleName = 'EvaluatorModule';

    getConfigurationsValue(
        key: string,
        configurations: InspectorModuleConfigFieldModel[]
    ): InspectorModuleConfigFieldModel {
        return configurations.find((configuration) => configuration.id === key);
    }

    isModuleEnabled(workspaceSettings: InspectorModulesModel[], moduleName: string): boolean {
        const module = workspaceSettings.find((module) => module.name === moduleName);
        return module ? module.enabled : false;
    }
}
