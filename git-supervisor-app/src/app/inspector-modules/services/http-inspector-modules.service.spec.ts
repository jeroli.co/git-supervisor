import { TestBed, inject } from '@angular/core/testing';

import { HttpInspectorModulesService } from './http-inspector-modules.service';

describe('HttpInspectorModulesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpInspectorModulesService]
    });
  });

  it('should be created', inject([HttpInspectorModulesService], (service: HttpInspectorModulesService) => {
    expect(service).toBeTruthy();
  }));
});
