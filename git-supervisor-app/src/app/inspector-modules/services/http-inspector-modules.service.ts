import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InspectorModulesModel } from '../models/inspector-modules.model';
import { environment } from '../../../environments/environment';
import { InspectorModuleConfigFieldModel } from '../models/inspector-module-config-field.model';
import { ModulesFiltersModel } from '../modules-filters/models/modules-filters.model';
import { Observable } from 'rxjs';

const apiUrl: string = environment.api_addr;

@Injectable({
    providedIn: 'root',
})
export class HttpInspectorModulesService {
    constructor(private http: HttpClient) {}

    getModuleList(): Observable<InspectorModulesModel[]> {
        return this.http.get<InspectorModulesModel[]>(apiUrl + '/modules');
    }

    public getModuleConfigurations(
        workspaceId: string,
        moduleName: string
    ): Observable<InspectorModuleConfigFieldModel[]> {
        return this.http.get<InspectorModuleConfigFieldModel[]>(
            apiUrl + '/workspaces/' + workspaceId + '/modules/' + moduleName + '/configurations'
        );
    }

    public addModuleConfiguration(
        workspaceId: string,
        moduleName: string,
        configuration: InspectorModuleConfigFieldModel
    ): Observable<InspectorModuleConfigFieldModel> {
        const formData = new FormData();
        formData.append('configuration', JSON.stringify(configuration));
        return this.http.post<InspectorModuleConfigFieldModel>(
            apiUrl + '/workspaces/' + workspaceId + '/modules/' + moduleName + '/configurations',
            formData
        );
    }

    public deleteModuleConfiguration(
        workspaceId: string,
        moduleName: string,
        configuration: InspectorModuleConfigFieldModel
    ): Observable<string> {
        return this.http.delete<string>(
            apiUrl + '/workspaces/' + workspaceId + '/modules/' + moduleName + '/configurations/' + configuration.id
        );
    }

    public updateModuleConfigurations(
        workspaceId: string,
        moduleName: string,
        configurations: InspectorModuleConfigFieldModel[]
    ): Observable<InspectorModuleConfigFieldModel[]> {
        const formData = new FormData();
        formData.append('configurations', JSON.stringify(configurations));
        return this.http.put<InspectorModuleConfigFieldModel[]>(
            apiUrl + '/workspaces/' + workspaceId + '/modules/' + moduleName + '/configurations',
            formData
        );
    }

    public getModuleData(
        workspaceId: string,
        repositoryId: string,
        moduleName: string,
        filters?: ModulesFiltersModel,
        subDataURI?: string
    ): Observable<unknown> {
        const params = filters ? this.removeFalsyValues(filters) : null;
        const dataURI = subDataURI ? '/data/' + subDataURI : '/data';
        return this.http.get<unknown>(
            apiUrl +
                '/workspaces/' +
                workspaceId +
                '/repositories/' +
                repositoryId +
                '/modules/' +
                moduleName +
                dataURI,
            { params: params }
        );
    }

    private removeFalsyValues(obj: ModulesFiltersModel) {
        const newObj = {};
        Object.keys(obj).forEach((prop) => {
            if (obj[prop]) {
                newObj[prop] = obj[prop];
            }
        });
        return newObj;
    }
}
