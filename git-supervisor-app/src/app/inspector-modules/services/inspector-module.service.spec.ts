import { TestBed, inject } from '@angular/core/testing';

import { InspectorModuleService } from './inspector-module.service';

describe('InspectorModuleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InspectorModuleService]
    });
  });

  it('should be created', inject([InspectorModuleService], (service: InspectorModuleService) => {
    expect(service).toBeTruthy();
  }));
});
