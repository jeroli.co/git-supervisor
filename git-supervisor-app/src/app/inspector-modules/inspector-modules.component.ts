import { Component, Input, OnInit } from '@angular/core';
import { InspectorModuleService } from './services/inspector-module.service';
import { WorkspaceModel } from '../dashboard/workspace/models/workspace.model';
import { RepositoryModel } from '../dashboard/repository/models/repository.model';
import { ManageModuleViewService } from '../dashboard/repository/manage-module-view-modal/services/manage-module-view.service';

@Component({
    selector: 'app-inspector-modules',
    templateUrl: './inspector-modules.component.html',
})
export class InspectorModulesComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;

    private manageViewModalActive = false;

    constructor(
        private inspectorModulesService: InspectorModuleService, // TODO: Replace usage in template by methods here
        private manageModuleViewModalService: ManageModuleViewService
    ) {}

    ngOnInit(): void {
        this.manageModuleViewModalService.initLocalyEnabledModule(this.currentWorkspace.modules);
    }

    showManageModuleViewModal(): void {
        this.manageViewModalActive = true;
    }

    hideManageModuleViewModal(): void {
        this.manageViewModalActive = false;
    }

    isManageModuleViewModalActive(): boolean {
        return this.manageViewModalActive;
    }

    isModuleLocalyEnabled(moduleName: string): boolean {
        return this.manageModuleViewModalService.isModuleLocalyEnabled(moduleName);
    }

    isModuleEnabled(moduleName: string): boolean {
        return this.inspectorModulesService.isModuleEnabled(this.currentWorkspace.modules, moduleName);
    }
}
