/**
 * The Subcriteria class
 */
export class EvaluationSubCriteriaModel {
  id: string;
  key: string;
  compareSymbol: string;
  value: any;
  keyType: string;
  weight: number;

  constructor(id: string) {
    this.id = id;
    this.key = '';
    this.compareSymbol = '';
    this.value = null;
    this.keyType = '';
    this.weight = 0;
  }
}
