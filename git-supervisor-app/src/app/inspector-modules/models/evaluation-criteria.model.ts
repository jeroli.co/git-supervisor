import {EvaluationSubCriteriaModel} from './evaluation-subcriteria.model';
import {EvaluationOperatorModel} from './evaluation-operator.model';

/**
 * Value : The Criteria class
 */
export class EvaluationCriteriaModel {
  moduleName: string;
  subcriterias: EvaluationSubCriteriaModel[];
  operators: EvaluationOperatorModel[];
  weight: number;

  constructor(moduleName: string) {
    this.moduleName = moduleName;
    this.subcriterias = [];
    this.operators = [];
    this.weight = 0;
  }
}

