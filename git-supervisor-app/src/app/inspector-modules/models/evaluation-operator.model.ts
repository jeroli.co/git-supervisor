/**
 * The Operator class
 */
export class EvaluationOperatorModel {
  id: string;
  firstId: string;
  secondId: string;
  type: string;

  constructor(id: string) {
    this.id = id;
    this.firstId = '';
    this.secondId = '';
    this.type = '';
  }
}
