import {InspectorModuleConfigFieldModel} from './inspector-module-config-field.model';

/**
 * Module
 */
export interface InspectorModulesModel {
  name:             string;
  description:      string;
  configurations:   InspectorModuleConfigFieldModel[];
  enabled:          boolean;
  data:             any;
}
