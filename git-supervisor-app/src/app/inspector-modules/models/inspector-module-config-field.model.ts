/**
 * Configuration
 * EvaluatorModule : Criteria
 */
export class InspectorModuleConfigFieldModel {
  id: string;
  label: string;
  description: string;
  value: any;
  selected: string;

  constructor(id: string, label: string, description: string, value: any, selected?: string) {
    this.id = id;
    this.label = label;
    this.description = description;
    this.value = value;
    if (selected) {
      this.selected = selected;
    }
  }
}
