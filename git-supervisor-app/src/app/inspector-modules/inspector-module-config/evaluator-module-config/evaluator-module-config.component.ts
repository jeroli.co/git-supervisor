import { Component, Input, OnInit } from '@angular/core';
import { WorkspaceModel } from '../../../dashboard/workspace/models/workspace.model';
import { InspectorModuleService } from '../../services/inspector-module.service';
import { InspectorModuleConfigFieldModel } from '../../models/inspector-module-config-field.model';
import { NotificationsService } from '../../../notifications/services/notifications.service';
import { HttpInspectorModulesService } from '../../services/http-inspector-modules.service';
import { EvaluationSubCriteriaModel } from '../../models/evaluation-subcriteria.model';
import { EvaluationOperatorModel } from '../../models/evaluation-operator.model';
import { InspectorModulesModel } from '../../models/inspector-modules.model';

@Component({
    selector: 'app-evaluator-module-config',
    templateUrl: './evaluator-module-config.component.html',
    styleUrls: ['./evaluator-module-config.component.css'],
})
export class EvaluatorModuleConfigComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;

    public evaluatorModuleConfigurations: InspectorModuleConfigFieldModel[] = null;
    private isAddPanelActive = false;
    private editCriteriaActive = false;
    private currentEditCriteria: InspectorModuleConfigFieldModel = null;
    public editCriteriaLabel = '';
    public editCriteriaDescription = '';
    public editCriteriaSubcriterias: EvaluationSubCriteriaModel[] = null;
    public editCriteriaOperators: EvaluationOperatorModel[] = null;

    constructor(
        private inspectorModuleService: InspectorModuleService,
        private notificationService: NotificationsService,
        private httpInspectorModuleService: HttpInspectorModulesService
    ) {}

    ngOnInit(): void {
        this.httpInspectorModuleService
            .getModuleConfigurations(this.currentWorkspace._id, this.inspectorModuleService.evaluatorModuleName)
            .subscribe((configurations: InspectorModuleConfigFieldModel[]) => {
                this.evaluatorModuleConfigurations = configurations;
            });
    }

    updateEvaluatorCriteria(): void {
        this.currentEditCriteria.label = this.editCriteriaLabel;
        this.currentEditCriteria.description = this.editCriteriaDescription;
        this.currentEditCriteria.label = this.editCriteriaLabel;
        this.currentEditCriteria.description = this.editCriteriaDescription;
        this.currentEditCriteria.value.subcriterias = this.editCriteriaSubcriterias;
        this.currentEditCriteria.value.operators = this.editCriteriaOperators;
        this.currentEditCriteria.value.weight = 0;
        for (const sub of this.currentEditCriteria.value.subcriterias) {
            this.currentEditCriteria.value.weight += sub.weight;
        }
        // tslint:disable-next-line:max-line-length
        this.httpInspectorModuleService
            .updateModuleConfigurations(
                this.currentWorkspace._id,
                this.inspectorModuleService.evaluatorModuleName,
                this.evaluatorModuleConfigurations
            )
            .subscribe(() => {
                this.disactiveEditCriteria();
                this.notificationService.pushSuccess('Criteria updated');
            });
    }

    deleteEvaluatorCriteria(criteria: InspectorModuleConfigFieldModel): void {
        // tslint:disable-next-line:max-line-length
        this.httpInspectorModuleService
            .deleteModuleConfiguration(
                this.currentWorkspace._id,
                this.inspectorModuleService.evaluatorModuleName,
                criteria
            )
            .subscribe((criteriaId: string) => {
                const criteriaIndex = this.evaluatorModuleConfigurations.findIndex(
                    (config) => config.id === criteriaId
                );
                if (criteriaIndex !== -1) {
                    this.evaluatorModuleConfigurations.splice(criteriaIndex, 1);
                    this.notificationService.pushSuccess('Configuration deleted');
                }
            });
    }

    getModuleData(moduleName: string): string[] {
        const module = this.currentWorkspace.modules.find((m) => m.name === moduleName);
        return Object.keys(module.data).filter((key) => module.data[key] != null);
    }

    getComparators(dataSelected: string, moduleName: string): string[] {
        let comparators: string[];

        switch (this.getDataType(dataSelected, moduleName)) {
            case 'number':
                comparators = ['>', '<', '=', '!='];
                break;

            case 'boolean':
                comparators = ['=', '!='];
                break;

            case 'string':
                comparators = ['=', '!='];
                break;

            default:
                comparators = ['='];
        }

        return comparators;
    }

    activeAddPanel(): void {
        this.isAddPanelActive = true;
    }

    disactiveAddPanel(): void {
        this.isAddPanelActive = false;
    }

    addPanelActive(): boolean {
        return this.isAddPanelActive;
    }

    activeEditCriteria(criteria: InspectorModuleConfigFieldModel): void {
        this.editCriteriaLabel = criteria.label;
        this.editCriteriaDescription = criteria.description;
        this.editCriteriaSubcriterias = this.cloneArray(this.editCriteriaSubcriterias, criteria.value.subcriterias);
        this.editCriteriaOperators = this.cloneArray(this.editCriteriaOperators, criteria.value.operators);
        this.currentEditCriteria = criteria;
        this.editCriteriaActive = true;
    }

    disactiveEditCriteria(): void {
        this.editCriteriaLabel = '';
        this.editCriteriaDescription = '';
        this.editCriteriaSubcriterias = null;
        this.editCriteriaOperators = null;
        this.currentEditCriteria = null;
        this.editCriteriaActive = false;
    }

    isEditCriteriaActivate(criteria: InspectorModuleConfigFieldModel): boolean {
        return this.editCriteriaActive && criteria === this.currentEditCriteria;
    }

    getModule(moduleName: string): InspectorModulesModel {
        return this.currentWorkspace.modules.find((m) => m.name === moduleName);
    }

    getDataType(data: string, moduleName: string): any {
        const module = this.getModule(moduleName);
        for (const key in module.data) {
            if (key === data) {
                return typeof module.data[key];
            }
        }
    }

    /**
     * Check if a subcriteria is the last in a list of subcriterias
     * @param subcriterias the list of subcriterias
     * @param sub the subcriteria
     */
    isLastSub(subcriterias: EvaluationSubCriteriaModel[], sub: EvaluationSubCriteriaModel): boolean {
        return subcriterias.indexOf(sub) === subcriterias.length - 1;
    }

    /**
     * Add a subcriteria to a list of subcriteria
     * @param subcriterias the list of subcriterias
     * @param operators the list of operators
     */
    addSubCriteria(subcriterias: EvaluationSubCriteriaModel[], operators: EvaluationOperatorModel[]): void {
        const ope = new EvaluationOperatorModel(this.generateId(subcriterias, operators).toString());
        ope.firstId = this.getLastSub(subcriterias).id;
        operators.push(ope);
        const sub = new EvaluationSubCriteriaModel(this.generateId(subcriterias, operators).toString());
        subcriterias.push(sub);
        ope.secondId = sub.id;
    }

    /**
     * Check if adding a subcriteria or a criteria is allowed
     * @param subcriterias the list of subcriterias
     */
    addingAllowed(subcriterias: EvaluationSubCriteriaModel[]): boolean {
        for (const sub of subcriterias) {
            // tslint:disable-next-line:max-line-length
            if (
                this.editCriteriaLabel.length === 0 ||
                this.editCriteriaDescription.length === 0 ||
                sub.key.length === 0 ||
                sub.keyType.length === 0 ||
                sub.compareSymbol.length === 0 ||
                sub.value == null ||
                sub.weight < 0
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove a subcriteria from a list of subcriterias
     * @param subcriterias the list of subcriterias
     * @param operators the list of operators
     * @param subInd the index of the subcriteria to delete
     */
    removeSubCriteria(
        subcriterias: EvaluationSubCriteriaModel[],
        operators: EvaluationOperatorModel[],
        subInd: number
    ): void {
        if (!this.isLastSub(subcriterias, subcriterias[subInd])) {
            // Not last subcriteria
            operators[subInd - 1].firstId = subcriterias[subInd - 1].id;
        }
        subcriterias.splice(subInd, 1);
        operators.splice(subInd - 1, 1);
    }

    /**
     * Generate a subcriteria id
     * @param subcriterias the list of subcriterias
     * @param operators the list of operators
     */
    generateId(subcriterias: EvaluationSubCriteriaModel[], operators: EvaluationOperatorModel[]): number {
        return subcriterias.length + operators.length + 1;
    }

    /**
     * Get the last subcriteria from a list of subcriterias
     * @param subcriterias the list of subcriterias
     */
    getLastSub(subcriterias: EvaluationSubCriteriaModel[]): EvaluationSubCriteriaModel {
        return subcriterias.slice(-1)[0];
    }

    /**
     * Called when the key of a subcriteria is changed
     * @param moduleName the module of the criteria
     * @param sub the subcriteria
     */
    dataKeyChange(moduleName, sub: EvaluationSubCriteriaModel): void {
        sub.keyType = this.getDataType(sub.key, moduleName);
    }

    /**
     * Clone an array
     * @param empty the array to fill
     * @param toClone the array to be cloned
     */
    cloneArray(empty: any, toClone: any): any {
        empty = [];
        toClone.forEach((val) => empty.push(Object.assign({}, val)));
        return empty;
    }
}
