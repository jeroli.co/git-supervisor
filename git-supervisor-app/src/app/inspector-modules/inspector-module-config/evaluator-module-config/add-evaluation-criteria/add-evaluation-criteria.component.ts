import { Component, Input, OnInit } from '@angular/core';
import { WorkspaceModel } from '../../../../dashboard/workspace/models/workspace.model';
import { InspectorModulesModel } from '../../../models/inspector-modules.model';
import { EvaluationCriteriaModel } from '../../../models/evaluation-criteria.model';
import { InspectorModuleConfigFieldModel } from '../../../models/inspector-module-config-field.model';
import { InspectorModuleService } from '../../../services/inspector-module.service';
import { NotificationsService } from '../../../../notifications/services/notifications.service';
import { HttpInspectorModulesService } from '../../../services/http-inspector-modules.service';
import { EvaluationSubCriteriaModel } from '../../../models/evaluation-subcriteria.model';
import { EvaluationOperatorModel } from '../../../models/evaluation-operator.model';

@Component({
    selector: 'app-add-evaluation-criteria',
    templateUrl: './add-evaluation-criteria.component.html',
    styleUrls: ['./add-evaluation-criteria.component.css'],
})
export class AddEvaluationCriteriaComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() evaluatorConfiguration: InspectorModuleConfigFieldModel[];

    public modulesWithDataModel: InspectorModulesModel[];
    public newCriteriasNames: string[];
    public newCriteriasDescriptions: string[];

    public newEvaluationCriterias: EvaluationCriteriaModel[];

    constructor(
        private httpInspectorModuleService: HttpInspectorModulesService,
        private inspectorModuleService: InspectorModuleService,
        private notificationService: NotificationsService
    ) {}

    ngOnInit(): void {
        this.modulesWithDataModel = [];
        this.newEvaluationCriterias = [];
        this.newCriteriasNames = [];
        this.newCriteriasDescriptions = [];

        this.currentWorkspace.modules.forEach((module) => {
            if (module.data) {
                this.modulesWithDataModel.push(module);
                const criteria = new EvaluationCriteriaModel(module.name);
                criteria.subcriterias.push(
                    new EvaluationSubCriteriaModel(AddEvaluationCriteriaComponent.generateId(criteria).toString())
                );
                this.newEvaluationCriterias.push(criteria);
                this.newCriteriasNames.push('');
                this.newCriteriasDescriptions.push('');
            }
        });
    }

    dataKeyChange(ind: number, subInd: number): void {
        this.newEvaluationCriterias[ind].subcriterias[subInd].keyType = this.getDataType(
            this.newEvaluationCriterias[ind].subcriterias[subInd].key,
            this.modulesWithDataModel[ind]
        );
    }

    getComparators(module: InspectorModulesModel, ind: number, subInd: number): string[] {
        let comparators: string[];
        const dataSelected = this.newEvaluationCriterias[ind].subcriterias[subInd].key;

        switch (this.getDataType(dataSelected, module)) {
            case 'number':
                comparators = ['>', '<', '=', '!='];
                break;

            case 'boolean':
                comparators = ['=', '!='];
                break;

            case 'string':
                comparators = ['=', '!='];
                break;

            default:
                comparators = ['='];
        }

        return comparators;
    }

    getDataType(data: string, module: InspectorModulesModel): any {
        for (const key in module.data) {
            if (key === data) {
                return typeof module.data[key];
            }
        }
    }

    getData(module: InspectorModulesModel): string[] {
        return Object.keys(module.data).filter((key) => module.data[key] != null);
    }

    /**
     * Insert To DataBase
     * @param ind the subcriteria index
     */
    addEvaluatorCriteria(ind: number): void {
        const id =
            this.newCriteriasNames[ind] +
            this.newCriteriasDescriptions[ind] +
            this.newEvaluationCriterias[ind].operators.length.toString() +
            this.newEvaluationCriterias[ind].subcriterias.length.toString();
        for (const sub of this.newEvaluationCriterias[ind].subcriterias) {
            this.newEvaluationCriterias[ind].weight += sub.weight;
        }
        const newEvaluatorCriteria = new InspectorModuleConfigFieldModel(
            id.replace(/\s/g, ''),
            this.newCriteriasNames[ind],
            this.newCriteriasDescriptions[ind],
            this.newEvaluationCriterias[ind]
        );
        this.httpInspectorModuleService
            .addModuleConfiguration(
                this.currentWorkspace._id,
                this.inspectorModuleService.evaluatorModuleName,
                newEvaluatorCriteria
            )
            .subscribe((configuration: InspectorModuleConfigFieldModel) => {
                this.evaluatorConfiguration.push(configuration);
                this.notificationService.pushSuccess('Criteria added');
                this.resetModuleFormField(ind);
            });
    }

    /**
     * Add a subcriteria to a criteria
     * @param criteria the criteria
     */
    addSubCriteria(criteria: EvaluationCriteriaModel): void {
        const ope = new EvaluationOperatorModel(AddEvaluationCriteriaComponent.generateId(criteria).toString());
        ope.firstId = AddEvaluationCriteriaComponent.getLastSub(criteria).id;
        criteria.operators.push(ope);
        const sub = new EvaluationSubCriteriaModel(AddEvaluationCriteriaComponent.generateId(criteria).toString());
        criteria.subcriterias.push(sub);
        ope.secondId = sub.id;
    }

    /**
     * Get the last subcriteria from a criteria
     * @param criteria the criteria
     */
    private static getLastSub(criteria: EvaluationCriteriaModel): EvaluationSubCriteriaModel {
        return criteria.subcriterias[criteria.subcriterias.length - 1];
    }

    /**
     * Remove a subcriteria from a criteria
     * @param criteria the criteria
     * @param subInd the index of the subcriteria to delete
     */
    removeSubCriteria(criteria: EvaluationCriteriaModel, subInd: number): void {
        if (!this.isLastSub(criteria, criteria.subcriterias[subInd + 1])) {
            // Not last subcriteria
            criteria.operators[subInd + 1].firstId = criteria.subcriterias[subInd].id;
        }
        criteria.subcriterias.splice(subInd + 1, 1);
        criteria.operators.splice(subInd, 1);
    }

    /**
     * Check if a subcriteria is the last in a criteria
     * @param criteria the criteria
     * @param sub the subcriteria
     */
    isLastSub(criteria: EvaluationCriteriaModel, sub: EvaluationSubCriteriaModel): boolean {
        return criteria.subcriterias.indexOf(sub) === criteria.subcriterias.length - 1;
    }

    /**
     * Generate a subcriteria id
     * @param criteria the criteria containing the subcriteria
     */
    private static generateId(criteria: EvaluationCriteriaModel): number {
        return criteria.subcriterias.length + criteria.operators.length + 1;
    }

    /**
     * Empties the form field after the creation of a criteria
     * @param ind the index of the new criteria
     */
    private resetModuleFormField(ind: number): void {
        this.newCriteriasNames[ind] = '';
        this.newCriteriasDescriptions[ind] = '';
        this.newEvaluationCriterias[ind] = new EvaluationCriteriaModel(this.modulesWithDataModel[ind].name);
        this.newEvaluationCriterias[ind].subcriterias[0] = new EvaluationSubCriteriaModel(
            AddEvaluationCriteriaComponent.generateId(this.newEvaluationCriterias[ind]).toString()
        );
    }

    /**
     * Check if adding a subcriteria or a criteria is allowed
     * @param criteria the criteria to check
     * @param i the criteria index
     */
    addingAllowed(criteria: EvaluationCriteriaModel, i: number): boolean {
        for (const sub of criteria.subcriterias) {
            if (
                this.newCriteriasNames[i].length === 0 ||
                this.newCriteriasDescriptions[i].length === 0 ||
                sub.key.length === 0 ||
                sub.keyType.length === 0 ||
                sub.compareSymbol.length === 0 ||
                sub.value == null ||
                sub.weight < 0
            ) {
                return true;
            }
        }
        return false;
    }
}
