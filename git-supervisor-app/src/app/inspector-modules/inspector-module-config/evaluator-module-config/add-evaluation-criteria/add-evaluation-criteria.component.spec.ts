import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEvaluationCriteriaComponent } from './add-evaluation-criteria.component';

describe('AddEvaluationCriteriaComponent', () => {
  let component: AddEvaluationCriteriaComponent;
  let fixture: ComponentFixture<AddEvaluationCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEvaluationCriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEvaluationCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
