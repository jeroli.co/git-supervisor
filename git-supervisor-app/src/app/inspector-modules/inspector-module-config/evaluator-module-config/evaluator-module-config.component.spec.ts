import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatorModuleConfigComponent } from './evaluator-module-config.component';

describe('EvaluatorModuleConfigComponent', () => {
  let component: EvaluatorModuleConfigComponent;
  let fixture: ComponentFixture<EvaluatorModuleConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluatorModuleConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluatorModuleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
