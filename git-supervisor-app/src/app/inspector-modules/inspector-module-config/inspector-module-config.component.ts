import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InspectorModuleConfigFieldModel } from '../models/inspector-module-config-field.model';
import { WorkspaceService } from '../../dashboard/workspace/services/workspace.service';
import { WorkspaceModel } from '../../dashboard/workspace/models/workspace.model';
import { InspectorModuleService } from '../services/inspector-module.service';
import { HttpWorkspaceService } from '../../dashboard/workspace/services/http-workspace.service';
import { NotificationsService } from '../../notifications/services/notifications.service';
import { HttpInspectorModulesService } from '../services/http-inspector-modules.service';

@Component({
    selector: 'app-inspector-module-config',
    templateUrl: './inspector-module-config.component.html',
    styleUrls: ['./inspector-module-config.component.css'],
})
export class InspectorModuleConfigComponent implements OnInit {
    public currentWorkspace: WorkspaceModel = null;

    public configForm: FormGroup = null;

    public moduleName: string;
    public moduleConfiguration: InspectorModuleConfigFieldModel[];

    constructor(
        private route: ActivatedRoute,
        private workspaceService: WorkspaceService,
        private httpWorkspaceService: HttpWorkspaceService,
        private notificationService: NotificationsService,
        private inspectorModulesService: InspectorModuleService,
        private httpInspectorModuleService: HttpInspectorModulesService
    ) {}

    ngOnInit(): void {
        const workspaceId = this.route.snapshot.paramMap.get('workspaceId');
        const moduleName = this.route.snapshot.paramMap.get('moduleName');

        if (workspaceId && moduleName) {
            this.httpWorkspaceService.getWorkspace(workspaceId).subscribe((workspace: WorkspaceModel) => {
                this.currentWorkspace = workspace;
            });

            this.moduleName = moduleName;
            this.httpInspectorModuleService
                .getModuleConfigurations(workspaceId, moduleName)
                .subscribe((configurations: InspectorModuleConfigFieldModel[]) => {
                    this.moduleConfiguration = configurations;
                    if (!this.isEvaluatorModule()) {
                        this.configForm = this.configToFormGroup();
                    }
                });
        }
    }

    updateModuleConfigurations(): void {
        if (this.isValid()) {
            const data = this.configForm.value;
            Object.keys(data).forEach((key) => {
                this.moduleConfiguration.forEach((config) => {
                    if (config.id === key) {
                        if (config.value instanceof Array) {
                            config.selected = data[key];
                        } else {
                            config.value = data[key];
                        }
                    }
                });
            });

            this.httpInspectorModuleService
                .updateModuleConfigurations(this.currentWorkspace._id, this.moduleName, this.moduleConfiguration)
                .subscribe((configurations: InspectorModuleConfigFieldModel[]) => {
                    this.moduleConfiguration = configurations;
                    this.notificationService.pushSuccess('Configurations updated');
                    this.workspaceService.openWorkspaceSettings(this.currentWorkspace._id);
                });
        }
    }

    isValid(): boolean {
        let formValid = true;
        this.moduleConfiguration.forEach((config) => {
            if (!this.configForm.controls[config.id].valid) {
                formValid = false;
            }
        });

        return formValid;
    }

    isSelected(configField: InspectorModuleConfigFieldModel, opt: string): boolean {
        return opt === configField.selected;
    }

    configToFormGroup(): FormGroup {
        const group: any = {};
        this.moduleConfiguration.forEach((config) => {
            if (config.value instanceof Array) {
                if (config.value.length > 0) {
                    group[config.id] = new FormControl(config.value[0], Validators.required);
                } else {
                    group[config.id] = new FormControl('', Validators.required);
                }
            } else {
                group[config.id] = new FormControl(config.value, Validators.required);
            }
        });

        return new FormGroup(group);
    }

    isString(value: unknown): boolean {
        return typeof value === 'string';
    }

    isBool(value: unknown): boolean {
        return typeof value === 'boolean';
    }

    isList(value: unknown): boolean {
        return value instanceof Array;
    }

    // Evaluator Config
    isEvaluatorModule(): boolean {
        return this.moduleName === this.inspectorModulesService.evaluatorModuleName;
    }
}
