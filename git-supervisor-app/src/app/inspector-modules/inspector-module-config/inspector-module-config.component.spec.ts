import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectorModuleConfigComponent } from './inspector-module-config.component';

describe('InspectorModuleConfigComponent', () => {
  let component: InspectorModuleConfigComponent;
  let fixture: ComponentFixture<InspectorModuleConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectorModuleConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectorModuleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
