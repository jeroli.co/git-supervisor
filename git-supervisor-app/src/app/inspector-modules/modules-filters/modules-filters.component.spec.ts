import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesFiltersComponent } from './modules-filters.component';

describe('ModulesFiltersComponent', () => {
  let component: ModulesFiltersComponent;
  let fixture: ComponentFixture<ModulesFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulesFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
