export class ModulesFiltersModel {

  branch: string;
  since: string;
  until: string;

  constructor(branch: string, since: string, until: string) {
    this.branch = branch;
    this.since = since;
    this.until = until;
  }

}
