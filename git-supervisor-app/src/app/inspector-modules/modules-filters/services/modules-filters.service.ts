import { EventEmitter, Injectable, Output } from '@angular/core';
import { ModulesFiltersModel } from '../models/modules-filters.model';

@Injectable({
    providedIn: 'root',
})
export class ModulesFiltersService {
    @Output() modulesFiltersChangeEvent: EventEmitter<ModulesFiltersModel> = new EventEmitter<ModulesFiltersModel>();

    private _branchSelected: string;
    private _sinceDateSelected: string;
    private _untilDateSelected: string;

    public modulesFiltersChange(filters: ModulesFiltersModel): void {
        this.modulesFiltersChangeEvent.emit(filters);
    }

    get branchSelected(): string {
        return this._branchSelected;
    }

    set branchSelected(value: string) {
        this._branchSelected = value;
    }

    get sinceDateSelected(): string {
        return this._sinceDateSelected;
    }

    set sinceDateSelected(value: string) {
        this._sinceDateSelected = value;
    }

    get untilDateSelected(): string {
        return this._untilDateSelected;
    }

    set untilDateSelected(value: string) {
        this._untilDateSelected = value;
    }
}
