import { TestBed, inject } from '@angular/core/testing';

import { ModulesFiltersService } from './modules-filters.service';

describe('ModulesFiltersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModulesFiltersService]
    });
  });

  it('should be created', inject([ModulesFiltersService], (service: ModulesFiltersService) => {
    expect(service).toBeTruthy();
  }));
});
