import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModulesFiltersModel } from './models/modules-filters.model';
import * as moment from 'moment';
import { HttpRepositoryService } from '../../dashboard/repository/services/http-repository.service';
import { WorkspaceModel } from '../../dashboard/workspace/models/workspace.model';
import { RepositoryModel } from '../../dashboard/repository/models/repository.model';
import { ModulesFiltersService } from './services/modules-filters.service';

@Component({
    selector: 'app-modules-filters',
    templateUrl: './modules-filters.component.html',
    styleUrls: ['./modules-filters.component.css'],
})
export class ModulesFiltersComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;

    // Filter module
    public filtersShowed: boolean;

    // Filter - By date
    private sinceDateSelected: string;
    public sinceCalendarOptions: any;

    private untilDateSelected: string;
    public untilCalendarOptions: any;

    // Filter - By branch
    public branchSelected: string;
    public branches: string[];

    constructor(
        private modulesFiltersService: ModulesFiltersService,
        private httpRepositoryService: HttpRepositoryService
    ) {}

    ngOnInit(): void {
        // Filter module
        this.filtersShowed = null;

        this.branchSelected = '';
        this.modulesFiltersService.branchSelected = '';
        this.sinceDateSelected = '';
        this.modulesFiltersService.sinceDateSelected = '';
        this.untilDateSelected = '';
        this.modulesFiltersService.untilDateSelected = '';

        // Filters - By date
        this.sinceCalendarOptions = {
            type: 'date',
            color: 'link',
            dateFormat: 'DD-MM-YYYY',
            weekStart: 1,
            startDate: moment().toString(),
            showTodayButton: false,
        };

        this.untilCalendarOptions = {
            type: 'date',
            color: 'link',
            dateFormat: 'DD-MM-YYYY',
            weekStart: 1,
            startDate: moment().toString(),
        };

        // Filter - By branch
        this.branches = [];
        this.httpRepositoryService
            .getBranches(this.currentWorkspace._id, this.currentRepository._id)
            .subscribe((branches) => {
                this.branches = branches;
                if (this.branches.length > 0) {
                    this.branchSelected = this.branches[0];
                    this.modulesFiltersService.branchSelected = this.branches[0];
                }
            });
    }

    /**
     * @description Hide or show the filter on click
     */
    public toggleFilter(): void {
        this.filtersShowed = this.filtersShowed === null || !this.filtersShowed;
    }

    /**
     * @description Event handler for branch filter
     * @param branch
     */
    public branchChange(branch: string): void {
        if (this.branchSelected !== branch) {
            this.branchSelected = branch;
            this.modulesFiltersService.branchSelected = branch;
            this.applyFilter();
        }
    }

    /**
     * @description Event handler for since date filter
     * @param date
     */
    public sinceChange(date: string): void {
        if (date !== null && this.sinceDateSelected !== date) {
            this.sinceDateSelected = date;
            this.modulesFiltersService.sinceDateSelected = date;
            this.applyFilter();
        }
    }

    /**
     * @description Event handler for until date filter
     * @param date
     */
    public untilChange(date: string): void {
        if (date !== null && this.untilDateSelected !== date) {
            this.untilDateSelected = date;
            this.modulesFiltersService.untilDateSelected = date;
            this.applyFilter();
        }
    }

    /**
     * @description Apply filters and reload modules
     */
    private applyFilter(): void {
        const filters = new ModulesFiltersModel(this.branchSelected, this.sinceDateSelected, this.untilDateSelected);
        this.modulesFiltersService.modulesFiltersChange(filters);
    }
}
