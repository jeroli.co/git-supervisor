import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as BulmaCalendar from 'bulma-calendar/dist/js/bulma-calendar';

@Component({
    selector: 'app-calendar-date-filter',
    templateUrl: './calendar-date-filter.component.html',
    styleUrls: ['./calendar-date-filter.component.css'],
})
export class CalendarDateFilterComponent implements OnInit, AfterViewInit {
    @Input() calendarId: string;
    private selector: string;

    @Input() options: any;

    private date = '';
    @Output() dateEvent: EventEmitter<string> = new EventEmitter<string>();

    ngOnInit(): void {
        this.selector = '#' + this.calendarId;
    }

    ngAfterViewInit(): void {
        if (this.options) {
            BulmaCalendar.attach(this.selector, this.options);
            const inputElement: any = document.querySelector(this.selector);
            if (inputElement) {
                inputElement.bulmaCalendar.on('select', (datepicker) => {
                    this.date = datepicker.data.value();
                    this.dateEvent.emit(this.date);
                });
            }
        }
    }
}
