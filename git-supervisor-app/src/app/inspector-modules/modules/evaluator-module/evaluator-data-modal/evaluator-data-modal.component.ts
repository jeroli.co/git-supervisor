import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-evaluator-data-modal',
    templateUrl: './evaluator-data-modal.component.html',
    styleUrls: ['./evaluator-data-modal.component.css'],
})
export class EvaluatorDataModalComponent implements OnInit {
    @Input() data: any;

    @Output() hideModalEvent: EventEmitter<void> = new EventEmitter();

    public keys: string[];

    ngOnInit(): void {
        this.keys = Object.keys(this.data);
    }

    hideModal(): void {
        this.hideModalEvent.emit();
    }

    isPrintable(key: string): boolean {
        return !(this.data[key] instanceof Array || this.data[key] instanceof Object);
    }
}
