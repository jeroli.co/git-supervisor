import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatorDataModalComponent } from './evaluator-data-modal.component';

describe('EvaluatorDataModalComponent', () => {
  let component: EvaluatorDataModalComponent;
  let fixture: ComponentFixture<EvaluatorDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluatorDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluatorDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
