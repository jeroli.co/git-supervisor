import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { WorkspaceModel } from '../../../dashboard/workspace/models/workspace.model';
import { RepositoryModel } from '../../../dashboard/repository/models/repository.model';
import { InspectorModuleService } from '../../services/inspector-module.service';
import { EvaluatorModuleDataModel } from './models/evaluator-module-data.model';
import { ModulesFiltersModel } from '../../modules-filters/models/modules-filters.model';
import { Subscription } from 'rxjs';
import { ModulesFiltersService } from '../../modules-filters/services/modules-filters.service';
import { InspectorModuleConfigFieldModel } from '../../models/inspector-module-config-field.model';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpInspectorModulesService } from '../../services/http-inspector-modules.service';

@Component({
    selector: 'app-evaluator-module',
    templateUrl: './evaluator-module.component.html',
    styleUrls: ['./evaluator-module.component.css'],
})
export class EvaluatorModuleComponent implements OnInit, OnDestroy {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;

    public sortedEvaluatorCriterias: {
        moduleName: string;
        criterias: { configuration: InspectorModuleConfigFieldModel; fulfilled: boolean; data: string }[];
    }[];
    public criteriasSortedAndLoaded: boolean;

    private filters: ModulesFiltersModel;
    private modulesFiltersChangedSubscription: Subscription;

    public notFulfilledOnly: boolean;

    public evaluationDataModalData: any;
    private _isEvaluationDataModalActive: boolean;

    public errorLoadingData: string = null;

    constructor(
        private inspectorModuleService: InspectorModuleService,
        private httpInspectorModuleService: HttpInspectorModulesService,
        private modulesFiltersService: ModulesFiltersService
    ) {}

    ngOnInit(): void {
        this.filters = null;
        this.notFulfilledOnly = false;
        this.criteriasSortedAndLoaded = false;

        this.evaluationDataModalData = null;
        this._isEvaluationDataModalActive = false;

        this.modulesFiltersChangedSubscription = this.modulesFiltersService.modulesFiltersChangeEvent.subscribe(
            (filters: ModulesFiltersModel) => {
                this.filters = filters;
                this.getEvaluatorModuleData();
            }
        );

        this.getEvaluatorModuleData();
    }

    private getEvaluatorModuleData() {
        if (this.inspectorModuleService.isModuleEnabled(this.currentWorkspace.modules, 'EvaluatorModule')) {
            this.httpInspectorModuleService
                .getModuleData(
                    this.currentWorkspace._id,
                    this.currentRepository._id,
                    this.inspectorModuleService.evaluatorModuleName,
                    this.filters
                )
                .subscribe(
                    (data: EvaluatorModuleDataModel[]) => {
                        this.sortedEvaluatorCriterias = [];
                        data.forEach((criteria: EvaluatorModuleDataModel) => {
                            const moduleIndex = this.sortedEvaluatorCriterias.findIndex(
                                (m) => m.moduleName === criteria.configuration.value.moduleName
                            );
                            if (moduleIndex === -1) {
                                this.sortedEvaluatorCriterias.push({
                                    moduleName: criteria.configuration.value.moduleName,
                                    criterias: [
                                        {
                                            configuration: criteria.configuration,
                                            fulfilled: criteria.fulfilled,
                                            data: criteria.data,
                                        },
                                    ],
                                });
                            } else {
                                this.sortedEvaluatorCriterias[moduleIndex].criterias.push({
                                    configuration: criteria.configuration,
                                    fulfilled: criteria.fulfilled,
                                    data: criteria.data,
                                });
                            }
                        });
                        this.criteriasSortedAndLoaded = true;
                    },
                    (error: HttpErrorResponse) => {
                        this.errorLoadingData = 'Cannot load evaluator data. ' + error.message;
                    }
                );
        }
    }

    public filterFulfilled(fulfilled: boolean): boolean {
        return !(this.notFulfilledOnly && fulfilled);
    }

    public showEvaluationDataModal(data: any): void {
        this.evaluationDataModalData = data;
        this._isEvaluationDataModalActive = true;
    }

    public hideEvaluationDataModal(): void {
        this._isEvaluationDataModalActive = false;
        this.evaluationDataModalData = null;
    }

    public isEvaluationDataModalActive(): boolean {
        return this._isEvaluationDataModalActive;
    }

    ngOnDestroy(): void {
        this.modulesFiltersChangedSubscription.unsubscribe();
    }

    mark(): number {
        let mark = 0;
        if (this.criteriasSortedAndLoaded) {
            let numerator = 0;
            let denominator = 0;
            for (const listC of this.sortedEvaluatorCriterias) {
                for (const s of listC.criterias) {
                    if (s.fulfilled) {
                        numerator += s.configuration.value.weight;
                    }
                    denominator += s.configuration.value.weight;
                }
                if (denominator > 0) {
                    mark = (numerator / denominator) * 20;
                }
            }
        }
        mark = Math.round(mark * 10) / 10;
        return mark;
    }

    getTotalWeight(): number {
        let weight = 0;
        for (const listC of this.sortedEvaluatorCriterias) {
            for (const s of listC.criterias) {
                weight += s.configuration.value.weight;
            }
        }
        return weight;
    }
}
