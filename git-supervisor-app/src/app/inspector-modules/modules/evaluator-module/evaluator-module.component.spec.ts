import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatorModuleComponent } from './evaluator-module.component';

describe('EvaluatorModuleComponent', () => {
  let component: EvaluatorModuleComponent;
  let fixture: ComponentFixture<EvaluatorModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluatorModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluatorModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
