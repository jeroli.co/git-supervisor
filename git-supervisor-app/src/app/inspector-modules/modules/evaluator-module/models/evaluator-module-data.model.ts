import {InspectorModuleConfigFieldModel} from "../../../models/inspector-module-config-field.model";

export class EvaluatorModuleDataModel {
  configuration: InspectorModuleConfigFieldModel;
  fulfilled: boolean;
  data: any;
}
