import { Component, Input, OnInit } from '@angular/core';
import { InspectorModuleService } from '../../services/inspector-module.service';
import { BasicModuleDataModel } from './models/basic-module-data.model';
import { RepositoryModel } from '../../../dashboard/repository/models/repository.model';
import { WorkspaceModel } from '../../../dashboard/workspace/models/workspace.model';
import { InspectorModuleConfigFieldModel } from '../../models/inspector-module-config-field.model';
import { HttpInspectorModulesService } from '../../services/http-inspector-modules.service';

@Component({
    selector: 'app-basic-module',
    templateUrl: './basic-module.component.html',
    styleUrls: ['./basic-module.component.css'],
})
export class BasicModuleComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;

    public basicModuleData: BasicModuleDataModel;
    public basicModuleConfigurations: InspectorModuleConfigFieldModel[];

    constructor(
        private inspectorModuleService: InspectorModuleService,
        private httpInspectorModuleService: HttpInspectorModulesService
    ) {}

    ngOnInit(): void {
        this.basicModuleData = null;
        this.basicModuleConfigurations = null;
        if (this.inspectorModuleService.isModuleEnabled(this.currentWorkspace.modules, 'BasicModule')) {
            this.httpInspectorModuleService
                .getModuleData(
                    this.currentWorkspace._id,
                    this.currentRepository._id,
                    this.inspectorModuleService.basicModuleName
                )
                .subscribe((data: BasicModuleDataModel) => {
                    this.basicModuleData = data;
                });

            this.httpInspectorModuleService
                .getModuleConfigurations(this.currentWorkspace._id, this.inspectorModuleService.basicModuleName)
                .subscribe((configurations: InspectorModuleConfigFieldModel[]) => {
                    this.basicModuleConfigurations = configurations;
                });
        }
    }

    // TODO: Place into util (static)
    dateToLocalDateString(date: string): string {
        return new Date(date).toLocaleDateString('fr-FR');
    }

    isLicenseEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('license', this.basicModuleConfigurations);
    }

    isCommitCountEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('commitsCount', this.basicModuleConfigurations);
    }

    isBranchCountEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('branchesCount', this.basicModuleConfigurations);
    }

    isOpenIssuesCountEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('openIssuesCount', this.basicModuleConfigurations);
    }

    isTagCountEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('tagsCount', this.basicModuleConfigurations);
    }

    isSizeEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('size', this.basicModuleConfigurations);
    }

    isContributorCountEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('contributorsCount', this.basicModuleConfigurations);
    }

    isLastActivityAtEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('lastActivityAt', this.basicModuleConfigurations);
    }

    isWikiEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('wiki', this.basicModuleConfigurations);
    }

    isURLEnabled(): InspectorModuleConfigFieldModel {
        return this.inspectorModuleService.getConfigurationsValue('url', this.basicModuleConfigurations);
    }
}
