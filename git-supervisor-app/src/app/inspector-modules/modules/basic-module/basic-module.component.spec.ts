import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicModuleComponent } from './basic-module.component';

describe('BasicModuleComponent', () => {
  let component: BasicModuleComponent;
  let fixture: ComponentFixture<BasicModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
