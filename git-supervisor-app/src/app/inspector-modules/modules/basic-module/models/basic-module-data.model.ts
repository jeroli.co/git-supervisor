export class BasicModuleDataModel {
  license:            string;
  commits_count:       number;
  branches_count:       number;
  tags_count:          number;
  open_issues_count:  number;
  size:               number;
  contributors_count:  number;
  last_activity_at:   string;
  wiki_enabled:       boolean;
  url:                string;
}
