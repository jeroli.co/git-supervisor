import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitsModalComponent } from './commits-modal.component';

describe('CommitsModalComponent', () => {
  let component: CommitsModalComponent;
  let fixture: ComponentFixture<CommitsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommitsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
