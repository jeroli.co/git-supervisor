import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommitModel } from '../models/commit.model';

@Component({
    selector: 'app-commits-modal',
    templateUrl: './commits-modal.component.html',
    styleUrls: ['./commits-modal.component.css'],
})
export class CommitsModalComponent {
    @Input() public contributorName: string;
    @Input() public startDate: string;
    @Input() public endDate: string;
    @Input() public commits: CommitModel[];

    @Output() public hideCommitsModal: EventEmitter<void> = new EventEmitter();

    hideCommitsModalEvent(): void {
        this.hideCommitsModal.emit();
    }

    commitDateToLocalDateString(commitDate: string): string {
        return new Date(commitDate).toLocaleDateString('fr-FR');
    }
}
