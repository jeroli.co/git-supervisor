import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorsModuleComponent } from './contributors-module.component';

describe('ContributorsModuleComponent', () => {
  let component: ContributorsModuleComponent;
  let fixture: ComponentFixture<ContributorsModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorsModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorsModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
