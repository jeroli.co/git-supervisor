import {CommitModel} from "./commit.model";

export class ContributorModuleDataModel {
  id: number;
  name: string;
  username: string;
  email: string[];
  avatar: string;
  webUrl: string;
  commitsCount: number;
  issuesCount: number;
  openIssuesCount: number;
  issues: number;
  commits: CommitModel[];
}
