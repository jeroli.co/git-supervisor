export class CommitModel {
  title: string;
  createdAt: string;
  authorEmail: string;
}
