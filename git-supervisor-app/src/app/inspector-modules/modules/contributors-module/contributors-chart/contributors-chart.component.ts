import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CommitModel } from '../models/commit.model';
import { ModulesFiltersModel } from '../../../modules-filters/models/modules-filters.model';
import { Subscription } from 'rxjs';
import { ModulesFiltersService } from '../../../modules-filters/services/modules-filters.service';
import * as moment from 'moment';

@Component({
    selector: 'app-contributors-chart',
    templateUrl: './contributors-chart.component.html',
    styleUrls: ['./contributors-chart.component.css'],
})
export class ContributorsChartComponent implements OnInit, OnChanges {
    @Input() public commits: CommitModel[];

    // Filter
    private filters: ModulesFiltersModel;
    private moduleFiltersChangedSubscription: Subscription;

    // Date filter
    private since: Date;
    private until: Date;

    private PERIODDURATION: number;

    // Contributors
    private contributorsCommitsData: number[];
    private contributorsDateArray: Date[];

    // Contributors chart
    public contributorsChartData;
    public contributorsChartOptions;
    public contributorsChartLabels: string[];
    public contributorsChartColor;

    // CommitsModal info
    public contributorsPeriodCommits: CommitModel[];
    public contributorsPeriodStartDate: string;
    public contributorsPeriodEndDate: string;
    private commitsModalActive: boolean;

    constructor(private modulesFiltersService: ModulesFiltersService) {}

    ngOnInit(): void {
        this.commitsModalActive = false;
        this.moduleFiltersChangedSubscription = this.modulesFiltersService.modulesFiltersChangeEvent.subscribe(
            (filters: ModulesFiltersModel) => {
                this.filters = filters;
                this.setInfosChartForContributors();
            }
        );
    }

    ngOnChanges(): void {
        this.setInfosChartForContributors();
    }

    /**
     * @description Set chart for all contributors
     */
    private setInfosChartForContributors(): void {
        // Reset data from the chart, begin a new chart
        this.resetInfosChart();

        // Set dates bounds from the filter
        this.setSinceUntilDatesFromFilter();

        // Set data for each period for the chart
        this.setDataForChart();

        // Set data and option for the rendering
        this.setChartParts();
    }

    /**
     * @description Set data for ccontributor chart
     */
    private setDataForChart(): void {
        let date: Date = new Date();
        date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        // Index to cut the chart in several parts (1 per week)
        let bOT = 1;

        // Let's go back to the beginning of the period
        date.setTime(this.since.getTime());

        // Set the chart gap
        const period: number = this.until.getTime() - this.since.getTime();
        this.setPeriodDuration(period);

        // For each single bunch of time, fill the labels field with the date of all the period (if date is out of bounds, her is reset)
        for (let i = 0; i < bOT; i++) {
            if (date > this.until) {
                date = this.until;
            }

            const dateTest = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
            if (i + 1 === bOT && date < this.until) {
                bOT++;
            }
            this.contributorsDateArray.push(dateTest);
            this.contributorsChartLabels.push(date.getDate() + '/' + (date.getMonth() + 1));

            date.setTime(date.getTime() + this.PERIODDURATION);
        }

        for (let i = 0; i < this.contributorsDateArray.length; i++) {
            this.contributorsCommitsData.push(this.getCommitByPeriod(i).length);
        }
    }

    /**
     * Set period duration between each chart gap
     * @param period
     */
    private setPeriodDuration(period: number) {
        const WEEKDURATION = 604800000;
        if (period < 2 * WEEKDURATION) {
            this.PERIODDURATION = WEEKDURATION / 7;
        } else if (period >= 2 * WEEKDURATION && period < 4 * WEEKDURATION * 3) {
            this.PERIODDURATION = WEEKDURATION;
        } else {
            this.PERIODDURATION = 2 * WEEKDURATION;
        }
    }
    /**
     * @description Set date from filter string date
     * @param sinceUntilDate
     * @return Date
     * Return the well formed date
     */
    private static setSinceUntilDate(sinceUntilDate: string): Date {
        return moment.utc(sinceUntilDate, 'DD-MM-YYYY').toDate();
    }

    /**
     * @description Set the since and until dates with in adequacy of the filter
     * @return number
     * Return the effective period by date
     */
    private setSinceUntilDatesFromFilter(): void {
        if (this.filters) {
            if (this.commits) {
                this.since =
                    this.filters.since && this.filters.since.length > 0
                        ? ContributorsChartComponent.setSinceUntilDate(this.filters.since)
                        : this.commits.length > 2
                        ? new Date(this.commits[this.commits.length - 1].createdAt)
                        : new Date(this.commits[0].createdAt);
                this.until =
                    this.filters.until && this.filters.until.length > 0
                        ? ContributorsChartComponent.setSinceUntilDate(this.filters.until)
                        : new Date();
            }
        } else {
            this.since =
                this.commits.length > 2
                    ? new Date(this.commits[this.commits.length - 1].createdAt)
                    : new Date(this.commits[0].createdAt);
            this.until = new Date();
        }
        this.since = new Date(Date.UTC(this.since.getFullYear(), this.since.getMonth(), this.since.getDate()));
        this.until = new Date(Date.UTC(this.until.getFullYear(), this.until.getMonth(), this.until.getDate()));
    }

    /**
     * @description Set parts for the all contributor chart
     */
    private setChartParts(): void {
        // Set data for chart
        this.contributorsChartData = [
            {
                label: '# of commits',
                data: this.contributorsCommitsData,
            },
        ];

        // Set the color for chart
        this.contributorsChartColor = [
            {
                // Line
                backgroundColor: 'rgba(255, 170, 0, 0.6)',
                borderColor: 'rgba(243, 156, 2, 1)',

                // Point
                pointBackgroundColor: 'rgba(255, 255, 255, 1)',
                pointBorderColor: 'rgba(243, 156, 2, 1)',

                // Point hover
                pointHoverBackgroundColor: 'rgba(255, 170, 0, 0.9))',
                pointHoverBorderColor: 'rgba(243, 156, 2, 1)',
            },
        ];

        // Set options for chart<
        this.contributorsChartOptions = {
            title: {
                display: true,
                text: 'Commits on the project',
            },
            responsive: true,
            tooltips: {
                mode: 'index',
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            precision: 0,
                        },
                    },
                ],
            },
        };
    }

    /**
     * @description Get the commits done in a period in adequacy with an index
     * @param index
     * @return CommitModel[]
     * Return an array of commits done according the data in contributorModule
     */
    private getCommitByPeriod(index: number): CommitModel[] {
        const currentDate: Date = this.contributorsDateArray[index];
        const listCommit: CommitModel[] = [];

        if (index === 0) {
            this.commits.forEach((commit: CommitModel) => {
                const commitDate = new Date(commit.createdAt);
                if (ContributorsChartComponent.compareDate(commitDate, currentDate) === 0) {
                    listCommit.push(commit);
                }
            });
            return listCommit;
        }

        const previousDate: Date = this.contributorsDateArray[index - 1];
        this.commits.forEach((commit: CommitModel) => {
            const commitDate = new Date(commit.createdAt);
            const compareCurrentDate: number = ContributorsChartComponent.compareDate(commitDate, currentDate);
            if (
                ContributorsChartComponent.compareDate(commitDate, previousDate) === 1 &&
                (compareCurrentDate === 0 || compareCurrentDate === -1)
            ) {
                listCommit.push(commit);
            }
        });

        return listCommit;
    }

    /**
     * @description Compare two dates, works like a comparator
     * @param date1
     * @param date2
     * @return number
     * Return if dates are equals (0), date1 > date2 (1) or date1 < date2 (-1)
     */
    // TODO: Place into an util file
    private static compareDate(date1: Date, date2: Date): number {
        const date1WithoutTime = new Date(Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate())).getTime();
        const date2WithoutTime = new Date(Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate())).getTime();

        if (date1WithoutTime < date2WithoutTime) {
            return -1;
        } else if (date1WithoutTime > date2WithoutTime) {
            return 1;
        }
        return 0;
    }

    /**
     * @description Reset infos for a ccontributor chart
     */
    private resetInfosChart(): void {
        this.contributorsChartLabels = [];
        this.contributorsCommitsData = [];
        this.contributorsChartOptions = [];

        this.contributorsDateArray = [];
    }

    /**
     * @description Event handler for the click on the chart on a ccontributor
     * @param event
     */
    chartClick(event): void {
        if (event.active.length > 0) {
            const index = event.active[0]._index;
            this.contributorsPeriodCommits = this.getCommitByPeriod(index);

            if (index === 0) {
                this.contributorsPeriodStartDate = this.contributorsDateArray[0].toLocaleDateString('fr-FR');
            } else {
                this.contributorsPeriodStartDate = this.contributorsDateArray[index - 1].toLocaleDateString('fr-FR');
            }
            this.contributorsPeriodEndDate = this.contributorsDateArray[index].toLocaleDateString('fr-FR');
            this.showCommitsModal();
        }
    }

    public showCommitsModal(): void {
        this.commitsModalActive = true;
    }

    public hideCommitsModal(): void {
        this.commitsModalActive = false;
    }

    public isCommitsModalActive(): boolean {
        return this.commitsModalActive;
    }
}
