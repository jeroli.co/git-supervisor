import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorsChartComponent } from './contributors-chart.component';

describe('ContributorsChartComponent', () => {
  let component: ContributorsChartComponent;
  let fixture: ComponentFixture<ContributorsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
