import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CommitModel } from '../models/commit.model';
import { ModulesFiltersModel } from '../../../modules-filters/models/modules-filters.model';
import { Subscription } from 'rxjs';
import { ModulesFiltersService } from '../../../modules-filters/services/modules-filters.service';
import * as moment from 'moment';

@Component({
    selector: 'app-contributor-chart',
    templateUrl: './contributor-chart.component.html',
    styleUrls: ['./contributor-chart.component.css'],
})
export class ContributorChartComponent implements OnInit, OnChanges {
    @Input() public contributorName: string;
    @Input() public commits: CommitModel[];

    // Filters
    private filters: ModulesFiltersModel;
    private moduleFiltersChangedSubscription: Subscription;

    // Date filter
    private since: Date;
    private until: Date;

    private PERIODDURATION = 604800000;

    // Contributor
    private contributorCommitsData: number[];
    private contributorDateArray: Date[];

    // Contributor chart
    public contributorChartData;
    public contributorChartOptions;
    public contributorChartLabel: string[];

    // CommitsModal info
    public contributorPeriodCommits: CommitModel[];
    public contributorPeriodStartDate: string;
    public contributorPeriodEndDate: string;
    private commitsModalActive: boolean;

    constructor(private modulesFiltersService: ModulesFiltersService) {}

    ngOnInit(): void {
        this.commitsModalActive = false;

        this.moduleFiltersChangedSubscription = this.modulesFiltersService.modulesFiltersChangeEvent.subscribe(
            (filters: ModulesFiltersModel) => {
                this.filters = filters;
                this.setInfosChartForAContributor();
            }
        );
    }

    ngOnChanges(): void {
        this.setInfosChartForAContributor();
    }

    /**
     * @description Reset infos for a contributor chart
     */
    private resetInfosChart(): void {
        this.contributorChartLabel = [];
        this.contributorCommitsData = [];
        this.contributorChartOptions = [];

        this.contributorDateArray = [];
    }

    /**
     * @description Set infos for a contributor chart
     */
    public setInfosChartForAContributor(): void {
        // Reset data from the chart, begin a new chart
        this.resetInfosChart();

        // Set dates bounds from the filter
        this.setSinceUntilDatesFromFilter();

        // Set data for each period for the chart
        this.setDataForChart();

        // Set data and option for the rendering
        this.setChartParts();
    }

    /**
     * @description Set the since and until dates with in adequacy of the filter
     * @return number
     * Return the effective period by date
     */
    private setSinceUntilDatesFromFilter(): void {
        if (this.filters) {
            if (this.commits) {
                this.since =
                    this.filters.since && this.filters.since.length > 0
                        ? ContributorChartComponent.setSinceUntilDate(this.filters.since)
                        : this.commits.length > 2
                        ? new Date(this.commits[this.commits.length - 1].createdAt)
                        : new Date(this.commits[0].createdAt);
                this.until =
                    this.filters.until && this.filters.until.length > 0
                        ? ContributorChartComponent.setSinceUntilDate(this.filters.until)
                        : new Date();
            }
        } else {
            this.since =
                this.commits.length > 2
                    ? new Date(this.commits[this.commits.length - 1].createdAt)
                    : new Date(this.commits[0].createdAt);
            this.until = new Date();
        }
        this.since = new Date(Date.UTC(this.since.getFullYear(), this.since.getMonth(), this.since.getDate()));
        this.until = new Date(Date.UTC(this.until.getFullYear(), this.until.getMonth(), this.until.getDate()));
    }

    /**
     * @description Set parts for the contributor chart
     */
    private setChartParts(): void {
        // Set chart data
        this.contributorChartData = [
            {
                label: this.contributorName,
                data: this.contributorCommitsData,
            },
        ];

        // Set chart option
        this.contributorChartOptions = {
            title: {
                display: true,
                text: 'Commits by time',
            },
            responsive: true,
            tooltips: {
                mode: 'index',
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                            precision: 0,
                        },
                    },
                ],
            },
        };
    }

    /**
     * @description Set date from filter string date
     * @param sinceUntilDate
     * @return Date
     * Return the well formed date
     */
    private static setSinceUntilDate(sinceUntilDate: string): Date {
        return moment.utc(sinceUntilDate, 'DD-MM-YYYY').toDate();
    }

    /**
     * @description Set data for contributor chart
     */
    private setDataForChart(): void {
        let date: Date = new Date();
        date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        // Index to cut the chart in several parts (1 per week)
        let bOT = 1;

        // Let's go back to the beginning of the period
        date.setTime(this.since.getTime());

        // Set the chart gap
        const period: number = this.until.getTime() - this.since.getTime();
        this.setPeriodDuration(period);

        // For each single bunch of time, fill the labels field with the date of all the period (if date is out of bounds, her is reset)
        for (let i = 0; i < bOT; i++) {
            if (date > this.until) {
                date = this.until;
            }

            const dateTest = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
            if (i + 1 === bOT && date < this.until) {
                bOT++;
            }
            this.contributorDateArray.push(dateTest);
            this.contributorChartLabel.push(date.getDate() + '/' + (date.getMonth() + 1));

            date.setTime(date.getTime() + this.PERIODDURATION);
        }

        for (let i = 0; i < this.contributorDateArray.length; i++) {
            this.contributorCommitsData.push(this.getCommitByPeriod(i).length);
        }
    }

    /**
     * Set period duration between each chart gap
     * @param period
     */
    private setPeriodDuration(period: number) {
        const WEEKDURATION = 604800000;
        if (period < 2 * WEEKDURATION) {
            this.PERIODDURATION = WEEKDURATION / 7;
        } else if (period >= 2 * WEEKDURATION && period < 4 * WEEKDURATION * 3) {
            this.PERIODDURATION = WEEKDURATION;
        } else {
            this.PERIODDURATION = 2 * WEEKDURATION;
        }
    }
    /**
     * @description Event handler for the click on the chart on a contributor
     * @param event
     */
    // TODO: Place into contributors service
    public chartClick(event): void {
        if (event.active.length > 0) {
            const index = event.active[0]._index;
            this.contributorPeriodCommits = this.getCommitByPeriod(index);

            if (index === 0) {
                this.contributorPeriodStartDate = this.contributorDateArray[0].toLocaleDateString('fr-FR');
            } else {
                this.contributorPeriodStartDate = this.contributorDateArray[index - 1].toLocaleDateString('fr-FR');
            }
            this.contributorPeriodEndDate = this.contributorDateArray[index].toLocaleDateString('fr-FR');
            this.showCommitsModal();
        }
    }

    /**
     * @description Get the commits done in a period in adequacy with an index
     * @param index
     * @return CommitModel[]
     * Return an array of commits done according the data in contributorModule
     */
    private getCommitByPeriod(index: number): CommitModel[] {
        const currentDate: Date = this.contributorDateArray[index];
        const listCommit: CommitModel[] = [];

        if (index === 0) {
            this.commits.forEach((commit: CommitModel) => {
                const commitDate = new Date(commit.createdAt);
                if (ContributorChartComponent.compareDate(commitDate, currentDate) === 0) {
                    listCommit.push(commit);
                }
            });
            return listCommit;
        }

        const previousDate: Date = this.contributorDateArray[index - 1];
        this.commits.forEach((commit: CommitModel) => {
            const commitDate = new Date(commit.createdAt);
            const compareCurrentDate: number = ContributorChartComponent.compareDate(commitDate, currentDate);
            if (
                ContributorChartComponent.compareDate(commitDate, previousDate) === 1 &&
                (compareCurrentDate === 0 || compareCurrentDate === -1)
            ) {
                listCommit.push(commit);
            }
        });

        return listCommit;
    }

    /**
     * @description Compare two dates, works like a comparator
     * @param date1
     * @param date2
     * @return number
     * Return if dates are equals (0), date1 > date2 (1) or date1 < date2 (-1)
     */
    // TODO: Place into an util file
    private static compareDate(date1: Date, date2: Date): number {
        const date1WithoutTime = new Date(Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate())).getTime();
        const date2WithoutTime = new Date(Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate())).getTime();

        if (date1WithoutTime < date2WithoutTime) {
            return -1;
        } else if (date1WithoutTime > date2WithoutTime) {
            return 1;
        }
        return 0;
    }

    public showCommitsModal(): void {
        this.commitsModalActive = true;
    }

    public hideCommitsModal(): void {
        this.commitsModalActive = false;
    }

    public isCommitsModalActive(): boolean {
        return this.commitsModalActive;
    }
}
