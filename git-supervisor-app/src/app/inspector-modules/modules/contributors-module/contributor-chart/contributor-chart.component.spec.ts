import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorChartComponent } from './contributor-chart.component';

describe('ContributorChartComponent', () => {
  let component: ContributorChartComponent;
  let fixture: ComponentFixture<ContributorChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
