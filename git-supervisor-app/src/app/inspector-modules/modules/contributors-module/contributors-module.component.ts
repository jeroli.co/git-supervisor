import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InspectorModuleService } from '../../services/inspector-module.service';
import { NotificationsService } from '../../../notifications/services/notifications.service';
import { WorkspaceModel } from '../../../dashboard/workspace/models/workspace.model';
import { RepositoryModel } from '../../../dashboard/repository/models/repository.model';
import { ModulesFiltersModel } from '../../modules-filters/models/modules-filters.model';
import { ModulesFiltersService } from '../../modules-filters/services/modules-filters.service';
import { ContributorModuleDataModel } from './models/contributor-module-data.model';
import { CommitModel } from './models/commit.model';
import { HttpInspectorModulesService } from '../../services/http-inspector-modules.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-contributors-module',
    templateUrl: './contributors-module.component.html',
    styleUrls: ['./contributors-module.component.css'],
})
export class ContributorsModuleComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;

    public contributorsModuleData: ContributorModuleDataModel[];
    public contributorsCommits: CommitModel[];

    public contributorSelected: ContributorModuleDataModel;
    public contributorCommits: CommitModel[];

    private contributorModalActive: boolean;
    public contributorModalInput: ContributorModuleDataModel;
    public commitsModalInput: CommitModel[];

    private filters: ModulesFiltersModel;
    private filtersChangedEventSubscription: Subscription;

    public errorLoadingContributors: string = null;
    public contributorsModuleName: string;

    constructor(
        private inspectorModuleService: InspectorModuleService,
        private notificationsService: NotificationsService,
        private httpInspectorModuleService: HttpInspectorModulesService,
        private modulesFiltersService: ModulesFiltersService
    ) {}

    ngOnInit(): void {
        this.contributorsModuleData = null;

        this.contributorsCommits = null;

        this.contributorSelected = null;
        this.contributorCommits = null;

        this.contributorModalActive = false;
        this.contributorModalInput = null;
        this.commitsModalInput = null;

        this.contributorsModuleName = this.inspectorModuleService.contributorsModuleName;

        this.filtersChangedEventSubscription = this.modulesFiltersService.modulesFiltersChangeEvent.subscribe(
            (filters: ModulesFiltersModel) => {
                this.filters = filters;
                this.getContributorsModuleData();
            }
        );

        this.getContributorsModuleData();
    }

    public getContributorsModuleData(): void {
        if (this.inspectorModuleService.isModuleEnabled(this.currentWorkspace.modules, 'ContributorsModule')) {
            this.httpInspectorModuleService
                .getModuleData(
                    this.currentWorkspace._id,
                    this.currentRepository._id,
                    this.inspectorModuleService.contributorsModuleName,
                    this.filters
                )
                .subscribe(
                    (contributors: ContributorModuleDataModel[]) => {
                        this.contributorsModuleData = contributors;
                        this.getContributorsCommits();
                        if (this.contributorSelected) {
                            this.selectContributor(this.contributorSelected);
                        }
                    },
                    (error: HttpErrorResponse) => {
                        this.errorLoadingContributors = 'Cannot load contributors data. ' + error.message;
                    }
                );
        }
    }

    private getContributorsCommits() {
        const commits: CommitModel[] = [];
        this.contributorsModuleData.forEach((arrayCommits) => {
            arrayCommits.commits.forEach((commit) => {
                commits.push(commit);
            });
        });
        this.contributorsCommits = commits;
    }

    private getContributorCommits(contributorEmail: string[]): CommitModel[] {
        const contributor = this.contributorsModuleData.find((contributor) => contributor.email === contributorEmail);
        return contributor ? contributor.commits : null;
    }

    public selectContributor(contributor: ContributorModuleDataModel): void {
        this.contributorSelected = contributor;
        this.contributorCommits = this.getContributorCommits(contributor.email);
    }

    public showContributorCommitsModal(contributor: ContributorModuleDataModel): void {
        this.contributorModalInput = contributor;
        this.commitsModalInput = this.getContributorCommits(contributor.email);
        this.contributorModalActive = true;
    }

    public hideContributorCommitsModal(): void {
        this.contributorModalActive = false;
        this.contributorModalInput = null;
        this.commitsModalInput = null;
    }

    public isContributorCommitsModalActive(): boolean {
        return this.contributorModalActive;
    }

    ngOnDestroy(): void {
        if (this.filtersChangedEventSubscription) {
            this.filtersChangedEventSubscription.unsubscribe();
        }
    }
}
