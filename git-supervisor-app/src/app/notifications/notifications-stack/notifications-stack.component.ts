import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../services/notifications.service';
import { Notification } from '../models/notification';
import { NotificationTypes } from '../models/notification-types';

@Component({
    selector: 'app-notifications-stack',
    templateUrl: './notifications-stack.component.html',
    styleUrls: ['./notifications-stack.component.css'],
})
export class NotificationsStackComponent implements OnInit, OnDestroy {
    public notifications: Notification[];

    private addNotificationSubscription: Subscription;
    private removeNotificationSubscription: Subscription;
    private notifTimer = null;

    constructor(private notificationsService: NotificationsService) {}

    ngOnInit(): void {
        this.notifications = [];

        this.addNotificationSubscription = this.notificationsService.addNotificationEvent.subscribe((notification) => {
            this.notifications.push(notification);
            this.notifTimer = setTimeout(() => {
                this.removeNotification(notification);
            }, 8000);
        });

        this.removeNotificationSubscription = this.notificationsService.removeNotificationEvent.subscribe(
            (notification) => {
                this.notifications.splice(this.notifications.indexOf(notification), 1);
            }
        );
    }

    removeNotification(notification: Notification): void {
        const node = document.querySelector('.notification');
        if (node != null) {
            node.classList.remove('fadeInUp');
            node.classList.add('fadeOutDown');
            this.notifications.shift();
            const handleAnimationEnd = () => {
                node.classList.remove('animated', 'fadeOutDown');
                node.removeEventListener('animationend', handleAnimationEnd);
                this.notificationsService.removeNotification(notification);
            };
            node.addEventListener('animationend', handleAnimationEnd);
        } else {
            clearTimeout(this.notifTimer);
        }
    }

    isSuccess(notification: Notification): boolean {
        return notification.type === NotificationTypes.SUCCESS;
    }

    isInfo(notification: Notification): boolean {
        return notification.type === NotificationTypes.INFO;
    }

    isWarning(notification: Notification): boolean {
        return notification.type === NotificationTypes.WARNING;
    }

    isDanger(notification: Notification): boolean {
        return notification.type === NotificationTypes.DANGER;
    }

    ngOnDestroy(): void {
        this.notifications = [];
        this.addNotificationSubscription.unsubscribe();
        this.removeNotificationSubscription.unsubscribe();
    }
}
