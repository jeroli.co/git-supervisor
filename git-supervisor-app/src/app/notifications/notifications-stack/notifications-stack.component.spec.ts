import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsStackComponent } from './notifications-stack.component';

describe('NotificationsStackComponent', () => {
  let component: NotificationsStackComponent;
  let fixture: ComponentFixture<NotificationsStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
