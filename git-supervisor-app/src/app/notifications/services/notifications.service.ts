import { EventEmitter, Injectable, Output } from '@angular/core';
import { Notification } from '../models/notification';
import { NotificationTypes } from '../models/notification-types';

@Injectable({
    providedIn: 'root',
})
export class NotificationsService {
    @Output() public addNotificationEvent: EventEmitter<Notification> = new EventEmitter<Notification>();
    @Output() public removeNotificationEvent: EventEmitter<Notification> = new EventEmitter<Notification>();

    private addNotification(notification: Notification) {
        this.addNotificationEvent.emit(notification);
    }

    public removeNotification(notification: Notification): void {
        this.removeNotificationEvent.emit(notification);
    }

    public pushSuccess(message: string): void {
        this.addNotification(new Notification(NotificationTypes.SUCCESS, message));
    }

    public pushError(message: string): void {
        this.addNotification(new Notification(NotificationTypes.DANGER, message));
    }

    public pushWarning(message: string): void {
        this.addNotification(new Notification(NotificationTypes.WARNING, message));
    }

    public pushInfo(message: string): void {
        this.addNotification(new Notification(NotificationTypes.INFO, message));
    }
}
