export enum NotificationTypes {
  SUCCESS = 1,
  WARNING,
  DANGER,
  INFO,
}
