import { NotificationTypes } from './notification-types';

export class Notification {
  type:     NotificationTypes;
  message:  string;

  constructor(type: NotificationTypes, message: string) {
    this.type = type;
    this.message = message;
  }
}
