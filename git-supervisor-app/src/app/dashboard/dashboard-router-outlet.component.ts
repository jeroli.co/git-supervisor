import { Component } from '@angular/core';

@Component({
    selector: 'app-dashboard-router-outlet',
    template: ` <router-outlet></router-outlet> `,
    styles: [],
})
export class DashboardRouterOutletComponent {}
