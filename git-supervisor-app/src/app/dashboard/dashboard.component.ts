import { Component, OnDestroy, OnInit } from '@angular/core';
import { WorkspaceModel } from './workspace/models/workspace.model';
import { WorkspaceService } from './workspace/services/workspace.service';
import { DeleteWorkspaceModalService } from './workspace/delete-workspace-modal/services/delete-workspace-modal.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationsService } from '../notifications/services/notifications.service';
import { HttpWorkspaceService } from './workspace/services/http-workspace.service';
import { Subscription } from 'rxjs';
import { VCSClient } from '../../enums/VCSClient';
import { AuthService } from '../authentication/services/auth.service';

const WORKSPACE_MAX_LENGTH_NAME = 20;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
    public vcsClients = { ...VCSClient };

    // All Workspaces
    public workspaces: WorkspaceModel[];

    // Search for particular workspace
    public searchFilter: string;
    // New workspace
    public workspaceToAddName: string;
    public workspaceToAddVCSClient: VCSClient;

    // Renaming
    public workspaceToRename: WorkspaceModel;

    public newWorkspaceName: string;

    // Subscription
    private workspaceDeletedEventSubscription: Subscription;

    constructor(
        private httpWorkspaceService: HttpWorkspaceService,
        private workspaceService: WorkspaceService,
        private notificationsService: NotificationsService,
        private deleteWorkspaceModalService: DeleteWorkspaceModalService,
        private authService: AuthService
    ) {}

    ngOnInit(): void {
        this.workspaces = null;
        this.workspaceToRename = null;
        this.workspaceToAddName = '';
        this.workspaceToAddVCSClient = this.authService.getVCS();

        this.httpWorkspaceService.getWorkspaces().subscribe((workspaces: WorkspaceModel[]) => {
            this.workspaces = workspaces.reverse();
        });

        this.workspaceDeletedEventSubscription = this.workspaceService.workspaceDeletedEvent.subscribe(
            (workspaceId) => {
                const workspaceIndex = this.workspaces.findIndex((ws) => ws._id === workspaceId);
                if (workspaceIndex !== -1) {
                    this.workspaces.splice(workspaceIndex, 1);
                }
            }
        );
    }

    private isWorkspaceNameValid(name: string): boolean {
        let isValid = false;

        if (name.replace(/\s/g, '').length === 0) {
            this.notificationsService.pushError('Cannot add empty workspace');
        } else if (name.length > WORKSPACE_MAX_LENGTH_NAME) {
            this.notificationsService.pushError('Must be less than ' + WORKSPACE_MAX_LENGTH_NAME + ' character');
        } else if (this.isWorkspaceAlreadyExist(name)) {
            this.notificationsService.pushError('Workspace name already exist');
        } else {
            isValid = true;
        }

        return isValid;
    }

    private isWorkspaceAlreadyExist(name: string): boolean {
        const workspaceIndex = this.workspaces.findIndex((workspace) => workspace.name === name);
        return workspaceIndex !== -1;
    }

    public addWorkspace(): void {
        if (this.isWorkspaceNameValid(this.workspaceToAddName)) {
            const workspace = new WorkspaceModel();
            workspace.name = this.workspaceToAddName;
            workspace.vcs = this.workspaceToAddVCSClient;
            this.httpWorkspaceService.addWorkspace(workspace).subscribe(
                (ws) => {
                    this.workspaces.unshift(ws);
                    this.workspaceToAddName = '';
                    this.notificationsService.pushSuccess('Workspace successfully added');
                },
                (error) => {
                    if (error instanceof HttpErrorResponse) {
                        if (error.status === 500) {
                            this.notificationsService.pushError('Workspace not added');
                        }
                    }
                }
            );
        }
    }

    public renameWorkspace(workspaceToRename: WorkspaceModel): void {
        if (this.isWorkspaceNameValid(this.newWorkspaceName)) {
            const oldName = workspaceToRename.name;
            workspaceToRename.name = this.newWorkspaceName;
            this.httpWorkspaceService.updateWorkspace(workspaceToRename).subscribe(
                (workspaceRenamed) => {
                    this.workspaces.splice(
                        this.workspaces.findIndex((ws) => workspaceRenamed._id === ws._id),
                        1,
                        workspaceRenamed
                    );
                    if (this.isRenameActive(workspaceRenamed)) {
                        this.disactiveRenameWorkspace();
                    }
                    this.notificationsService.pushSuccess('Workspace renamed');
                },
                (error) => {
                    if (error instanceof HttpErrorResponse) {
                        if (error.status === 500) {
                            this.notificationsService.pushError('Cannot rename workspace');
                        }
                    }
                    workspaceToRename.name = oldName;
                }
            );
        }
    }

    public openWorkspace(workspace: WorkspaceModel): void {
        this.workspaceService.openWorkspace(workspace);
    }

    public exportToJSON(workspace: WorkspaceModel): void {
        this.workspaceService.exportWorkspaceConfigurations(workspace._id);
    }

    public openWorkspaceSettings(workspace: WorkspaceModel): void {
        this.workspaceService.openWorkspaceSettings(workspace._id);
    }

    public activeRenameWorkspace(workspace: WorkspaceModel): void {
        this.newWorkspaceName = workspace.name;
        this.workspaceToRename = workspace;
    }

    public disactiveRenameWorkspace(): void {
        this.workspaceToRename = null;
        this.newWorkspaceName = '';
    }

    public isRenameActive(workspace: WorkspaceModel) {
        return this.workspaceToRename && this.workspaceToRename._id === workspace._id;
    }

    public clearSearch(): void {
        this.searchFilter = '';
    }

    public showDeleteWorkspaceModal(workspace: WorkspaceModel): void {
        this.deleteWorkspaceModalService.showDeleteWorkspaceModal(workspace);
    }

    public isDeleteWorkspaceModalActive(): boolean {
        return this.deleteWorkspaceModalService.isDeleteWorkspaceModalActive();
    }

    ngOnDestroy(): void {
        this.workspaceDeletedEventSubscription.unsubscribe();
    }
}
