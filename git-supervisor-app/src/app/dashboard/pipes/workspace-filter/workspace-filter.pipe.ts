import { Pipe, PipeTransform } from '@angular/core';
import { WorkspaceModel } from '../../workspace/models/workspace.model';

@Pipe({
  name: 'workspaceFilter',
  pure: false
})
export class WorkspaceFilterPipe implements PipeTransform {

  transform(workspaces: WorkspaceModel[], args?: any): any {
    if (args) {
      const filter = args.toString().toLocaleLowerCase();
      const regex = new RegExp(filter);
      return workspaces.filter(workspace => regex.test(workspace.name.toLocaleLowerCase()));
    } else {
      return workspaces;
    }
  }

}
