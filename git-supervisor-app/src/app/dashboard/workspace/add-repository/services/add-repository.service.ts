import { EventEmitter, Injectable, Output } from '@angular/core';
import { RepositoryModel } from '../../../repository/models/repository.model';

@Injectable({
    providedIn: 'root',
})
export class AddRepositoryService {
    private addRepositoryModalActive: boolean;

    @Output() public addRepositoryClickEvent: EventEmitter<RepositoryModel> = new EventEmitter();

    constructor() {
        this.addRepositoryModalActive = false;
    }

    showAddRepositoriesModal() {
        this.addRepositoryModalActive = true;
    }

    hideAddRepositoriesModal() {
        this.addRepositoryModalActive = false;
    }

    isAddRepositoryModalActive() {
        return this.addRepositoryModalActive;
    }

    addRepositoryClick(repository: RepositoryModel) {
        this.addRepositoryClickEvent.emit(repository);
    }
}
