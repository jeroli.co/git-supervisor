import { TestBed, inject } from '@angular/core/testing';

import { AddRepositoryService } from './add-repository.service';

describe('AddRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddRepositoryService]
    });
  });

  it('should be created', inject([AddRepositoryService], (service: AddRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
