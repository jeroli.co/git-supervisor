import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { RepositoryModel } from '../../repository/models/repository.model';
import { WorkspaceModel } from '../models/workspace.model';
import { Subscription } from 'rxjs';
import { RepositoryService } from '../../repository/services/repository.service';
import { AddRepositoryService } from './services/add-repository.service';
import { UserRepositoriesModel } from './models/user-repositories.model';

@Component({
    selector: 'app-add-repository',
    templateUrl: './add-repository.component.html',
    styleUrls: ['./add-repository.component.css'],
})
export class AddRepositoryComponent implements OnInit, OnDestroy {
    @Input() public currentWorkspace: WorkspaceModel;

    // Search by url
    public searchInput: string;
    public foundedRepositories: RepositoryModel[];

    // Get user repo
    public userRepositories: UserRepositoriesModel;

    private addRepositoryClickSubscription: Subscription;
    private userRepositoriesSubscription: Subscription;
    private searchedRepositoriesSubscription: Subscription;
    private addRepositorySubscription: Subscription;

    constructor(private repositoryService: RepositoryService, private addRepositoryService: AddRepositoryService) {}

    ngOnInit() {
        this.searchInput = '';
        this.foundedRepositories = [];
        this.userRepositories = null;

        this.addRepositoryClickSubscription = this.addRepositoryService.addRepositoryClickEvent.subscribe(
            (repository: RepositoryModel) => {
                this.addRepository(repository);
            }
        );

        this.userRepositoriesSubscription = this.repositoryService.userRepositoriesEvent.subscribe((repositories) => {
            this.userRepositories = this.filterAlreadyAddedUserRepositories(repositories);
        });

        this.addRepositorySubscription = this.repositoryService.addedRepositoryEvent.subscribe((repository) => {
            this.deleteRepositoryFromAllRepositories(repository);
        });

        this.repositoryService.getUserRepositories(this.currentWorkspace._id);
    }

    public searchRepositories(): void {
        if (this.searchedRepositoriesSubscription) {
            this.searchedRepositoriesSubscription.unsubscribe();
        }

        if (this.searchInput.replace(/\s/g, '').length === 0) {
            return;
        }

        if (this.searchInput.length > 2) {
            this.searchedRepositoriesSubscription = this.repositoryService.searchedRepositoriesEvent.subscribe(
                (repositories) => {
                    this.foundedRepositories = this.filterAlreadyAddedRepositories(repositories);
                }
            );
            this.repositoryService.searchRepositories(this.currentWorkspace._id, this.searchInput);
        }
    }

    private filterAlreadyAddedRepositories(repositories: RepositoryModel[]) {
        const repositoriesFiltered: RepositoryModel[] = [];

        repositories.forEach((repository) => {
            if (!this.repositoryAlreadyAdded(repository)) {
                repositoriesFiltered.push(repository);
            }
        });

        return repositoriesFiltered;
    }

    private filterAlreadyAddedUserRepositories(userRepositories: UserRepositoriesModel): UserRepositoriesModel {
        const userRepositoriesFiltered = new UserRepositoriesModel();

        userRepositoriesFiltered.owned = this.filterAlreadyAddedRepositories(userRepositories.owned);
        userRepositoriesFiltered.starred = this.filterAlreadyAddedRepositories(userRepositories.starred);
        userRepositoriesFiltered.memberOf = this.filterAlreadyAddedRepositories(userRepositories.memberOf);

        return userRepositoriesFiltered;
    }

    public repositoryAlreadyAdded(repository: RepositoryModel) {
        return this.currentWorkspace.repositories.find((repo) => repo.gitId === repository.gitId) !== undefined;
    }

    public noRepositoriesFound(): boolean {
        return this.foundedRepositories.length === 0;
    }

    public noRepositoriesOwned(): boolean {
        return this.userRepositories.owned.length === 0;
    }

    public noRepositoriesStarred(): boolean {
        return this.userRepositories.starred.length === 0;
    }

    public noRepositoriesMemberOf(): boolean {
        return this.userRepositories.memberOf.length === 0;
    }

    public hideAddRepositoriesModal() {
        this.addRepositoryService.hideAddRepositoriesModal();
    }

    private addRepository(repository: RepositoryModel) {
        this.repositoryService.addRepository(this.currentWorkspace._id, repository);
    }

    private deleteRepositoryFromAllRepositories(repository: RepositoryModel) {
        let repoInd = this.foundedRepositories.findIndex((repo) => repo._id === repository._id);
        if (repoInd !== -1) {
            this.foundedRepositories.splice(repoInd, 1);
        }

        repoInd = this.userRepositories.owned.findIndex((repo) => repo._id === repository._id);
        if (repoInd !== -1) {
            this.userRepositories.owned.splice(repoInd, 1);
        }

        repoInd = this.userRepositories.starred.findIndex((repo) => repo._id === repository._id);
        if (repoInd !== -1) {
            this.userRepositories.starred.splice(repoInd, 1);
        }

        repoInd = this.userRepositories.memberOf.findIndex((repo) => repo._id === repository._id);
        if (repoInd !== -1) {
            this.userRepositories.memberOf.splice(repoInd, 1);
        }
    }

    ngOnDestroy(): void {
        this.userRepositoriesSubscription.unsubscribe();

        if (this.addRepositoryClickSubscription) {
            this.addRepositoryClickSubscription.unsubscribe();
        }

        if (this.addRepositorySubscription) {
            this.addRepositorySubscription.unsubscribe();
        }

        if (this.searchedRepositoriesSubscription) {
            this.searchedRepositoriesSubscription.unsubscribe();
        }

        if (this.addRepositoryService.isAddRepositoryModalActive()) {
            this.hideAddRepositoriesModal();
        }
    }
}
