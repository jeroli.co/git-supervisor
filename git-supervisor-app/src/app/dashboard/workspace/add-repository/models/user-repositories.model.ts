import {RepositoryModel} from "../../../repository/models/repository.model";

export class UserRepositoriesModel {
  owned: RepositoryModel[] = [];
  starred: RepositoryModel[] = [];
  memberOf: RepositoryModel[] = [];
}
