import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoryItemAdderComponent } from './repository-item-adder.component';

describe('RepositoryItemAdderComponent', () => {
  let component: RepositoryItemAdderComponent;
  let fixture: ComponentFixture<RepositoryItemAdderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepositoryItemAdderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryItemAdderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
