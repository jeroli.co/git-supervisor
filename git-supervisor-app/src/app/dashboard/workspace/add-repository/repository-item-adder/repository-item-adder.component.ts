import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RepositoryModel} from "../../../repository/models/repository.model";
import {AddRepositoryService} from "../services/add-repository.service";

@Component({
  selector: 'app-repository-item-adder',
  templateUrl: './repository-item-adder.component.html',
  styleUrls: ['./repository-item-adder.component.css']
})
export class RepositoryItemAdderComponent implements OnInit {

  @Input() public repository: RepositoryModel;

  public MAX_REPO_NAME_LENGTH = 30;

  constructor(
    private addRepositoryService: AddRepositoryService,
  ) { }

  ngOnInit() {
  }

  addRepository() {
    this.addRepositoryService.addRepositoryClick(this.repository);
  }

}
