import { RepositoryModel } from '../../repository/models/repository.model';
import { InspectorModulesModel } from '../../../inspector-modules/models/inspector-modules.model';
import { VCSClient } from '../../../../enums/VCSClient';

export class WorkspaceModel {
    _id: string;
    name: string;
    repositories: RepositoryModel[];
    modules: InspectorModulesModel[];
    vcs: VCSClient;
}
