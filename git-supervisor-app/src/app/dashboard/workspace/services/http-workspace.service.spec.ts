import { TestBed, inject } from '@angular/core/testing';

import { HttpWorkspaceService } from './http-workspace.service';

describe('HttpWorkspaceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpWorkspaceService]
    });
  });

  it('should be created', inject([HttpWorkspaceService], (service: HttpWorkspaceService) => {
    expect(service).toBeTruthy();
  }));
});
