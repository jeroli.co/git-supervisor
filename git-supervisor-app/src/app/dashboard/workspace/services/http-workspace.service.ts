import { Injectable } from '@angular/core';
import { WorkspaceModel } from '../models/workspace.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { InspectorModulesModel } from '../../../inspector-modules/models/inspector-modules.model';
import { InspectorModuleConfigFieldModel } from '../../../inspector-modules/models/inspector-module-config-field.model';
import { ModulesFiltersModel } from '../../../inspector-modules/modules-filters/models/modules-filters.model';
import { ContributorModuleDataModel } from '../../../inspector-modules/modules/contributors-module/models/contributor-module-data.model';

const apiUrl: string = environment.api_addr + '/workspaces';

@Injectable({
    providedIn: 'root',
})
export class HttpWorkspaceService {
    constructor(private http: HttpClient) {}

    public getWorkspaces() {
        return this.http.get<WorkspaceModel[]>(apiUrl);
    }

    public getWorkspace(workspaceId: string) {
        return this.http.get<WorkspaceModel>(apiUrl + '/' + workspaceId);
    }

    /**
     * Get the settings of the workspace in parameter
     * @param workspaceId
     */
    public getWorkspaceSettings(workspaceId: string) {
        return this.http.get<InspectorModulesModel>(apiUrl + '/' + workspaceId + '/modules');
    }

    public addWorkspace(workspace: WorkspaceModel) {
        const formData = new FormData();
        formData.append('workspace', JSON.stringify(workspace));
        return this.http.post<WorkspaceModel>(apiUrl, formData);
    }

    public updateWorkspace(workspace: WorkspaceModel) {
        const formData = new FormData();
        formData.append('workspace', JSON.stringify(workspace));
        return this.http.put<WorkspaceModel>(apiUrl + '/' + workspace._id, formData);
    }

    public deleteWorkspace(workspace: WorkspaceModel) {
        return this.http.delete<string>(apiUrl + '/' + workspace._id);
    }

    public getModules(workspaceId: string) {
        return this.http.get<InspectorModulesModel[]>(apiUrl + '/' + workspaceId + '/modules');
    }

    public getReportData(workspaceId: string) {
        return this.http.get<string>(apiUrl + '/' + workspaceId + '/reportdata');
    }

    public addModule(workspaceId: string, module: InspectorModulesModel) {
        const formData = new FormData();
        formData.append('module', JSON.stringify(module));
        return this.http.post<InspectorModulesModel>(apiUrl + '/' + workspaceId + '/modules', formData);
    }

    public deleteModule(workspaceId: string, moduleName: string) {
        return this.http.delete<string>(apiUrl + '/' + workspaceId + '/modules/' + moduleName);
    }

    public updateEnableModuleState(workspaceId: string, moduleName: string, enabled: boolean) {
        const formData = new FormData();
        formData.append('enabled', JSON.stringify(enabled));
        return this.http.patch<boolean>(apiUrl + '/' + workspaceId + '/modules/' + moduleName, formData);
    }

    public updateSettings(workspaceId: string, modules: string) {
        const formData = new FormData();
        formData.append('modules', JSON.stringify(modules));
        return this.http.post<InspectorModulesModel[]>(apiUrl + '/' + workspaceId + '/configurations', formData);
    }
}
