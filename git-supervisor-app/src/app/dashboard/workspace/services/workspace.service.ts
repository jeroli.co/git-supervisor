import { EventEmitter, Injectable, Output } from '@angular/core';
import { WorkspaceModel } from '../models/workspace.model';
import { HttpWorkspaceService } from './http-workspace.service';
import { NotificationsService } from '../../../notifications/services/notifications.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { InspectorModulesModel } from '../../../inspector-modules/models/inspector-modules.model';
import saveAs from 'file-saver';

@Injectable({
    providedIn: 'root',
})
export class WorkspaceService {
    @Output() workspaceDeletedEvent: EventEmitter<string> = new EventEmitter();
    @Output() moduleAddedEvent: EventEmitter<InspectorModulesModel> = new EventEmitter();

    constructor(
        private httpWorkspaceService: HttpWorkspaceService,
        private notificationsService: NotificationsService,
        private router: Router
    ) {}

    public deleteWorkspace(workspace: WorkspaceModel): void {
        this.httpWorkspaceService.deleteWorkspace(workspace).subscribe(
            (ws_id) => {
                this.workspaceDeletedEvent.emit(ws_id);
                this.notificationsService.pushSuccess('Workspace deleted');
            },
            (error) => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 500) {
                        this.notificationsService.pushError('Cannot delete workspace');
                    }
                }
            }
        );
    }

    /**
     * Create a JSON file with the configurations of the worskpace in parameter and launch the downloading
     * @param workspaceId
     */
    public exportWorkspaceConfigurations(workspaceId: string): void {
        this.httpWorkspaceService.getWorkspaceSettings(workspaceId).subscribe((res) => {
            const blob = new Blob([JSON.stringify(res, null, 4)], {
                type: 'application/json',
            });
            saveAs(blob, 'workspace-settings.json');
        });
    }

    public addModule(workspaceId: string, module: InspectorModulesModel): void {
        this.httpWorkspaceService.addModule(workspaceId, module).subscribe(
            (module: InspectorModulesModel) => {
                this.moduleAddedEvent.emit(module);
                this.notificationsService.pushSuccess('Module added to workspace');
            },
            (error) => {
                this.notificationsService.pushError('Module not added to workspace');
            }
        );
    }

    public openWorkspace(workspace: WorkspaceModel): void {
        this.router.navigate(['dashboard', 'workspace', workspace._id]);
    }

    public openWorkspaceSettings(workspaceId: string): void {
        this.router.navigate(['dashboard', 'workspace', workspaceId, 'modules']);
    }

    public openModuleConfigurations(workspaceId: string, moduleName: string) {
        this.router.navigate(['dashboard', 'workspace', workspaceId, 'configurations', moduleName]);
    }

    public updateSettings(workspaceId: string, modules: string) {
        this.httpWorkspaceService.updateSettings(workspaceId, JSON.parse(modules.toString())).subscribe(
            (modules: InspectorModulesModel[]) => {
                modules.forEach((module) => this.moduleAddedEvent.emit(module));
                this.notificationsService.pushSuccess('Settings file imported');
            },
            (error) => {
                this.notificationsService.pushError('Error : ' + error.error.message);
            }
        );
    }
}
