import {Component, OnDestroy, OnInit} from '@angular/core';
import {WorkspaceModel} from '../models/workspace.model';
import {DeleteWorkspaceModalService} from './services/delete-workspace-modal.service';

@Component({
  selector: 'app-delete-workspace-modal',
  templateUrl: './delete-workspace-modal.component.html',
  styleUrls: ['./delete-workspace-modal.component.css']
})
export class DeleteWorkspaceModalComponent implements OnInit, OnDestroy {

  public workspaceSelected: WorkspaceModel;

  constructor(
    private deleteWorkspaceModalService: DeleteWorkspaceModalService
  ) { }

  ngOnInit() {
    this.workspaceSelected = this.deleteWorkspaceModalService.workspaceSelected;
  }

  hideDeleteWorkspaceModal() {
    this.deleteWorkspaceModalService.hideDeleteWorkspaceModal();
  }

  deleteWorkspace() {
    this.deleteWorkspaceModalService.deleteWorkspace(this.workspaceSelected);
    this.hideDeleteWorkspaceModal();
  }

  ngOnDestroy(): void {
    if (this.deleteWorkspaceModalService.isDeleteWorkspaceModalActive()) {
      this.hideDeleteWorkspaceModal();
    }
  }

}
