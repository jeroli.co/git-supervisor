import { TestBed, inject } from '@angular/core/testing';

import { DeleteWorkspaceModalService } from './delete-workspace-modal.service';

describe('DeleteWorkspaceModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteWorkspaceModalService]
    });
  });

  it('should be created', inject([DeleteWorkspaceModalService], (service: DeleteWorkspaceModalService) => {
    expect(service).toBeTruthy();
  }));
});
