import { Injectable } from '@angular/core';
import { WorkspaceModel } from '../../models/workspace.model';
import { WorkspaceService } from '../../services/workspace.service';

@Injectable({
  providedIn: 'root'
})
export class DeleteWorkspaceModalService {

  private deleteWorkspaceModalActive: boolean;
  private _workspaceSelected: WorkspaceModel;

  constructor(private workspaceService: WorkspaceService) {
    this.deleteWorkspaceModalActive = false;
    this._workspaceSelected = null;
  }

  get workspaceSelected(): WorkspaceModel {
    return this._workspaceSelected;
  }

  set workspaceSelected(value: WorkspaceModel) {
    this._workspaceSelected = value;
  }

  showDeleteWorkspaceModal(workspace: WorkspaceModel) {
    this.workspaceSelected = workspace;
    this.deleteWorkspaceModalActive = true;
  }

  hideDeleteWorkspaceModal() {
    this.workspaceSelected = null;
    this.deleteWorkspaceModalActive = false;
  }

  isDeleteWorkspaceModalActive() {
    return this.deleteWorkspaceModalActive;
  }

  deleteWorkspace(workspace: WorkspaceModel) {
    this.workspaceService.deleteWorkspace(workspace);
  }
}
