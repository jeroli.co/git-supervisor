import { Component, OnDestroy, OnInit } from '@angular/core';
import { WorkspaceModel } from './models/workspace.model';
import { RepositoryModel } from '../repository/models/repository.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { WorkspaceService } from './services/workspace.service';
import { RepositoryService } from '../repository/services/repository.service';
import { AddRepositoryService } from './add-repository/services/add-repository.service';
import { DeleteRepositoryModalService } from '../repository/delete-repository-modal/services/delete-repository-modal.service';
import { HttpWorkspaceService } from './services/http-workspace.service';
import { isEmpty } from '../../utils/strings';

@Component({
    selector: 'app-workspace',
    templateUrl: './workspace.component.html',
    styleUrls: ['./workspace.component.css'],
})
export class WorkspaceComponent implements OnInit, OnDestroy {
    public workspace: WorkspaceModel;

    private deleteRepositorySubscription: Subscription;
    private addRepositorySubscription: Subscription;

    constructor(
        private workspaceService: WorkspaceService,
        private httpWorkspaceService: HttpWorkspaceService,
        private repositoryService: RepositoryService,
        private deleteRepositoryModalService: DeleteRepositoryModalService,
        private addRepositoryService: AddRepositoryService,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.workspace = null;
        const workspaceId = this.route.snapshot.paramMap.get('workspaceId');
        if (!isEmpty(workspaceId)) {
            this.httpWorkspaceService.getWorkspace(workspaceId).subscribe((workspace: WorkspaceModel) => {
                this.workspace = workspace;
            });
        }

        this.addRepositorySubscription = this.repositoryService.addedRepositoryEvent.subscribe((repository) => {
            this.workspace.repositories.push(repository);
        });

        this.deleteRepositorySubscription = this.repositoryService.deletedRepositoryEvent.subscribe(
            (repositoryId: string) => {
                const index = this.workspace.repositories.findIndex((r) => r._id === repositoryId);
                if (index !== -1) {
                    this.workspace.repositories.splice(index, 1);
                }
            }
        );
    }

    public openRepository(repository: RepositoryModel): void {
        this.repositoryService.openRepository(this.workspace._id, repository);
    }

    public openWorkspaceSettings(): void {
        this.workspaceService.openWorkspaceSettings(this.workspace._id);
    }

    public showDeleteRepositoryModal(repository: RepositoryModel): void {
        this.deleteRepositoryModalService.showDeleteRepositoryModal(repository);
    }

    public isDeleteRepositoryModalActive(): boolean {
        return this.deleteRepositoryModalService.isDeleteRepositoryModalActive();
    }

    public showAddRepositoriesModal(): void {
        this.addRepositoryService.showAddRepositoriesModal();
    }

    public isAddRepositoriesModalActive(): boolean {
        return this.addRepositoryService.isAddRepositoryModalActive();
    }

    ngOnDestroy(): void {
        this.deleteRepositorySubscription.unsubscribe();
        this.addRepositorySubscription.unsubscribe();
    }
}
