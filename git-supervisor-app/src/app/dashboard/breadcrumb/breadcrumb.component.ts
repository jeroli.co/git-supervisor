import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkspaceService } from '../workspace/services/workspace.service';
import { RepositoryService } from '../repository/services/repository.service';
import { RepositoryModel } from '../repository/models/repository.model';
import { WorkspaceModel } from '../workspace/models/workspace.model';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.css'],
})
export class BreadcrumbComponent implements OnInit {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;
    public workspaceSettings: boolean;
    public moduleName: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private workspaceService: WorkspaceService,
        private repositoryService: RepositoryService
    ) {}

    ngOnInit() {
        const moduleName = this.route.snapshot.paramMap.get('moduleName');
        if (moduleName) {
            this.workspaceSettings = true;
            this.moduleName = moduleName;
        } else {
            const urlSegments = this.route.snapshot.url;
            if (urlSegments.length > 0) {
                if (urlSegments[urlSegments.length - 1].path === 'modules') {
                    this.workspaceSettings = true;
                }
            }
        }
    }

    openWorkspace() {
        this.workspaceService.openWorkspace(this.currentWorkspace);
    }

    openWorkspaceSettings() {
        this.workspaceService.openWorkspaceSettings(this.currentWorkspace._id);
    }

    openRepository() {
        this.repositoryService.openRepository(this.currentWorkspace._id, this.currentRepository);
    }
}
