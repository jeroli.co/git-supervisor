import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageModuleViewModalComponent } from './manage-module-view-modal.component';

describe('ManageModuleViewModalComponent', () => {
  let component: ManageModuleViewModalComponent;
  let fixture: ComponentFixture<ManageModuleViewModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageModuleViewModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageModuleViewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
