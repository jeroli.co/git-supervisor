import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ManageModuleViewService} from "./services/manage-module-view.service";
import {WorkspaceModel} from "../../workspace/models/workspace.model";
import {InspectorModulesModel} from "../../../inspector-modules/models/inspector-modules.model";

@Component({
  selector: 'app-manage-module-view-modal',
  templateUrl: './manage-module-view-modal.component.html',
  styleUrls: ['./manage-module-view-modal.component.css']
})
export class ManageModuleViewModalComponent implements OnInit, OnDestroy {

  @Input() currentWorkspace: WorkspaceModel;
  @Output() hideModalEvent: EventEmitter<void> = new EventEmitter();

  public localyEnabledModules: { module: InspectorModulesModel, localyEnabled: boolean }[];

  constructor(
    private manageViewModalService: ManageModuleViewService,
  ) { }

  ngOnInit() {
    this.localyEnabledModules = this.manageViewModalService.getLocalyEnabledModules();
  }

  hideManageModulesViewModal() {
    this.hideModalEvent.emit();
  }

  localyEnabledChange(module: { module: InspectorModulesModel, localyEnabled: boolean }) {
    this.manageViewModalService.setLocalChanges(module);
  }

  ngOnDestroy(): void {
    this.localyEnabledModules = [];
    this.hideModalEvent.emit();
  }

}
