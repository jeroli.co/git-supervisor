import {Injectable} from '@angular/core';
import {InspectorModulesModel} from "../../../../inspector-modules/models/inspector-modules.model";

@Injectable({
  providedIn: 'root'
})
export class ManageModuleViewService {

  private localyModuleEnabled: { module: InspectorModulesModel, localyEnabled: boolean }[];

  constructor() {
    this.localyModuleEnabled = [];
  }

  initLocalyEnabledModule(modules: InspectorModulesModel[]) {
    this.localyModuleEnabled = [];
    modules.forEach(module => {
      if (module.enabled) {
        this.localyModuleEnabled.push({ module: module, localyEnabled: module.enabled });
      }
    });
  }

  isModuleLocalyEnabled(moduleName: string) {
    const elem = this.localyModuleEnabled.find(e => e.module.name === moduleName);
    return elem ? elem.localyEnabled : false;
  }

  getLocalyEnabledModules() {
    return this.localyModuleEnabled;
  }

  setLocalChanges(module: { module: InspectorModulesModel, localyEnabled: boolean }) {
    const moduleFinded = this.localyModuleEnabled.find(m => m.module.name === module.module.name);
    moduleFinded.localyEnabled = module.localyEnabled;
  }
}
