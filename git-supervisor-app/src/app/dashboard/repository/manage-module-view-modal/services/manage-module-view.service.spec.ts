import { TestBed, inject } from '@angular/core/testing';

import { ManageModuleViewService } from './manage-module-view.service';

describe('ManageModuleViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManageModuleViewService]
    });
  });

  it('should be created', inject([ManageModuleViewService], (service: ManageModuleViewService) => {
    expect(service).toBeTruthy();
  }));
});
