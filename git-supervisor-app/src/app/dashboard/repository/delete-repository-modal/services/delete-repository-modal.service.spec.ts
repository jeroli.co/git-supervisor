import { TestBed, inject } from '@angular/core/testing';

import { DeleteRepositoryModalService } from './delete-repository-modal.service';

describe('DeleteRepositoryModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteRepositoryModalService]
    });
  });

  it('should be created', inject([DeleteRepositoryModalService], (service: DeleteRepositoryModalService) => {
    expect(service).toBeTruthy();
  }));
});
