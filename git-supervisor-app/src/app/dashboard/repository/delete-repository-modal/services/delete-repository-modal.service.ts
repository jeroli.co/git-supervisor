import { Injectable } from '@angular/core';
import { RepositoryService } from '../../services/repository.service';
import { RepositoryModel } from '../../models/repository.model';

@Injectable({
    providedIn: 'root',
})
export class DeleteRepositoryModalService {
    private deleteRepositoryModalActive: boolean;
    private _repositorySelected: RepositoryModel;

    constructor(private repositoryService: RepositoryService) {
        this.deleteRepositoryModalActive = false;
        this._repositorySelected = null;
    }

    get repositorySelected(): RepositoryModel {
        return this._repositorySelected;
    }

    set repositorySelected(value: RepositoryModel) {
        this._repositorySelected = value;
    }

    showDeleteRepositoryModal(repository: RepositoryModel) {
        this.repositorySelected = repository;
        this.deleteRepositoryModalActive = true;
    }

    hideDeleteRepositoryModal() {
        this.repositorySelected = null;
        this.deleteRepositoryModalActive = false;
    }

    isDeleteRepositoryModalActive() {
        return this.deleteRepositoryModalActive;
    }

    deleteRepository(workspaceId: string, repository: RepositoryModel) {
        this.repositoryService.deleteRepository(workspaceId, repository);
    }
}
