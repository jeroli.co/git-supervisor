import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { RepositoryModel } from '../models/repository.model';
import { DeleteRepositoryModalService } from './services/delete-repository-modal.service';
import { WorkspaceModel } from '../../workspace/models/workspace.model';

@Component({
    selector: 'app-delete-repository-modal',
    templateUrl: './delete-repository-modal.component.html',
    styleUrls: ['./delete-repository-modal.component.css'],
})
export class DeleteRepositoryModalComponent implements OnInit, OnDestroy {
    @Input() currentWorkspace: WorkspaceModel;
    public repositorySelected: RepositoryModel;

    constructor(private deleteRepositoryModalService: DeleteRepositoryModalService) {}

    ngOnInit() {
        this.repositorySelected = this.deleteRepositoryModalService.repositorySelected;
    }

    hideDeleteRepositoryModal() {
        this.deleteRepositoryModalService.hideDeleteRepositoryModal();
    }

    deleteRepository() {
        this.deleteRepositoryModalService.deleteRepository(this.currentWorkspace._id, this.repositorySelected);
        this.hideDeleteRepositoryModal();
    }

    ngOnDestroy(): void {
        if (this.deleteRepositoryModalService.isDeleteRepositoryModalActive()) {
            this.hideDeleteRepositoryModal();
        }
    }
}
