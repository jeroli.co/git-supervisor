import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteRepositoryModalComponent } from './delete-repository-modal.component';

describe('DeleteRepositoryModalComponent', () => {
  let component: DeleteRepositoryModalComponent;
  let fixture: ComponentFixture<DeleteRepositoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteRepositoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRepositoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
