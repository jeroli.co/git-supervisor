import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { HttpRepositoryService } from '../services/http-repository.service';
import { WorkspaceModel } from '../../workspace/models/workspace.model';
import { RepositoryModel } from '../models/repository.model';
import { HttpInspectorModulesService } from '../../../inspector-modules/services/http-inspector-modules.service';
import { InspectorModulesModel } from '../../../inspector-modules/models/inspector-modules.model';
import { ManageModuleViewService } from '../manage-module-view-modal/services/manage-module-view.service';
import { ModulesFiltersService } from '../../../inspector-modules/modules-filters/services/modules-filters.service';

@Component({
    selector: 'app-export-to-html-modal',
    templateUrl: './export-to-html-modal.component.html',
    styleUrls: ['./export-to-html-modal.component.css'],
})
export class ExportToHtmlModalComponent implements OnInit, OnDestroy {
    @Input() currentWorkspace: WorkspaceModel;
    @Input() currentRepository: RepositoryModel;
    @Output() hideExportToHtmlEvent: EventEmitter<void> = new EventEmitter();
    @Output() exportReportToHtmlEvent: EventEmitter<object> = new EventEmitter<{
        sinceDate: string;
        untilDate: string;
        branches: string[];
        modules: string[];
    }>();

    public localyEnabledModules: { module: InspectorModulesModel; localyEnabled: boolean }[];
    private modulesSelected: string[];

    public branches: string[];
    private branchesSelected: string[];
    public branchSelected: string;
    public loadingBranches: boolean;

    private sinceDateSelected: string;
    public sinceDateSelectedConverted: string;
    private untilDateSelected: string;
    public untilDateSelectedConverted: string;

    constructor(
        private httpRepositoryService: HttpRepositoryService,
        private httpInspectorModulesService: HttpInspectorModulesService,
        private manageViewModalService: ManageModuleViewService,
        private modulesFiltersService: ModulesFiltersService
    ) {}

    ngOnInit(): void {
        this.loadingBranches = false;
        this.localyEnabledModules = this.manageViewModalService.getLocalyEnabledModules();
        this.modulesSelected = [];
        this.branches = [];
        this.branchesSelected = [];
        this.branchSelected = this.modulesFiltersService.branchSelected;
        this.branchesSelected.push(this.modulesFiltersService.branchSelected);
        this.sinceDateSelected = this.modulesFiltersService.sinceDateSelected;
        this.sinceDateSelectedConverted = this.sinceDateSelected.split('-').reverse().join('-');
        this.untilDateSelected = this.modulesFiltersService.untilDateSelected;
        this.untilDateSelectedConverted = this.untilDateSelected.split('-').reverse().join('-');

        for (const module of this.localyEnabledModules) {
            if (module.localyEnabled) {
                this.modulesSelected.push(module.module.name);
            }
        }

        this.httpRepositoryService
            .getBranches(this.currentWorkspace._id, this.currentRepository._id)
            .subscribe((branches) => {
                this.branches = branches;
                this.loadingBranches = true;
            });
    }

    ngOnDestroy(): void {
        this.hideExportToHtmlEvent.emit();
    }

    hideExportToHtmlModal(): void {
        this.hideExportToHtmlEvent.emit();
    }

    export(): void {
        this.exportReportToHtmlEvent.emit({
            sinceDate: this.sinceDateSelectedConverted,
            untilDate: this.untilDateSelectedConverted,
            branches: this.branchesSelected,
            modules: this.modulesSelected,
        });
        this.hideExportToHtmlEvent.emit();
    }

    changeBranchesSelected(branch: string): void {
        if (this.branchesSelected.includes(branch)) {
            this.branchesSelected.splice(this.branchesSelected.indexOf(branch), 1);
        } else {
            this.branchesSelected.push(branch);
        }
    }

    changeModulesSelected(moduleName: string): void {
        if (this.modulesSelected.includes(moduleName)) {
            this.modulesSelected.splice(this.modulesSelected.indexOf(moduleName), 1);
        } else {
            this.modulesSelected.push(moduleName);
        }
    }

    onSubmitSince(event: any): void {
        this.sinceDateSelectedConverted = event.target.value;
    }

    onSubmitUntil(event: any): void {
        this.untilDateSelectedConverted = event.target.value;
    }
}
