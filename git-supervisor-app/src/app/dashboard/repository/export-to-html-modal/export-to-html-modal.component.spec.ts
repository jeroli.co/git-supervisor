import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExportToHtmlModalComponent} from './export-to-html-modal.component';

describe('ExportToHtmlModalComponent', () => {
  let component: ExportToHtmlModalComponent;
  let fixture: ComponentFixture<ExportToHtmlModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExportToHtmlModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportToHtmlModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
