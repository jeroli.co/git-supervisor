export class RepositoryModel {
    _id: string;
    gitId: number;
    name: string;
    description: string;
    userId: number;
    owner: string;
    created_at: string;
}
