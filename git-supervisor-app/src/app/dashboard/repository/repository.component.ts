import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { RepositoryModel } from './models/repository.model';
import { ActivatedRoute } from '@angular/router';
import { RepositoryService } from './services/repository.service';
import { WorkspaceModel } from '../workspace/models/workspace.model';
import { HttpWorkspaceService } from '../workspace/services/http-workspace.service';
import saveAs from 'file-saver';

@Component({
    selector: 'app-repository',
    templateUrl: './repository.component.html',
    styleUrls: ['./repository.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class RepositoryComponent implements OnInit {
    @ViewChild('htmlButton', { static: false }) public htmlButton: ElementRef;

    public currentWorkspace: WorkspaceModel;
    public currentRepository: RepositoryModel;
    public reportData: string;
    private loadingHtml: boolean;

    private showExportToHtml: boolean;

    constructor(
        private repositoryService: RepositoryService,
        private httpWorkspaceService: HttpWorkspaceService,
        private route: ActivatedRoute,
        private ref: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.loadingHtml = true;
        this.currentWorkspace = null;
        this.currentRepository = null;
        this.showExportToHtml = false;

        const workspaceId = this.route.snapshot.paramMap.get('workspaceId');
        const repositoryId = this.route.snapshot.paramMap.get('repositoryId');

        if (workspaceId && repositoryId) {
            this.httpWorkspaceService.getWorkspace(workspaceId).subscribe((workspace: WorkspaceModel) => {
                this.currentWorkspace = workspace;
                this.currentRepository = this.currentWorkspace.repositories.find(
                    (repository) => repository._id === repositoryId
                );
            });
        }
    }

    exportReportToHTML(event) {
        this.loadingHtml = false;
        this.repositoryService
            .exportReport(
                this.currentWorkspace._id,
                this.currentRepository._id,
                event.sinceDate,
                event.untilDate,
                event.branches,
                event.modules
            )
            .subscribe(async (data: string) => {
                this.reportData = data;
                const blob = new Blob([data], { type: 'text/plain;charset=utf-8' });
                saveAs(blob, 'git-supervisor_report.html');
                this.loadingHtml = true;
            });
    }

    showExportToHtmlModal() {
        this.showExportToHtml = true;
    }

    hideExportToHtml() {
        this.showExportToHtml = false;
    }

    isExportToHtmlModalActive() {
        return this.showExportToHtml;
    }
}
