import { TestBed, inject } from '@angular/core/testing';

import { HttpRepositoryService } from './http-repository.service';

describe('HttpRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpRepositoryService]
    });
  });

  it('should be created', inject([HttpRepositoryService], (service: HttpRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
