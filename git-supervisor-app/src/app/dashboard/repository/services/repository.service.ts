import { EventEmitter, Injectable, Output } from '@angular/core';
import { RepositoryModel } from '../models/repository.model';
import { HttpRepositoryService } from './http-repository.service';
import { Router } from '@angular/router';
import { NotificationsService } from '../../../notifications/services/notifications.service';
import { UserRepositoriesModel } from '../../workspace/add-repository/models/user-repositories.model';

@Injectable({
    providedIn: 'root',
})
export class RepositoryService {
    @Output() repositoryLoadedEvent: EventEmitter<RepositoryModel> = new EventEmitter();
    @Output() addedRepositoryEvent: EventEmitter<RepositoryModel> = new EventEmitter();
    @Output() deletedRepositoryEvent: EventEmitter<string> = new EventEmitter();
    @Output() userRepositoriesEvent: EventEmitter<UserRepositoriesModel> = new EventEmitter();
    @Output() searchedRepositoriesEvent: EventEmitter<RepositoryModel[]> = new EventEmitter();
    @Output() branchesLoadedEvent: EventEmitter<string[]> = new EventEmitter<string[]>();
    @Output() reportDataLoadedEvent: EventEmitter<string> = new EventEmitter<string>();

    constructor(
        private httpRepositoryService: HttpRepositoryService,
        private notificationService: NotificationsService,
        private router: Router
    ) {}

    addRepository(workspaceId: string, repository: RepositoryModel) {
        this.httpRepositoryService.addRepository(workspaceId, repository).subscribe((r) => {
            this.addedRepositoryEvent.emit(r);
        });
    }

    deleteRepository(workspaceId: string, repository: RepositoryModel) {
        this.httpRepositoryService.deleteRepository(workspaceId, repository).subscribe(
            (repositoryId: string) => {
                this.deletedRepositoryEvent.emit(repositoryId);
                this.notificationService.pushSuccess('Repository deleted');
            },
            (error) => {
                this.notificationService.pushError('Cannot delete this repository');
            }
        );
    }

    getRepository(workspaceId: string, repositoryId: string) {
        this.httpRepositoryService.getRepository(workspaceId, repositoryId).subscribe((repository) => {
            this.repositoryLoadedEvent.emit(repository);
        });
    }

    searchRepositories(workspaceId: string, searchInput: string) {
        this.httpRepositoryService.searchRepositories(workspaceId, searchInput).subscribe((repositories) => {
            this.searchedRepositoriesEvent.emit(repositories);
        });
    }

    getUserRepositories(workspaceId: string) {
        this.httpRepositoryService.getUserRepositories(workspaceId).subscribe((userRepositories) => {
            this.userRepositoriesEvent.emit(userRepositories);
        });
    }

    getBranches(workspaceId: string, repositoryId: string) {
        this.httpRepositoryService.getBranches(workspaceId, repositoryId).subscribe((branches: string[]) => {
            this.branchesLoadedEvent.emit(branches);
        });
    }

    public openRepository(workspaceId: string, repository: RepositoryModel) {
        if (repository) {
            this.router.navigate(['/dashboard/workspace/' + workspaceId + '/repository', repository._id]);
        }
    }

    public repositoryError() {
        this.router.navigate(['/404']);
    }

    public exportReport(
        workspaceId: string,
        repositoryId: string,
        since: string,
        until: string,
        branches: Array<string>,
        modules: Array<string>
    ) {
        return this.httpRepositoryService.getReportData(workspaceId, repositoryId, since, until, branches, modules);
    }
}
