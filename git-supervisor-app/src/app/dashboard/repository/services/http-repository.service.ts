import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RepositoryModel } from '../models/repository.model';
import { environment } from '../../../../environments/environment';
import { UserRepositoriesModel } from '../../workspace/add-repository/models/user-repositories.model';
import { CommitModel } from '../../../inspector-modules/modules/contributors-module/models/commit.model';

const apiUrl: string = environment.api_addr;

@Injectable({
    providedIn: 'root',
})
export class HttpRepositoryService {
    constructor(private http: HttpClient) {}

    public searchRepositories(workspaceId: string, name: string) {
        const params = new HttpParams().set('search', name);
        return this.http.get<RepositoryModel[]>(apiUrl + '/workspaces/' + workspaceId + '/search/repositories', { params: params });
    }

    public getUserRepositories(workspaceId: string) {
        return this.http.get<UserRepositoriesModel>(apiUrl + '/workspaces/' + workspaceId + '/repositories');
    }

    public getRepository(workspaceId: string, repositoryId: string) {
        return this.http.get<RepositoryModel>(apiUrl + '/workspaces/' + workspaceId + '/repositories/' + repositoryId);
    }

    public addRepository(workspaceId: string, repository: RepositoryModel) {
        const formData = new FormData();
        formData.append('repository', JSON.stringify(repository));
        return this.http.post<RepositoryModel>(apiUrl + '/workspaces/' + workspaceId + '/repositories', formData);
    }

    public getBranches(workspaceId: string, repositoryId: string) {
        return this.http.get<string[]>(
            apiUrl + '/workspaces/' + workspaceId + '/repositories/' + repositoryId + '/branches'
        );
    }

    public deleteRepository(workspaceId: string, repository: RepositoryModel) {
        return this.http.delete<string>(apiUrl + '/workspaces/' + workspaceId + '/repositories/' + repository._id);
    }

    public getReportData(
        workspaceId: string,
        repositoryId: string,
        since: string,
        until: string,
        branches: Array<string>,
        modules: Array<string>
    ) {
        const form = new FormData();
        form.append('since', since);
        form.append('until', until);
        form.append('Access-Control-Allow-Origin', '*');

        branches.forEach((value) => form.append('branches', value));
        modules.forEach((value) => form.append('modules', value));

        return this.http.post<string>(
            apiUrl + '/workspaces/' + workspaceId + '/repositories/' + repositoryId + '/html',
            form
        );
    }
}
