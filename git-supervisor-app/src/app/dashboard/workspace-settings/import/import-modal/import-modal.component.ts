import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-import-modal',
    templateUrl: './import-modal.component.html',
    styleUrls: ['./import-modal.component.css'],
})
export class ImportModalComponent {
    @Input() file: File;
    @Output() cancelImportEvent: EventEmitter<void> = new EventEmitter();
    @Output() triggerUpdateEvent: EventEmitter<void> = new EventEmitter();

    /**
     * Emitter for cancelling the import
     */
    cancelImport(): void {
        this.cancelImportEvent.emit();
    }

    /**
     * Emitter for triggering an update
     */
    triggerUpdate(): void {
        this.triggerUpdateEvent.emit();
    }
}
