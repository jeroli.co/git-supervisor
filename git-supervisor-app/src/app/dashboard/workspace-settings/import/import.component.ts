import { Component, Input, OnInit } from '@angular/core';
import { WorkspaceService } from '../../workspace/services/workspace.service';
import { NotificationsService } from '../../../notifications/services/notifications.service';

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.css'],
})
export class ImportComponent implements OnInit {
    @Input() workspaceId: string;

    private data: string;
    file: File;
    isModalActive: boolean;

    constructor(private workspaceService: WorkspaceService) {
        this.file = null;
    }

    ngOnInit(): void {
        this.setModalActive(false);
    }

    /**
     * When a file is dropped on the dropzone
     * @param $event
     */
    onFileDropped($event): void {
        this.readFileContent($event);
    }

    /**
     * When a file is selected
     * @param files
     */
    fileBrowseHandler(files): void {
        this.readFileContent(files);
    }

    /**
     * Gets all the files and load only the first one
     * @param files
     */
    readFileContent(files: Array<any>): void {
        const reader = new FileReader();
        reader.onload = () => {
            this.data = reader.result as string;
            this.setModalActive(true);
        };
        reader.readAsText(files[0]);
        this.file = files[0];
    }

    /**
     * Trigger an update for the data
     */
    triggerUpdate(): void {
        this.workspaceService.updateSettings(this.workspaceId, this.data);
        this.setModalActive(false);
    }

    /**
     * Set modal active
     * @param value
     */
    setModalActive(value: boolean): void {
        this.isModalActive = value;
    }

    /**
     * Cancel the import
     */
    cancelImport(): void {
        this.file = null;
        this.data = null;
        this.setModalActive(false);
    }
}
