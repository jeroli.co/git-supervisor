import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';

@Directive({
    selector: '[appImport]',
})
export class ImportDirective {
    @HostBinding('class.fileover') fileOver: boolean;
    @Output() fileDropped = new EventEmitter<any>();

    /**
     * Dragover listener
     * @param evt
     */
    @HostListener('dragover', ['$event']) onDragOver(evt): void {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = true;
    }

    /**
     * Dragleave listener
     * @param evt
     */
    @HostListener('dragleave', ['$event']) public onDragLeave(evt): void {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
    }

    /**
     * Drop listener
     * @param evt
     */
    @HostListener('drop', ['$event']) public ondrop(evt): void {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
        const files = evt.dataTransfer.files;
        this.fileDropped.emit(files);
    }
}
