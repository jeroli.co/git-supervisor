import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddModulesModalComponent } from './add-modules-modal.component';

describe('AddModulesModalComponent', () => {
  let component: AddModulesModalComponent;
  let fixture: ComponentFixture<AddModulesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddModulesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddModulesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
