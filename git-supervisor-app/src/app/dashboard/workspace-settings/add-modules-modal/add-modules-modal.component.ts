import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AddModulesModalService } from './services/add-modules-modal.service';
import { InspectorModulesModel } from '../../../inspector-modules/models/inspector-modules.model';
import { HttpInspectorModulesService } from '../../../inspector-modules/services/http-inspector-modules.service';
import { WorkspaceService } from '../../workspace/services/workspace.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-add-modules-modal',
    templateUrl: './add-modules-modal.component.html',
    styleUrls: ['./add-modules-modal.component.css'],
})
export class AddModulesModalComponent implements OnInit, OnDestroy {
    @Output() addModuleEvent: EventEmitter<InspectorModulesModel> = new EventEmitter<InspectorModulesModel>();

    @Input() modulesAlreadyAdded: InspectorModulesModel[];
    public modulesNotAlreadyAdded: InspectorModulesModel[];

    private moduleAddedEventSubscription: Subscription;

    constructor(
        private addModulesModalService: AddModulesModalService,
        private workspaceService: WorkspaceService,
        private httpInspectorModulesService: HttpInspectorModulesService
    ) {}

    ngOnInit(): void {
        this.modulesNotAlreadyAdded = null;

        this.httpInspectorModulesService.getModuleList().subscribe((modules: InspectorModulesModel[]) => {
            this.modulesNotAlreadyAdded = [];
            modules.forEach((m) => {
                const moduleInd = this.modulesAlreadyAdded.findIndex((module) => module.name === m.name);
                if (moduleInd === -1) {
                    this.modulesNotAlreadyAdded.push(m);
                }
            });
        });

        this.moduleAddedEventSubscription = this.workspaceService.moduleAddedEvent.subscribe(
            (module: InspectorModulesModel) => {
                const moduleInd = this.modulesNotAlreadyAdded.findIndex((m) => m.name === module.name);
                if (moduleInd !== -1) {
                    this.modulesNotAlreadyAdded.splice(moduleInd, 1);
                    if (this.modulesNotAlreadyAdded.length === 0) {
                        this.hideAddModulesModal();
                    }
                }
            }
        );
    }

    public hideAddModulesModal(): void {
        this.addModulesModalService.hideAddModulesModal();
    }

    public addModule(module: InspectorModulesModel): void {
        this.addModuleEvent.emit(module);
    }

    ngOnDestroy(): void {
        this.moduleAddedEventSubscription.unsubscribe();
        if (this.addModulesModalService.isAddModulesModalActive()) {
            this.hideAddModulesModal();
        }
    }
}
