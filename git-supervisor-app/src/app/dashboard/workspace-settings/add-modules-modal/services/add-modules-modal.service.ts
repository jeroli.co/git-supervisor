import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class AddModulesModalService {
    public addModulesModalActive: boolean;

    public showAddModulesModal(): void {
        this.addModulesModalActive = true;
    }

    public hideAddModulesModal(): void {
        this.addModulesModalActive = false;
    }

    public isAddModulesModalActive(): boolean {
        return this.addModulesModalActive;
    }
}
