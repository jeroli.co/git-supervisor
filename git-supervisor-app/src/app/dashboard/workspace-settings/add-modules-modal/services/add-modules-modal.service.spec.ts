import { TestBed, inject } from '@angular/core/testing';

import { AddModulesModalService } from './add-modules-modal.service';

describe('AddModulesModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddModulesModalService]
    });
  });

  it('should be created', inject([AddModulesModalService], (service: AddModulesModalService) => {
    expect(service).toBeTruthy();
  }));
});
