import { Component, OnDestroy, OnInit } from '@angular/core';
import { WorkspaceModel } from '../workspace/models/workspace.model';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkspaceService } from '../workspace/services/workspace.service';
import { Subscription } from 'rxjs';
import { InspectorModulesModel } from '../../inspector-modules/models/inspector-modules.model';
import { AddModulesModalService } from './add-modules-modal/services/add-modules-modal.service';
import { HttpWorkspaceService } from '../workspace/services/http-workspace.service';
import { NotificationsService } from '../../notifications/services/notifications.service';

@Component({
    selector: 'app-workspace-settings',
    templateUrl: './workspace-settings.component.html',
    styleUrls: ['./workspace-settings.component.css'],
})
export class WorkspaceSettingsComponent implements OnInit, OnDestroy {
    public currentWorkspace: WorkspaceModel; // TODO delete

    private moduleAddedEventSubscription: Subscription;

    public workspaceModules: InspectorModulesModel[];

    private deletionActivated: boolean;

    constructor(
        private workspaceService: WorkspaceService,
        private httpWorkspaceService: HttpWorkspaceService,
        private addModulesModalService: AddModulesModalService,
        private notificationsService: NotificationsService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.deletionActivated = false;
    }

    ngOnInit(): void {
        this.currentWorkspace = null;

        const workspaceId = this.route.snapshot.paramMap.get('workspaceId');
        if (workspaceId) {
            this.httpWorkspaceService.getWorkspace(workspaceId).subscribe((workspace: WorkspaceModel) => {
                this.currentWorkspace = workspace;
            });

            this.httpWorkspaceService.getModules(workspaceId).subscribe((modules: InspectorModulesModel[]) => {
                this.workspaceModules = modules;
            });
        }

        this.moduleAddedEventSubscription = this.workspaceService.moduleAddedEvent.subscribe(
            (m: InspectorModulesModel) => {
                this.workspaceModules.push(m);
            }
        );
    }

    /**
     * Trigger export of configurations
     */
    public exportToJSON(): void {
        this.workspaceService.exportWorkspaceConfigurations(this.currentWorkspace._id);
    }

    public addModule(module: InspectorModulesModel): void {
        this.workspaceService.addModule(this.currentWorkspace._id, module);
    }

    public deleteModule(moduleName: string): void {
        this.httpWorkspaceService.deleteModule(this.currentWorkspace._id, moduleName).subscribe((moduleId: string) => {
            const index = this.workspaceModules.findIndex((module) => module.name === moduleId);
            if (index !== -1) {
                this.workspaceModules.splice(index, 1);
                this.notificationsService.pushSuccess('Module deleted');
            }
        });
    }

    public deleteWorkspace(): void {
        this.workspaceService.deleteWorkspace(this.currentWorkspace);
        this.router.navigate(['/dashboard']);
    }

    public moduleEnabledStateChange(module: InspectorModulesModel): void {
        this.httpWorkspaceService
            .updateEnableModuleState(this.currentWorkspace._id, module.name, !module.enabled)
            .subscribe((enabled: boolean) => {
                module.enabled = enabled;
            });
    }

    public openModuleConfigurations(moduleName: string): void {
        this.workspaceService.openModuleConfigurations(this.currentWorkspace._id, moduleName);
    }

    public activeDeletion(): void {
        this.deletionActivated = true;
    }

    public deactivateDeletion(): void {
        this.deletionActivated = false;
    }

    public isDeletionActive(): boolean {
        return this.deletionActivated;
    }

    public showAddModulesModal(): void {
        this.addModulesModalService.showAddModulesModal();
    }

    public isAddModulesModalActive(): boolean {
        return this.addModulesModalService.isAddModulesModalActive();
    }

    ngOnDestroy(): void {
        this.moduleAddedEventSubscription.unsubscribe();
    }
}
