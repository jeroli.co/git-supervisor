export const isEmpty = (s: string): boolean => {
    return s.replace(/\s/g, '').length === 0;
};
