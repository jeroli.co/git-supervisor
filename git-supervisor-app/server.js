//Install express server
const express = require('express');
const path = require('path');
const enforce = require('express-sslify');
const http = require('http');

const app = express();

app.use(enforce.HTTPS({ trustProtoHeader: true }));

app.use(express.static(__dirname + '/dist/git-supervisor-app'));

app.get('/*', function(req,res) {
  res.sendFile(path.join(__dirname+'/dist/git-supervisor-app/index.html'));
});

const PORT = process.env.PORT || 8080;
http.createServer(app).listen(PORT, () => {
  console.log("Server is running on PORT: " + PORT);
});
