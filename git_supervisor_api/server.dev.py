import os
import sys

sys.path.append(".")
from flask import Flask
from git_supervisor_api.api import create_app
from flask_cors import CORS
from flasgger import Swagger

CLIENT_ADDR = ["http://127.0.0.1:4200", "http://localhost:4200"]
app = Flask(__name__, instance_relative_config=True)
config = "development.cfg"
app = create_app(app, config)


swagger = Swagger(app, template_file="template.json")

CORS(app, resources={r"/*": {"origins": CLIENT_ADDR}})

if __name__ == "__main__":
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    app.run(host="127.0.0.1", port=5000)
