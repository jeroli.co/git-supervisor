from flasgger import Swagger
from flask import Flask
from flask_cors import CORS

from git_supervisor_api.api import create_app

app = Flask(__name__, instance_relative_config=True)
app = create_app(app)

swagger = Swagger(app, template_file="template.json")

CLIENT_ADDR = [
    "https://git-supervisor.herokuapp.com",
    "https://git-supervisor-staging.herokuapp.com",
    "https://git-supervisor-review.herokuapp.com",
]
CORS(app, resources={r"/*": {"origins": CLIENT_ADDR}})

if __name__ == "__main__":
    app.run(host="0.0.0.0")
