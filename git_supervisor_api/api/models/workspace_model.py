from bson import ObjectId

from git_supervisor_api.api import users_collection, workspaces_collection
from git_supervisor_api.api.models.user_model import User


class Workspace:
    def __init__(
        self,
        name,
        vcs,
        _id=None,
        modules=[],
        repositories=[],
    ):
        if _id is not None:
            self.id = _id
        else:
            self.id = str(ObjectId())
        self.name = name
        self.repositories = repositories
        self.modules = modules
        self.vcs = vcs

    @staticmethod
    def addWorkspace(userId, workspace):

        if Workspace.getWorkspace(userId=userId, workspaceId=workspace.id) is not None:
            return None
        user_workspaces = Workspace.getWorkspaces(userId=userId)
        if workspace.name in [
            workspace_name["name"] for workspace_name in user_workspaces
        ]:
            return None
        query_user = {"_id": userId}
        value = {"$addToSet": {"workspaces": workspace.id}}
        result = users_collection.update_one(query_user, value)
        if result.modified_count == 0:
            return None
        workspace.modules = []
        result = workspaces_collection.insert_one(workspace.json())
        if result.inserted_id is None:
            return None
        return workspace

    @staticmethod
    def updateWorkspace(userId, workspaceId, updatedWorkspace):
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        query = {"_id": workspaceId}
        result = workspaces_collection.replace_one(query, updatedWorkspace.json())
        if result.modified_count == 0 and result.upserted_id is None:
            return None
        return updatedWorkspace

    @staticmethod
    def deleteWorkspace(userId, workspaceId):
        from git_supervisor_api.api.models.repository_model import Repository

        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        repositories = workspace.repositories
        for repo in repositories:
            result = Repository.deleteRepository(
                userId=user.id, workspaceId=workspace.id, repositoryId=repo
            )
            if result is None:
                return None
        result = users_collection.update_one(
            {"workspaces": workspace.id}, {"$pull": {"workspaces": workspace.id}}
        )
        if result.modified_count == 0:
            return None
        result = workspaces_collection.delete_one({"_id": workspace.id})
        if result.deleted_count == 0:
            return None
        return workspace.id

    @staticmethod
    def getWorkspace(userId, workspaceId):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        query = {"_id": workspaceId}
        results = workspaces_collection.find_one(query)
        if results is None:
            return None
        return Workspace(**results)

    @staticmethod
    def getWorkspaces(userId):
        user = User.getUserById(userId)
        user_workspaces = user.workspaces
        workspaces_list = []
        for r in user_workspaces:
            query = {"_id": r}
            workspace = workspaces_collection.find_one(query)
            if workspace is None:
                continue
            workspaces_list.append(Workspace(**workspace).json())
        return workspaces_list

    @staticmethod
    def getAllWorkspaces():
        return list(workspaces_collection.find({}))

    @staticmethod
    def getWorkspaceModules(userId, workspaceId):
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        return workspace.modules

    @staticmethod
    def updateWorkspaceModules(workspaceId, modules, userId=None):
        query = {"_id": workspaceId}
        if userId is not None:
            workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
            if workspace is None:
                return None
        else:
            results = workspaces_collection.find_one(query)
            if results is None:
                return None

        value = {"$set": {"modules": modules}}
        result = workspaces_collection.update_one(query, value)
        if result.modified_count == 0:
            return None
        return modules

    def json(self):
        return {
            "_id": self.id,
            "name": self.name,
            "repositories": self.repositories,
            "modules": self.modules,
            "vcs": self.vcs,
        }

    def full(self):
        from git_supervisor_api.api.models.repository_model import Repository

        self.repositories = Repository.getRepositories(self)
        return self

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
