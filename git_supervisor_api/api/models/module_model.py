import abc
from abc import ABC
from dataclasses import dataclass

from git_supervisor_api.api import modules_collection, workspaces_collection
from git_supervisor_api.api.models.user_model import User
from git_supervisor_api.api.models.workspace_model import Workspace


@dataclass
class Module(ABC):
    @property
    @abc.abstractmethod
    def name(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def description(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def enabled(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def configurations(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def data(self):
        raise NotImplementedError

    @staticmethod
    def createModule(module):
        query = {"name": module["name"]}
        value = {
            "name": module["name"],
            "description": module["description"],
            "enabled": module["enabled"],
            "configurations": module["configurations"],
            "data": module["data"],
        }
        result = modules_collection.replace_one(query, value, upsert=True)
        if result.modified_count == 0 and result.upserted_id is None:
            return None
        return module

    @staticmethod
    def addModule(userId, workspaceId, module):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspaceId}
        value = {"$addToSet": {"modules": module}}
        result = workspaces_collection.update_one(query, value)
        if result.modified_count == 0:
            return None
        return module

    @staticmethod
    def deleteModule(userId, workspaceId, moduleName):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspace.id}
        value = {"$pull": {"modules": {"name": moduleName}}}
        result = workspaces_collection.update_one(query, value)
        if result.modified_count == 0:
            return None
        return moduleName

    @staticmethod
    def getModule(userId, workspaceId, module_name):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
        if workspace is None:
            return None
        if not workspace.modules:
            return {}
        module = [
            module for module in workspace.modules if module["name"] == module_name
        ][0]
        return module

    @staticmethod
    def updateModuleEnabled(userId, workspaceId, moduleName, enabled):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspaceId, "modules": {"$elemMatch": {"name": moduleName}}}
        value = {"$set": {"modules.$.enabled": enabled}}
        result = workspaces_collection.update_one(query, value, False, True)
        if result.matched_count == 0:
            return None
        return enabled

    @staticmethod
    def updateModuleConfigurations(userId, workspaceId, module_name, configs):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspaceId, "modules": {"$elemMatch": {"name": module_name}}}
        value = {"$set": {"modules.$.configurations": configs}}
        result = workspaces_collection.update_one(query, value, False, True)
        if result.matched_count == 0:
            return None
        return configs

    @staticmethod
    def addModuleConfiguration(userId, workspaceId, module_name, config):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspaceId, "modules": {"$elemMatch": {"name": module_name}}}
        value = {"$addToSet": {"modules.$.configurations": config}}
        result = workspaces_collection.update_one(query, value, False, True)
        if result.matched_count == 0:
            return None
        return config

    @staticmethod
    def deleteModuleConfiguration(userId, workspaceId, module_name, configId):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        query = {"_id": workspaceId, "modules": {"$elemMatch": {"name": module_name}}}
        value = {"$pull": {"modules.$.configurations": {"id": configId}}}
        result = workspaces_collection.update_one(query, value, False, True)
        if result.matched_count == 0:
            return None
        return configId

    @staticmethod
    def getModules():
        return list(modules_collection.find({}, {"_id": False}))

    def json(self):
        return {
            "name": self.name,
            "description": self.description,
            "enabled": self.enabled,
            "data": self.data,
            "configurations": [m.__dict__ for m in self.configurations],
        }


class ModuleConfiguration:
    def __init__(self, id, label, value):
        self.id = id
        self.label = label
        self.value = value
