import random

from bson import json_util, ObjectId

from git_supervisor_api.api import (
    workspaces_collection,
    repositories_collection,
)
from git_supervisor_api.api.models.user_model import User
from git_supervisor_api.api.models.workspace_model import Workspace


class Repository:
    def __init__(
        self,
        gitId,
        userId,
        name,
        _id=None,
        owner="",
        description="",
        created_at="",
    ):
        self.gitId = gitId
        self.owner = owner
        self.name = name
        self.description = description
        self.userId = userId
        self.created_at = created_at
        if _id is not None:
            self.id = _id
        else:
            self.id = str(ObjectId())

    def json(self):
        return {
            "_id": self.id,
            "gitId": self.gitId,
            "owner": self.owner,
            "name": self.name,
            "description": self.description,
            "userId": self.userId,
            "created_at": self.created_at,
        }

    @staticmethod
    def getRepository(userId, workspaceId, repositoryId):
        user = User.getUserById(userId)
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if workspace is None:
            return None
        if (
            repositoryId not in workspace.repositories
            or str(workspace.id) not in user.workspaces
        ):
            return None
        query = {"_id": repositoryId}
        results = repositories_collection.find_one(query)
        if results is None:
            return None
        return Repository(**results)

    @staticmethod
    def addRepository(userId, workspaceId, repository):
        user = User.getUserById(userId)
        if workspaceId not in user.workspaces:
            return None
        query = {"_id": workspaceId}
        value = {"$addToSet": {"repositories": repository.id}}
        result = workspaces_collection.update_one(query, value)
        if result.modified_count == 0:
            return None
        query = {"_id": repository.id}
        result = repositories_collection.replace_one(
            query, repository.json(), upsert=True
        )
        if result.modified_count == 0 and result.upserted_id is None:
            return None
        return repository

    @staticmethod
    def deleteRepository(userId, workspaceId, repositoryId):
        user = User.getUserById(userId)
        workspace = Workspace.getWorkspace(userId=userId, workspaceId=workspaceId)
        if (
            repositoryId not in workspace.repositories
            or str(workspace.id) not in user.workspaces
        ):
            return None
        result = repositories_collection.delete_one({"_id": repositoryId})
        if result.deleted_count == 0:
            return None
        result = workspaces_collection.update_one(
            {"repositories": repositoryId},
            {"$pull": {"repositories": repositoryId}},
        )
        if result.modified_count == 0:
            return None

        return repositoryId

    @staticmethod
    def getRepositories(workspace):
        repositories = workspace.repositories
        results = repositories_collection.find({"_id": {"$in": repositories}})
        if results is None:
            return None
        repo_list = []
        for r in results:
            repo_list.append(Repository(**r).json())
        return repo_list
