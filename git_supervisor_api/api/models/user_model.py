import uuid
from enum import Enum

from bson import ObjectId

from git_supervisor_api.api import users_collection


class Vcs(str, Enum):
    GITHUB = "GitHub"
    GITLAB = "GitLab"


class User:
    def __init__(
        self,
        username,
        _id=None,
        workspaces=[],
        access_token_github=None,
        access_token_gitlab=None,
    ):
        self.workspaces = workspaces
        self.username = username
        self.access_token_github = access_token_github
        self.access_token_gitlab = access_token_gitlab
        if _id is not None:
            self.id = _id
        else:
            self.id = str(ObjectId())

    @staticmethod
    def getUserById(id):
        user = users_collection.find_one({"_id": id})
        if user is None:
            return None
        return User(**user)

    @staticmethod
    def getUserByUsername(username):
        user = users_collection.find_one({"username": username})
        if user is None:
            return None
        return User(**user)

    @staticmethod
    def saveUser(user):
        query = {"username": user.username}
        users_collection.replace_one(query, user.json(), upsert=True)
        return user

    def json(self):
        return {
            "_id": self.id,
            "username": self.username,
            "workspaces": self.workspaces,
            "access_token_gitlab": self.access_token_gitlab,
            "access_token_github": self.access_token_github,
        }
