import os
from datetime import timedelta
from flask_jwt_extended import JWTManager
from pymongo import MongoClient

from git_supervisor_api.api import config

users_collection = None
workspaces_collection = None
repositories_collection = None
modules_collection = None
jwt = None


def create_app(app, config_filename: object = None) -> object:
    global jwt, users_collection, workspaces_collection, repositories_collection, modules_collection
    app.config.from_object("git_supervisor_api.api.config")
    if config_filename:
        app.config.from_pyfile(config_filename)

    mongo_db_uri = os.environ.get("MONGODB_URI") or app.config["MONGODB_URI"]
    if mongo_db_uri is None:
        dbClient = MongoClient()
    else:
        dbClient = MongoClient(mongo_db_uri, connect=False, ssl=True)
    db = dbClient[os.environ.get("DB") or app.config["DB"]]

    users_collection = db[config.USERS_COLLECTION]
    workspaces_collection = db[config.WORKSPACES_COLLECTION]
    repositories_collection = db[config.REPOSITORIES_COLLECTION]
    modules_collection = db[config.MODULES_COLLECTION]

    jwt = JWTManager(app)
    app.config["JWT_SECRET_KEY"] = os.environ.get("JWT_SECRET_KEY", "secret-key")
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=2)
    app.config["SWAGGER"] = {"head_text": "<style>.try-out {display: none;}</style>"}

    registerBlueprints(app)

    return app


def registerBlueprints(app):
    with app.app_context():
        from git_supervisor_api.api.controllers import routes
        from git_supervisor_api.api.modules import routes_module

        app.register_blueprint(routes)
        app.register_blueprint(routes_module)
