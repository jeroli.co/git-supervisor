def test_basic_module(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories/5ee7d8aa6c57d0fd7ef6d79d/modules/BasicModule/data",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200


def test_basic_module_not_existing_repository(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories/5ee7d8aa6c57d0fd7ef6d88e/modules/BasicModule/data",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 404
