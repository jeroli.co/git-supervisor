import json


def test_search_repo_gitlab(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/search/repositories",
        headers={"Authorization": "Bearer " + login},
        query_string="search=git-supervisor",
    )
    assert response.status_code == 200
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/search/repositories",
        headers={"Authorization": "Bearer " + login},
        query_string="search=https://gitlab.com/jeroli.co/git-supervisor",
    )
    assert response.status_code == 200


def test_search_repo_github(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d454/search/repositories",
        headers={"Authorization": "Bearer " + login},
        query_string="search=Cheddarr",
    )
    assert response.status_code == 200
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d454/search/repositories",
        headers={"Authorization": "Bearer " + login},
        query_string="search=https://github.com/Jeroli-co/Cheddarr",
    )
    assert response.status_code == 200


def test_search_non_existing_repo(init_db, login, client):
    response = client.get(
        "/repositories",
        headers={"Authorization": "Bearer " + login},
        query_string="search=https://gitlab.com/jeroli.co/git-supeisor",
    )
    assert response.status_code == 404


def test_get_repos_gitlab(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200


def test_get_repos_github(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d123/repositories",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200


def test_add_repo_gitlab(init_db, login, client):
    from git_supervisor_api.api.models.repository_model import Repository

    response = client.post(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79f/repositories",
        headers={"Authorization": "Bearer " + login},
        data={
            "repository": json.dumps(
                {
                    "_id": "5ee7d8aa6c57d0fd7ef6d79a",
                    "owner": "owner_name",
                    "name": "repo1",
                    "description": "here is the description",
                    "userId": "666f6f2d6261722d71757578",
                    "gitId": 34667585,
                }
            )
        },
    )
    assert response.status_code == 201
    repo = Repository(**response.json)
    assert repo.id == "5ee7d8aa6c57d0fd7ef6d79a"
    assert repo.gitId == 34667585
    assert repo.owner == "owner_name"
    assert repo.name == "repo1"
    assert repo.description == "here is the description"
    assert repo.userId == "666f6f2d6261722d71757578"


def test_add_repo_github(init_db, login, client):
    from git_supervisor_api.api.models.repository_model import Repository

    response = client.post(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d25e/repositories",
        headers={"Authorization": "Bearer " + login},
        data={
            "repository": json.dumps(
                {
                    "_id": "5ee7d8aa6c57d0fd7ef6d523",
                    "owner": "owner_name",
                    "name": "repo1",
                    "description": "here is the description",
                    "userId": "666f6f2d6261722d71757578",
                    "gitId": 34667885,
                }
            )
        },
    )
    assert response.status_code == 201
    repo = Repository(**response.json)
    assert repo.id == "5ee7d8aa6c57d0fd7ef6d523"
    assert repo.gitId == 34667885
    assert repo.owner == "owner_name"
    assert repo.name == "repo1"
    assert repo.description == "here is the description"
    assert repo.userId == "666f6f2d6261722d71757578"


def test_get_repository_gitlab(init_db, login, client):
    from git_supervisor_api.api.models.repository_model import Repository

    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories/5ee7d8aa6c57d0fd7ef6d79d",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    repo = Repository(**response.json)
    assert repo.id == "5ee7d8aa6c57d0fd7ef6d79d"
    assert repo.gitId == 10283708
    assert repo.owner == ""
    assert repo.name == "Test project"
    assert repo.description == "bla bla bla"
    assert repo.userId == "666f6f2d6261722d71757578"


def test_get_repository_github(init_db, login, client):
    from git_supervisor_api.api.models.repository_model import Repository

    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d123/repositories/5ee7d8aa6c57d0fd7ef6d295",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    repo = Repository(**response.json)
    assert repo.id == "5ee7d8aa6c57d0fd7ef6d295"
    assert repo.gitId == 10283708
    assert repo.owner == ""
    assert repo.name == "Test project"
    assert repo.description == "bla bla bla"
    assert repo.userId == "666f6f2d6261722d71757578"


def test_get_non_existing_repository(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79e/repositories/5ee7d8aa6c57d0fd7ef6d79j",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 404


def test_get_repository_from_non_existing_workspace(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79v/repositories/5ee7d8aa6c57d0fd7ef6d79d",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 404


def test_delete_repository_from_non_existing_workspace(init_db, login, client):
    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79v/repositories/5ee7d8aa6c57d0fd7ef6d79e",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 404


def test_delete_non_existing_repository(init_db, login, client):
    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories/5ee7d8aa6c57d0fd7ef6d79a",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 500


def test_delete_repository_gitlab(init_db, login, client):
    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e/repositories/5ee7d8aa6c57d0fd7ef6d79e",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    assert response.json == "5ee7d8aa6c57d0fd7ef6d79e"


def test_delete_repository_github(init_db, login, client):
    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d123/repositories/5ee7d8aa6c57d0fd7ef6d55a",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    assert response.json == "5ee7d8aa6c57d0fd7ef6d55a"
