import json


def test_get_workspaces(init_db, login, client):
    from git_supervisor_api.api.models.module_model import Module

    response = client.get("/workspaces", headers={"Authorization": "Bearer " + login})
    assert response.status_code == 200
    assert response.json == [
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d79f",
            "name": "workspace1",
            "repositories": [],
            "modules": Module.getModules(),
            "vcs": "GitLab",
        },
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d80e",
            "name": "workspace2",
            "repositories": [],
            "modules": Module.getModules(),
            "vcs": "GitLab",
        },
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d81e",
            "name": "workspace3",
            "repositories": ["5ee7d8aa6c57d0fd7ef6d79d", "5ee7d8aa6c57d0fd7ef6d79e"],
            "modules": Module.getModules(),
            "vcs": "GitLab",
        },
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d454",
            "name": "github_workspace1",
            "repositories": [],
            "modules": Module.getModules(),
            "vcs": "GitHub",
        },
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d25e",
            "name": "github_workspace2",
            "repositories": [],
            "modules": Module.getModules(),
            "vcs": "GitHub",
        },
        {
            "_id": "5ee7d8aa6c57d0fd7ef6d123",
            "name": "github_workspace3",
            "repositories": ["5ee7d8aa6c57d0fd7ef6d295", "5ee7d8aa6c57d0fd7ef6d55a"],
            "modules": Module.getModules(),
            "vcs": "GitHub",
        },
    ]


def test_add_gitlab_workspace_user(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace

    response = client.post(
        "/workspaces",
        headers={"Authorization": "Bearer " + login},
        data={
            "workspace": json.dumps(
                {
                    "_id": -1,
                    "name": "testWorkspace1",
                    "repositories": [],
                    "modules": [],
                    "vcs": "GitLab",
                }
            )
        },
    )
    workspace = Workspace(**response.json)
    assert response.status_code == 201
    assert workspace.name == "testWorkspace1"
    assert workspace.repositories == []
    assert workspace.vcs == "GitLab"


def test_add_github_workspace_user(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace

    response = client.post(
        "/workspaces",
        headers={"Authorization": "Bearer " + login},
        data={
            "workspace": json.dumps(
                {
                    "_id": -1,
                    "name": "testWorkspaceGithub",
                    "repositories": [],
                    "modules": [],
                    "vcs": "GitHub",
                }
            )
        },
    )
    workspace = Workspace(**response.json)
    assert response.status_code == 201
    assert workspace.name == "testWorkspaceGithub"
    assert workspace.repositories == []
    assert workspace.vcs == "GitHub"


def test_add_workspace_existing_name(init_db, login, client):
    response = client.post(
        "/workspaces",
        headers={"Authorization": "Bearer " + login},
        data={
            "workspace": json.dumps(
                {
                    "_id": -1,
                    "name": "workspace1",
                    "repositories": [],
                    "modules": [],
                    "vcs": "Gitlab",
                }
            )
        },
    )
    assert response.status_code == 500


def test_update_gitlab_workspace(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace

    response = client.put(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79f",
        headers={"Authorization": "Bearer " + login},
        data={
            "workspace": json.dumps(
                {
                    "_id": "5ee7d8aa6c57d0fd7ef6d79f",
                    "name": "testWorkspace2",
                    "repositories": [],
                    "modules": [],
                    "vcs": "Gitlab",
                }
            )
        },
    )
    workspace = Workspace(**response.json)
    assert response.status_code == 200
    assert workspace.name == "testWorkspace2"
    assert workspace.repositories == []
    assert workspace.vcs == "Gitlab"


def test_update_github_workspace(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace

    response = client.put(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d25e",
        headers={"Authorization": "Bearer " + login},
        data={
            "workspace": json.dumps(
                {
                    "_id": "5ee7d8aa6c57d0fd7ef6d25e",
                    "name": "testWorkspace2",
                    "repositories": [],
                    "modules": [],
                    "vcs": "GitHub",
                }
            )
        },
    )
    workspace = Workspace(**response.json)
    assert response.status_code == 200
    assert workspace.name == "testWorkspace2"
    assert workspace.repositories == []
    assert workspace.vcs == "GitHub"


def test_add_repo_to_non_existing_workspace(init_db, login, client):
    response = client.post(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d88f/repositories",
        headers={"Authorization": "Bearer " + login},
        data={
            "repository": json.dumps(
                {
                    "_id": "5ee7d8aa6c57d0fd7ef6d790",
                    "owner": "owner_name",
                    "name": "repo1",
                    "description": "here is the description",
                    "userId": "666f6f2d6261722d71757578",
                    "gitId": 3454759679,
                }
            )
        },
    )
    assert response.status_code == 500


def test_get_gitlab_workspace(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace
    from git_supervisor_api.api.models.module_model import Module

    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d80e",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    workspace = Workspace(**response.json)
    assert workspace.id == "5ee7d8aa6c57d0fd7ef6d80e"
    assert workspace.name == "workspace2"
    assert workspace.repositories == []
    assert workspace.modules == Module.getModules()
    assert workspace.vcs == "GitLab"


def test_get_github_workspace(init_db, login, client):
    from git_supervisor_api.api.models.workspace_model import Workspace
    from git_supervisor_api.api.models.module_model import Module

    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d454",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 200
    workspace = Workspace(**response.json)
    assert workspace.id == "5ee7d8aa6c57d0fd7ef6d454"
    assert workspace.name == "github_workspace1"
    assert workspace.repositories == []
    assert workspace.modules == Module.getModules()
    assert workspace.vcs == "GitHub"


def test_get_non_existing_workspace(init_db, login, client):
    response = client.get(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d79i",
        headers={"Authorization": "Bearer " + login},
    )
    assert response.status_code == 404


def test_delete_gitlab_workspace(init_db, login, client):
    from git_supervisor_api.api import (
        users_collection,
        workspaces_collection,
        repositories_collection,
    )

    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d81e",
        headers={"Authorization": "Bearer " + login},
    )
    assert workspaces_collection.find_one({"_id": "5ee7d8aa6c57d0fd7ef6d81e"}) is None
    assert repositories_collection.find_one({"_id": "5ee7d8aa6c57d0fd7ef6d81e"}) is None
    user = users_collection.find_one({"_id": "666f6f2d6261722d71757578"})
    assert "5ee7d8aa6c57d0fd7ef6d79e" not in user["workspaces"]
    assert response.status_code == 200
    assert response.json == "5ee7d8aa6c57d0fd7ef6d81e"


def test_delete_github_workspace(init_db, login, client):
    from git_supervisor_api.api import (
        users_collection,
        workspaces_collection,
        repositories_collection,
    )

    response = client.delete(
        "/workspaces/5ee7d8aa6c57d0fd7ef6d454",
        headers={"Authorization": "Bearer " + login},
    )
    assert workspaces_collection.find_one({"_id": "5ee7d8aa6c57d0fd7ef6d454"}) is None
    assert repositories_collection.find_one({"_id": "5ee7d8aa6c57d0fd7ef6d454"}) is None
    user = users_collection.find_one({"_id": "666f6f2d6261722d71757578"})
    assert "5ee7d8aa6c57d0fd7ef6d454" not in user["workspaces"]
    assert response.status_code == 200
    assert response.json == "5ee7d8aa6c57d0fd7ef6d454"


def test_delete_not_existing_workspace(init_db, login, client):
    response = client.delete(
        "/workspaces/32632437863", headers={"Authorization": "Bearer " + login}
    )
    assert response.status_code == 500
