def test_json(init_db, login):
    from git_supervisor_api.api.models.repository_model import Repository
    from git_supervisor_api.api.models.workspace_model import Workspace
    from git_supervisor_api.api.models.user_model import User

    repo1 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d79d",
        name="testRepo1",
        description="description1",
        userId="5ee7d8aa6c57d0fd7ef6d65d",
        gitId=467323,
    )
    repo2 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d79e",
        name="testRepo2",
        description="description2",
        userId="5ee7d8aa6c57d0fd7ef6d65d",
        gitId=965421,
    )
    repo3 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d80e",
        name="testRepo3",
        description="description3",
        userId="5ee7d8aa6c57d0fd7ef6d65d",
        gitId=257942,
    )

    workspace1 = Workspace(
        _id="5ee7d8aa6c57d0fd7ef6d79f",
        name="work1",
        repositories=[repo1.id, repo2.id],
        vcs="Gitlab",
    )
    workspace2 = Workspace(
        _id="5ee7d8aa6c57d0fd7ef6d80e",
        name="work2",
        repositories=[repo3.id, repo2.id],
        vcs="Gitlab",
    )

    user = User(
        _id="5ee7d8aa6c57d0fd7ef6d88e",
        username="usernameTest",
        workspaces=[workspace1.id, workspace2.id],
        access_token_github="testtoen",
        access_token_gitlab="testgitlab",
    )

    expected = {
        "_id": "5ee7d8aa6c57d0fd7ef6d88e",
        "username": "usernameTest",
        "workspaces": ["5ee7d8aa6c57d0fd7ef6d79f", "5ee7d8aa6c57d0fd7ef6d80e"],
        "access_token_github": "testtoen",
        "access_token_gitlab": "testgitlab",
    }
    assert user.json() == expected
