def test_json(init_db, login):
    from git_supervisor_api.api.models.repository_model import Repository
    from git_supervisor_api.api.models.workspace_model import Workspace

    repo1 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d88e",
        name="testRepo1",
        description="description1",
        userId="5ee7d8aa6c57d0fd7ef6d90e",
        gitId=257952,
    )
    repo2 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d89e",
        name="testRepo2",
        description="description2",
        userId="5ee7d8aa6c57d0fd7ef6d90e",
        gitId=965421,
    )
    repo3 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d89e",
        name="testRepo3",
        description="description3",
        userId="5ee7d8aa6c57d0fd7ef6d90e",
        gitId=257942,
    )
    workspace1 = Workspace(
        _id="5ee7d8aa6c57d0fd7ef6d72e",
        name="work1",
        repositories=[repo1.id, repo2.id],
        modules=[],
        vcs="GitLab",
    )
    expected = {
        "_id": "5ee7d8aa6c57d0fd7ef6d72e",
        "name": "work1",
        "repositories": ["5ee7d8aa6c57d0fd7ef6d88e", "5ee7d8aa6c57d0fd7ef6d89e"],
        "modules": [],
        "vcs": "GitLab",
    }
    assert workspace1.json() == expected


def test_full_gitlab(init_db, login):
    from git_supervisor_api.api.models.workspace_model import Workspace

    workspace = Workspace(
        _id="5ee7d8aa6c57d0fd7ef6d81e",
        name="workspace3",
        repositories=["5ee7d8aa6c57d0fd7ef6d79d", "5ee7d8aa6c57d0fd7ef6d79e"],
        modules=[],
        vcs="Gitlab",
    )

    expected_workspace = Workspace(
        _id="5ee7d8aa6c57d0fd7ef6d81e",
        name="workspace3",
        repositories=[
            {
                "_id": "5ee7d8aa6c57d0fd7ef6d79d",
                "created_at": "",
                "gitId": 10283708,
                "name": "Test project",
                "description": "bla bla bla",
                "userId": "666f6f2d6261722d71757578",
                "owner": "",
            },
            {
                "_id": "5ee7d8aa6c57d0fd7ef6d79e",
                "created_at": "",
                "gitId": 326568334,
                "name": "Test project2",
                "description": "bla blo bla bla",
                "userId": "666f6f2d6261722d71757578",
                "owner": "",
            },
        ],
        modules=[],
        vcs="Gitlab",
    )

    result = expected_workspace == workspace.full()
    assert result
