def test_json(init_db, login):
    from git_supervisor_api.api.models.repository_model import Repository

    repo1 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d79d",
        name="testRepo1",
        description="description1",
        userId="666f6f2d6261722d71757578",
        gitId=10283708,
    )
    repo2 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d79e",
        name="testRepo2",
        description="description2",
        userId="666f6f2d6261722d71757578",
        gitId=326568334,
    )
    repo3 = Repository(
        _id="5ee7d8aa6c57d0fd7ef6d88e",
        name="testRepo3",
        description="description3",
        userId="666f6f2d6261722d71757578",
        gitId=257942,
    )
    expected1 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d79d",
        "created_at": "",
        "name": "testRepo1",
        "description": "description1",
        "userId": "666f6f2d6261722d71757578",
        "gitId": 10283708,
        "owner": "",
    }
    expected2 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d79e",
        "created_at": "",
        "name": "testRepo2",
        "description": "description2",
        "userId": "666f6f2d6261722d71757578",
        "gitId": 326568334,
        "owner": "",
    }
    expected3 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d88e",
        "created_at": "",
        "name": "testRepo3",
        "description": "description3",
        "userId": "666f6f2d6261722d71757578",
        "gitId": 257942,
        "owner": "",
    }

    assert repo1.json() == expected1
    assert repo2.json() == expected2
    assert repo3.json() == expected3
