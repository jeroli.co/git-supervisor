import os
from pathlib import Path

import pytest
from bson import ObjectId
from flask import Flask
from flask_jwt_extended import JWTManager, create_access_token


username = "jerolico.test"
gitlab_token = None
github_token = None
jwt = None


@pytest.fixture(scope="session")
def client():
    from git_supervisor_api.api import create_app
    from git_supervisor_api import api

    global jwt
    global gitlab_token
    global github_token
    app = Flask(__name__, instance_relative_config=True)
    testing_file = (
        "testing.cfg" if os.path.isfile("../../../instance/testing.cfg") else None
    )
    app = create_app(app, testing_file)
    app.config["TESTING"] = True
    jwt = JWTManager(app)
    testing_client = app.test_client()

    gitlab_token = app.config.get("GITLAB_TEST_API_TOKEN") or os.environ.get(
        "GITLAB_TEST_API_TOKEN"
    )

    github_token = app.config.get("GITHUB_TEST_API_TOKEN") or os.environ.get(
        "GITHUB_TEST_API_TOKEN"
    )

    ctx = app.app_context()
    ctx.push()

    yield testing_client
    ctx.pop()
    api.modules_collection.drop()


@pytest.fixture(scope="function")
def login(client):
    jwtToken = create_access_token(identity=username)
    return jwtToken


@pytest.fixture(scope="function")
def init_db(client):
    from git_supervisor_api import api
    from git_supervisor_api.api.models.module_model import Module

    gitlab_repo1 = {
        "description": "bla bla bla",
        "_id": "5ee7d8aa6c57d0fd7ef6d79d",
        "gitId": 10283708,
        "name": "Test project",
        "userId": "666f6f2d6261722d71757578",
    }

    gitlab_repo2 = {
        "description": "bla blo bla bla",
        "_id": "5ee7d8aa6c57d0fd7ef6d79e",
        "gitId": 326568334,
        "name": "Test project2",
        "userId": "666f6f2d6261722d71757578",
    }

    gitlab_workspace1 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d79f",
        "name": "workspace1",
        "repositories": [],
        "modules": Module.getModules(),
        "vcs": "GitLab",
    }

    gitlab_workspace2 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d80e",
        "name": "workspace2",
        "repositories": [],
        "modules": Module.getModules(),
        "vcs": "GitLab",
    }

    gitlab_workspace3 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d81e",
        "name": "workspace3",
        "repositories": ["5ee7d8aa6c57d0fd7ef6d79d", "5ee7d8aa6c57d0fd7ef6d79e"],
        "modules": Module.getModules(),
        "vcs": "GitLab",
    }

    github_repo1 = {
        "description": "bla bla bla",
        "_id": "5ee7d8aa6c57d0fd7ef6d295",
        "gitId": 10283708,
        "name": "Test project",
        "userId": "666f6f2d6261722d71757578",
    }

    github_repo2 = {
        "description": "bla blo bla bla",
        "_id": "5ee7d8aa6c57d0fd7ef6d55a",
        "gitId": 326568334,
        "name": "Test project2",
        "userId": "666f6f2d6261722d71757578",
    }

    github_workspace1 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d454",
        "name": "github_workspace1",
        "repositories": [],
        "modules": Module.getModules(),
        "vcs": "GitHub",
    }

    github_workspace2 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d25e",
        "name": "github_workspace2",
        "repositories": [],
        "modules": Module.getModules(),
        "vcs": "GitHub",
    }

    github_workspace3 = {
        "_id": "5ee7d8aa6c57d0fd7ef6d123",
        "name": "github_workspace3",
        "repositories": ["5ee7d8aa6c57d0fd7ef6d295", "5ee7d8aa6c57d0fd7ef6d55a"],
        "modules": Module.getModules(),
        "vcs": "GitHub",
    }

    user = {
        "_id": "666f6f2d6261722d71757578",
        "username": "jerolico.test",
        "workspaces": [
            "5ee7d8aa6c57d0fd7ef6d79f",
            "5ee7d8aa6c57d0fd7ef6d80e",
            "5ee7d8aa6c57d0fd7ef6d81e",
            "5ee7d8aa6c57d0fd7ef6d454",
            "5ee7d8aa6c57d0fd7ef6d25e",
            "5ee7d8aa6c57d0fd7ef6d123",
        ],
        "access_token_gitlab": gitlab_token,
        "access_token_github": github_token,
    }

    api.repositories_collection.insert_one(gitlab_repo1)
    api.repositories_collection.insert_one(gitlab_repo2)
    api.workspaces_collection.insert_one(gitlab_workspace1)
    api.workspaces_collection.insert_one(gitlab_workspace2)
    api.workspaces_collection.insert_one(gitlab_workspace3)
    api.repositories_collection.insert_one(github_repo1)
    api.repositories_collection.insert_one(github_repo2)
    api.workspaces_collection.insert_one(github_workspace1)
    api.workspaces_collection.insert_one(github_workspace2)
    api.workspaces_collection.insert_one(github_workspace3)
    api.users_collection.insert_one(user)

    yield None
    api.repositories_collection.drop()
    api.workspaces_collection.drop()
    api.users_collection.drop()
