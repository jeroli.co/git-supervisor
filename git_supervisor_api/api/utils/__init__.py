from flask_jwt_extended import get_jwt_identity
from requests_futures.sessions import FuturesSession
from git_supervisor_api.api.models.user_model import User
from git_supervisor_api.api.config import GITLAB_API_URL, GITHUB_API_URL

"""
 Utility module
"""

base_url_gitlab = GITLAB_API_URL
base_url_github = GITHUB_API_URL


def verify_user():
    """
    Function to verify current user's JWT token
    Returns:
        User: The current user
    """
    username = get_jwt_identity()
    return User.getUserByUsername(username=username)


def async_request_content(urls, access_token=None):
    """
    Function to make asynchronous requests and get responses contents
    Arguments:
        urls: list of urls to make async requests
        access_token: authentication token
    Returns:
        list of responses contents
    """
    with FuturesSession() as session:
        return [
            r.result().json()
            for r in [
                session.get(u, headers={"Authorization": f"token {access_token}"})
                for u in urls
            ]
        ]


def async_request_response(urls, access_token=None):
    """
    Function to make asynchronous requests and get responses objects
    Args:
        urls: list of urls to make async requests
        access_token: authentication token
    Returns:
        list of responses
    """
    with FuturesSession() as session:
        return [
            r.result()
            for r in [
                session.get(u, headers={"Authorization": f"token {access_token}"})
                for u in urls
            ]
        ]


def add_queries(url, **kwargs):
    parameters = kwargs
    query = [
        "{}={}".format(text, value)
        for text, value in parameters.items()
        if text and value and text != "git_id"
    ]

    return url + "&" + "&".join(query)


def make_request_url(base_url, resource, sub_resource="", **kwargs):
    """
    Function to build API request URL
    Args:
        resource: main resource (projects, users...)
        sub_resource: sub resource (repository, issues...)
        base_url: Gitlab or Github URL API
        **kwargs: queries to add
    Returns:
        String URL
    """
    url = (
        base_url
        + "/"
        + resource
        + ("/" + kwargs.get("git_id", "") if kwargs.get("git_id", "") != "" else "")
        + ("/" + sub_resource if sub_resource != "" else "")
        + "?per_page=100"
    )
    url = add_queries(url, **kwargs)
    return url


def make_request_url_gitlab(resource, sub_resource="", **kwargs):
    return make_request_url(base_url_gitlab, resource, sub_resource, **kwargs)


def make_request_url_github(resource, sub_resource="", **kwargs):
    return make_request_url(base_url_github, resource, sub_resource, **kwargs)


def call_gitlab_api(gitlab_url):
    r = async_request_response([gitlab_url])
    total_pages = r[0].headers.get("X-Total-Pages")
    if total_pages is not None:
        urls = []
        for i in range(1, int(total_pages) + 1):
            urls.append(add_queries(gitlab_url, page=i))
        result = async_request_content(urls)
        return [item for sublist in result for item in sublist]

    return async_request_content([gitlab_url])[0]


def call_github_api(github_url, access_token):
    r = async_request_response([github_url], access_token)
    total_pages = r[0].headers.get("X-Total-Pages")
    if total_pages is not None:
        urls = []
        for i in range(1, int(total_pages) + 1):
            urls.append(add_queries(github_url, page=i))
        result = async_request_content(urls, access_token)
        return [item for sublist in result for item in sublist]

    return async_request_content([github_url], access_token)[0]


def get_configuration(configs, id):
    return next(item["value"] for item in configs if item["id"] == id)
