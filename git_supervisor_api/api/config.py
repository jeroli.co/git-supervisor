ENV = "production"
GITLAB_API_URL = "https://gitlab.com/api/v4"
GITLAB_LOGIN_URL = "https://gitlab.com/oauth/token"

GITHUB_API_URL = "https://api.github.com"

DEBUG = False

MONGODB_URI = None
DB = "git-supervisor"

USERS_COLLECTION = "users"
WORKSPACES_COLLECTION = "workspaces"
REPOSITORIES_COLLECTION = "repositories"
MODULES_COLLECTION = "modules"
