import json

import requests
from bs4 import BeautifulSoup
from flask import jsonify
from flask_jwt_extended import jwt_required

from json2html import *
from git_supervisor_api.api.models.repository_model import Repository
from git_supervisor_api.api.models.workspace_model import Workspace
from git_supervisor_api.api.modules.basic_module.api.gitlab import get_basic_info
from git_supervisor_api.api.modules.basic_module.api.github import get_basic_info_github
from git_supervisor_api.api.modules import routes_module
from git_supervisor_api.api.utils import verify_user
from git_supervisor_api.api.models.user_model import Vcs


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/BasicModule/data",
    methods=["GET"],
)
@jwt_required
def get_BasicModule(workspaceId, repositoryId):
    """
    Gets repository basic information
    Returns basic information for a repository (number of commits, tags, branches, ...) if the specified workspace contains the repository.
    ---
      tags:
        - basic module
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository from which to get basic informations
          required: true
          type: string
      responses:
        '401':
          description: User not found
        '404':
          description: Repository not found
        '200':
          description: OK
          schema:
            type: object
            properties:
              data:
                type: object
                properties:
                  license:
                    type: string
                  commits_count:
                    type: integer
                  branches_count:
                    type: integer
                  tags_count:
                    type: integer
                  size:
                    type: number
                    format: float
                  contributors_count:
                    type: integer
                  open_issues_count:
                    type: integer
                  wiki_enabled:
                    type: boolean
                  last_activity_at:
                    type: string
                    format: date
                  url:
                    type: string
                    format: url
                example:
                  license: GNU GPLv3
                  commits_count: 19
                  branches_count: 8
                  tags_count: 3
                  size: 302.34
                  contributors_count: 7
                  open_issues_count: 16
                  wiki_enabled: true
                  last_activity_at: 2013-05-14 17:07:21
                  url: https://gitlab.com/jeroli.co/git-supervisor

    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    repository = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )

    if repository is None:
        return jsonify({"message": "Repository not found"}), 404

    git_id = str(repository.gitId)

    workspace = Workspace.getWorkspace(
        userId=user.id,
        workspaceId=workspaceId,
    )

    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    if workspace.vcs == Vcs.GITLAB:
        access_token = user.access_token_gitlab
        info = get_basic_info(access_token, git_id)
    elif workspace.vcs == Vcs.GITHUB:
        access_token = user.access_token_github
        info = get_basic_info_github(access_token, repository.owner, repository.name)
    else:
        info = None

    if info is None:
        return jsonify({"message": "Repository not found"}), 404

    data = info.__dict__

    return jsonify(data), 200


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/BasicModule/html",
    methods=["POST"],
)
@jwt_required
def export_html_basic(workspaceId, repositoryId):
    """
    Generate a HTML report of the BasicModule of a repository
    Returns a string containing the HTML.
    ---
    tags:
        - basic module
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to export
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository to export
          required: true
          type: string
    responses:
        '401':
          description: User not found
        '200':
          description: OK
          schema:
            type: string
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    # URL
    # apiUrl = request.host_url

    # Token
    # accessToken = request.form.get("token", None)

    # Request
    # url = apiUrl + 'workspaces/' + workspaceId + '/repositories/' + repositoryId + '/modules/BasicModule/data'
    # data = requests.get(url=url, headers={'Authorization': 'Bearer ' + accessToken}).json()

    # print(data.json)

    data = get_BasicModule(workspaceId, repositoryId)[0]

    # HTML
    html = json2html.convert(json=data.json)

    soup = BeautifulSoup(html, features="html5lib")

    table = soup.find("table")
    table["class"] = "customTables"
    del table["border"]

    div = soup.new_tag("div")
    div["id"] = "BasicModule"
    div["class"] = "module"

    div.append(table)
    soup.body.append(div)

    return str(soup), 200
