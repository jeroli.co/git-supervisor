from git_supervisor_api.api.models.module_model import Module, ModuleConfiguration


class BasicModuleData:
    def __init__(
        self,
        license="",
        commits_count=0,
        branches_count=0,
        tags_count=0,
        size=0,
        contributors_count=0,
        open_issues_count=0,
        wiki_enabled=False,
        last_activity_at=None,
        url=None,
    ):
        self.license = license
        self.commits_count = commits_count
        self.branches_count = branches_count
        self.tags_count = tags_count
        self.size = size
        self.contributors_count = contributors_count
        self.open_issues_count = open_issues_count
        self.wiki_enabled = wiki_enabled
        self.last_activity_at = last_activity_at
        self.url = url


class BasicModule(Module):
    name = "BasicModule"
    description = "Shows various information about a repository"
    enabled = True
    configurations = [
        ModuleConfiguration(id="license", label="Show license", value=True),
        ModuleConfiguration(id="commitsCount", label="Show commits count", value=True),
        ModuleConfiguration(
            id="branchesCount", label="Show branches count", value=True
        ),
        ModuleConfiguration(id="tagsCount", label="Show tags count", value=True),
        ModuleConfiguration(id="size", label="Show repository size", value=True),
        ModuleConfiguration(
            id="contributorsCount", label="Show contributors count", value=True
        ),
        ModuleConfiguration(
            id="openIssuesCount", label="Show open issues count", value=True
        ),
        ModuleConfiguration(id="wiki", label="Show wiki state", value=True),
        ModuleConfiguration(
            id="lastActivityAt", label="Show last activity date", value=True
        ),
        ModuleConfiguration(id="url", label="Show repository URL", value=True),
    ]
    data = BasicModuleData().__dict__
