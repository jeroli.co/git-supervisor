from git_supervisor_api.api.modules.basic_module.model import BasicModuleData
from git_supervisor_api.api.utils import (
    make_request_url_github,
    call_github_api,
)


def get_basic_info_github(access_token, owner, repo_name):
    repo_url = make_request_url_github(resource=f"repos/{owner}", git_id=repo_name)

    commits_url = make_request_url_github(resource=f"repos/{owner}", sub_resource="commits", git_id=repo_name)
    contributors_url = make_request_url_github(resource=f"repos/{owner}", sub_resource="contributors", git_id=repo_name)
    branches_url = make_request_url_github(resource=f"repos/{owner}", sub_resource="branches", git_id=repo_name)
    tags_url = make_request_url_github(resource=f"repos/{owner}", sub_resource="tags", git_id=repo_name)

    repo = call_github_api(repo_url, access_token=access_token)
    tags = call_github_api(tags_url, access_token=access_token)
    branches = call_github_api(branches_url, access_token=access_token)
    contributors = call_github_api(contributors_url, access_token=access_token)
    commits = call_github_api(commits_url, access_token=access_token)

    tag_count = len(tags)
    branches_count = len(branches)
    contributor_count = len(contributors)
    commit_count = len(commits)
    license = repo.get("license") or {}
    open_issues_count = repo.get("open_issues_count", 0)
    wiki_enabled = repo.get("has_wiki", False)
    last_activity_at = repo.get("updated_at", "")
    size = repo.get("size", "")
    url = repo.get("html_url", "")

    return BasicModuleData(
        license=license.get("name") or license.get("nickname") or "No license",
        commits_count=int(commit_count),
        branches_count=int(branches_count),
        tags_count=int(tag_count),
        size=float(size),
        contributors_count=int(contributor_count),
        open_issues_count=int(open_issues_count),
        wiki_enabled=wiki_enabled,
        last_activity_at=last_activity_at,
        url=url,
    )
