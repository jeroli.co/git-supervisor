from git_supervisor_api.api.modules.basic_module.model import BasicModuleData
from git_supervisor_api.api.utils import make_request_url_gitlab, async_request_response


def get_basic_info(access_token, git_id):
    urls = [
        make_request_url_gitlab(
            resource="projects",
            sub_resource="repository/tags",
            git_id=git_id,
            access_token=access_token,
        ),
        make_request_url_gitlab(
            resource="projects",
            sub_resource="repository/branches",
            git_id=git_id,
            access_token=access_token,
        ),
        make_request_url_gitlab(
            resource="projects",
            sub_resource="",
            git_id=git_id,
            access_token=access_token,
            statistics="true",
            license="true",
        ),
        make_request_url_gitlab(
            resource="projects",
            sub_resource="repository/contributors",
            git_id=git_id,
            access_token=access_token,
        ),
    ]

    res = async_request_response(urls)

    if "404 Project Not Found" in res[2].json().values():
        return None

    tag_count = res[0].headers.get("X-Total", 0)
    branches_count = res[1].headers.get("X-Total", 0)
    contributor_count = res[3].headers.get("X-Total", 0)
    commit_count = res[2].json().get("statistics", {}).get("commit_count", 0)
    license = res[2].json().get("license") or {}
    open_issues_count = res[2].json().get("open_issues_count", 0)
    wiki_enabled = res[2].json().get("wiki_enabled", False)
    last_activity_at = res[2].json().get("last_activity_at", "")
    size = round(
        res[2].json().get("statistics", {}).get("repository_size", 0) / (1024 * 1024), 2
    )
    url = res[2].json().get("web_url", "")

    return BasicModuleData(
        license=license.get("name") or license.get("nickname") or "No license",
        commits_count=int(commit_count),
        branches_count=int(branches_count),
        tags_count=int(tag_count),
        size=float(size),
        contributors_count=int(contributor_count),
        open_issues_count=int(open_issues_count),
        wiki_enabled=wiki_enabled,
        last_activity_at=last_activity_at,
        url=url,
    )
