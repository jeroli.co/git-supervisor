from collections import defaultdict

from flask import Blueprint
from git_supervisor_api.api.models.workspace_model import Workspace
from git_supervisor_api.api.models.module_model import Module

routes_module = Blueprint("modules", __name__)
modules_dict = defaultdict(list)


def update_workspaces_modules():
    """
    Function called at the launch of the app to update all of the existing
    workspaces' configurations according to those of the modules in de database
    """
    workspaces = Workspace.getAllWorkspaces()
    modules = Module.getModules()

    for workspace in workspaces:
        workspace_modules = workspace["modules"]
        updated_workspace_modules = []

        for workspace_module in workspace_modules:
            if workspace_module["name"] not in [module["name"] for module in modules]:
                continue

            module = [
                module
                for module in modules
                if module["name"] == workspace_module["name"]
            ][0]
            # Evaluator module not concerned by configurations differences
            if workspace_module["name"] != "EvaluatorModule":
                if len(workspace_module.keys()) != len(module.keys()) or len(
                    workspace_module["configurations"]
                ) != len(module["configurations"]):
                    temp_config = workspace_module["configurations"]
                    workspace_module = module
                    # Save config modules
                    iteration = min(
                        len(workspace_module["configurations"]), len(temp_config)
                    )
                    for i in range(iteration):
                        if (
                            temp_config[i]["value"]
                            != workspace_module["configurations"][i]["value"]
                        ):
                            workspace_module["configurations"][i][
                                "value"
                            ] = temp_config[i]["value"]
            updated_workspace_modules.append(workspace_module)

        Workspace.updateWorkspaceModules(
            workspaceId=workspace["_id"], modules=updated_workspace_modules
        )


from .basic_module import routes
from .contributors_module import routes
from .evaluator_module import routes
