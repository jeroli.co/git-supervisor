import json

import requests
from bs4 import BeautifulSoup
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_claims

from git_supervisor_api.api.modules import routes_module, modules_dict
from git_supervisor_api.api.modules.evaluator_module.model import (
    EvaluationCriteria,
    EvaluatorModule,
)
from git_supervisor_api.api.utils import verify_user
from git_supervisor_api.api.models.module_model import Module


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/EvaluatorModule/data",
    methods=["GET"],
)
@jwt_required
def get_EvaluatorModule(workspaceId, repositoryId):
    """
    Gets all evaluator module data from a the workspace and repository IDs

    :param workspaceId: ID of the desired workspace
    :param repositoryId: ID of the desired repository

    :return: a JSON containing all data with data, criteria and if they are fulfilled
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    module = EvaluatorModule()
    module.configurations = EvaluatorModule.getModule(
        userId=user.id, workspaceId=workspaceId, module_name=module.name
    ).get("configurations")
    evaluation = []
    for config in module.configurations:
        criteria = EvaluationCriteria(config)
        function = [
            item[1]
            for item in modules_dict["functions"]
            if item[0] == "get_" + criteria.moduleName
        ][0]
        # TODO Gérer les filtres
        data = function(workspaceId, repositoryId)[0].json
        if isinstance(data, list):
            for d in data:
                values = {}
                for subcriteria in criteria.subcriterias:
                    allData = d[subcriteria.key]
                    values[subcriteria.id] = allData
                fulfilled = module.are_all_subcriteria_fulfilled(criteria, values)
                evaluation.append(
                    {"configuration": config, "fulfilled": fulfilled, "data": d}
                )
        else:
            values = {}
            for subcriteria in criteria.subcriterias:
                allData = data[subcriteria.key]
                values[subcriteria.id] = allData
            fulfilled = module.are_all_subcriteria_fulfilled(criteria, values)
            evaluation.append(
                {"configuration": config, "fulfilled": fulfilled, "data": data}
            )
    return jsonify(evaluation), 200


@routes_module.route(
    "/workspaces/<workspaceId>/modules/EvaluatorModule/configurations", methods=["POST"]
)
@jwt_required
def add_configuration(workspaceId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    config = json.loads(request.form["configuration"])
    added_configuration = Module.addModuleConfiguration(
        userId=user.id,
        workspaceId=workspaceId,
        module_name="EvaluatorModule",
        config=config,
    )
    if added_configuration is None:
        return jsonify({"message": "Module configuration not added"}), 500

    return jsonify(added_configuration), 201


@routes_module.route(
    "/workspaces/<workspaceId>/modules/EvaluatorModule/configurations/<configurationId>",
    methods=["DELETE"],
)
@jwt_required
def delete_configuration(workspaceId, configurationId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    deleted_configuration = Module.deleteModuleConfiguration(
        userId=user.id,
        workspaceId=workspaceId,
        module_name="EvaluatorModule",
        configId=configurationId,
    )
    if deleted_configuration is None:
        return jsonify({"message": "Module configuration not deleted"}), 500

    return jsonify(deleted_configuration), 200


def isBasicModule(criteria):
    """
    Check if a criteria is from the BasicModule
    :param criteria: the criteria to check
    :return: a boolean true if the criteria is from the BasicModule false if not
    """
    if criteria.get("configuration").get("value").get("moduleName") == "BasicModule":
        return True
    return False


def isContributorsModule(criteria):
    """
    Check if a criteria is from the ContributorsModule
    :param criteria: the criteria to check
    :return: a boolean true if the criteria is from the ContributorsModule false if not
    """
    if (
        criteria.get("configuration").get("value").get("moduleName")
        == "ContributorsModule"
    ):
        return True
    return False


def getSubcriteriaRealValue(criteria, subcriteria):
    """
    Get the value of a subcriteria's field
    :param criteria: the criteria containing the subcriteria
    :param subcriteria: the subcriteria from which the real value is extracted
    :return: the real value
    """
    return criteria.get("data").get(getSubcriteriaKey(subcriteria))


def getCriteriaId(criteria):
    """
    Get the id of a criteria
    :param criteria: the criteria from which the id is extracted
    :return: the id
    """
    return criteria.get("configuration").get("id")


def getCriteriaWeight(criteria):
    """
    Get the weight of a criteria
    :param criteria: the criteria from which the weight is extracted
    :return: the weight
    """
    return criteria.get("configuration").get("value").get("weight")


def getSubcriteriaKey(subcriteria):
    """
    Get the key of a subcriteria
    :param subcriteria: the subcriteria from which the key is extracted
    :return: the key
    """
    return subcriteria.get("key")


def getCriteriaUserInfo(criteria):
    """
    Get the userinfo of a criteria
    :param criteria: the criteria from which the data are extracted
    :return: the username, name, avatar and url of the criteria's user
    """
    username = criteria.get("data").get("username")
    name = criteria.get("data").get("name")
    avatar = criteria.get("data").get("avatar")
    url = criteria.get("data").get("webUrl")
    return username, name, avatar, url


def getOperatorType(operator):
    """
    Get the type of an operator
    :param operator: the operator from which the type is extracted
    :return: the operator type
    """
    return operator.get("type")


def getUserInfo(username, data):
    """
    Get the userinfos from a username
    :param username: the username of the userinfos to get
    :param data: the data from which the userinfos are extracted
    :return: the username, name, avatar and url of the criteria's user
    """
    for criteria in data:
        if getCriteriaUserInfo(criteria)[0] == username:
            name = criteria.get("data").get("name")
            avatar = criteria.get("data").get("avatar")
            url = criteria.get("data").get("webUrl")
    return username, name, avatar, url


def getSubcriteriaValue(subcriteria):
    """
    Get the value of a subcriteria
    :param subcriteria: the subcriteria from which the value is extracted
    :return: the value
    """
    return subcriteria.get("value")


def getSubcriteriaSymbol(subcriteria):
    """
    Get the symbol of a subcriteria
    :param subcriteria: the subcriteria from which the symbol is extracted
    :return: the symbol
    """
    return subcriteria.get("compareSymbol")


def getSubcriteriaWeight(subcriteria):
    """
    Get the weight of a subcriteria
    :param subcriteria: the subcriteria from which the weight is extracted
    :return: the weight
    """
    return subcriteria.get("weight")


def getCriteriaSubcriterias(criteria):
    """
    Get the list of subcriterias of a criteria
    :param criteria: the criteria from which the list of subcriterias is extracted
    :return: the list of subcriterias
    """
    return criteria.get("configuration").get("value").get("subcriterias")


def getCriteriaOperators(criteria):
    """
    Get the list of operators of a criteria
    :param criteria: the criteria from which the list of operators is extracted
    :return: the list of operators
    """
    return criteria.get("configuration").get("value").get("operators")


def isCriteriaFulfilled(criteria):
    """
    Check if a criteria is fulfilled
    :param criteria: the criteria to check
    :return: a boolean true if the criteria is fulfilled false if not
    """
    if criteria["fulfilled"]:
        return True
    return False


def getCriteriaLabel(criteria):
    """
    Get the label of a criteria
    :param criteria: the criteria from which the label is extracted
    :return: the label
    """
    return criteria.get("configuration").get("label")


def getCriteriaDescription(criteria):
    """
    Get the description of a criteria
    :param criteria: the criteria from which the description is extracted
    :return: the description
    """
    return criteria.get("configuration").get("description")


def setSubcriteriaContent(soup, subcriteria, td):
    """
    Set the content for a subcriteria in a criteria
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param subcriteria: the subcriteria from which the content is extracted
    :param td: the BeautifulSoup object referencing the HTML td of the subcriteria
    :return:
    """
    key = getSubcriteriaKey(subcriteria)
    symbol = getSubcriteriaSymbol(subcriteria)
    value = getSubcriteriaValue(subcriteria)
    weight = getSubcriteriaWeight(subcriteria)
    td.append(key + " " + symbol + " " + str(value))
    small = soup.new_tag("small")
    small.string = " (" + str(weight) + ")"
    td.append(small)
    return soup


def set_basicmodule_content(soup, divModule, data):
    """
    Get the HTML content of the criterias of the BasicModule
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param divModule: the BeautifulSoup object referencing the HTML div of the EvaluatorModule
    :param data: the JSON data from which are extracted the information to export
    :return: the BeautifulSoup object referencing the HTML div of the EvaluatorModule
    """
    h3 = soup.new_tag("h3")
    h3.string = "Basic Module Criterias"
    divModule.append(h3)

    table = soup.new_tag("table")
    table["class"] = "customTables"
    divModule.append(table)

    thead = soup.new_tag("thead")
    table.append(thead)

    tr = soup.new_tag("tr")
    thead.append(tr)

    th = soup.new_tag("th")
    th.string = "Criteria"
    tr.append(th)

    th = soup.new_tag("th")
    th.string = "Fulfilled ?"
    tr.append(th)

    tbody = soup.new_tag("tbody")
    table.append(tbody)

    for criteria in data:
        label = getCriteriaLabel(criteria)
        subcriterias = getCriteriaSubcriterias(criteria)
        operators = getCriteriaOperators(criteria)
        fulfilled = isCriteriaFulfilled(criteria)
        description = getCriteriaDescription(criteria)
        weight = getCriteriaWeight(criteria)
        if isBasicModule(criteria):
            tr = soup.new_tag("tr")
            tbody.append(tr)

            td = soup.new_tag("td")

            b = soup.new_tag("b")
            b.string = label + " (weight = " + str(weight) + ")"
            td.append(b)

            br = soup.new_tag("br")
            td.append(br)

            em = soup.new_tag("em")
            em.string = description
            td.append(em)

            br = soup.new_tag("br")
            td.append(br)
            br = soup.new_tag("br")
            td.append(br)

            # First Subcriteria
            setSubcriteriaContent(soup, subcriterias[0], td)

            for index, subcriteria in enumerate(subcriterias[1:]):
                br = soup.new_tag("br")
                td.append(br)
                td.append(getOperatorType(operators[index]) + " ")
                setSubcriteriaContent(soup, subcriteria, td)

            tr.append(td)

            td = soup.new_tag("td")
            if fulfilled:
                td["bgcolor"] = "lightgreen"
            else:
                td["bgcolor"] = "#ff4d4d"

            keys = []

            key = getSubcriteriaKey(subcriterias[0])
            if key not in keys:
                keys.append(key)
                realvalue = getSubcriteriaRealValue(criteria, subcriterias[0])
                td.append(key + " = " + str(realvalue))

            for subcriteria in subcriterias[1:]:
                key = getSubcriteriaKey(subcriteria)
                if key not in keys:
                    keys.append(key)
                    realvalue = getSubcriteriaRealValue(criteria, subcriteria)
                    br = soup.new_tag("br")
                    td.append(br)
                    td.append(key + " = " + str(realvalue))
            tr.append(td)
    return divModule


def getCriteriaFromUser(id, username, data):
    """
    Get a specific criteria from a user
    :param id: the id of the criteria to return
    :param username: the user username
    :param data: the data containing the criteria to find
    :return: the criteria
    """
    for criteria in data:
        if (
            criteria.get("data").get("username") == username
            and criteria.get("configuration").get("id") == id
        ):
            return criteria


def set_contributorsmodule_content(soup, divModule, data):
    """
    Get the HTML content of the criterias of the ContributorsModule
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param divModule: the BeautifulSoup object referencing the HTML div of the EvaluatorModule
    :param data: the JSON data from which are extracted the information to export
    :return: the BeautifulSoup object referencing the HTML div of the EvaluatorModule
    """
    h3 = soup.new_tag("h3")
    h3.string = "Contributors Module Criterias"
    divModule.append(h3)
    criterias = []
    users = []

    table = soup.new_tag("table")
    table["class"] = "customTables"
    divModule.append(table)

    # Criterias Line
    thead = soup.new_tag("thead")
    table.append(thead)

    tr = soup.new_tag("tr")
    thead.append(tr)

    th = soup.new_tag("th")
    th.string = "Criteria"
    tr.append(th)

    th = soup.new_tag("th")
    th.string = "Fulfilled ?"
    tr.append(th)
    tbody = soup.new_tag("tbody")
    table.append(tbody)
    for criteria in data:
        id = getCriteriaId(criteria)
        if isContributorsModule(criteria) and not criterias.__contains__(id):
            criterias.append(id)
            label = getCriteriaLabel(criteria)
            subcriterias = getCriteriaSubcriterias(criteria)
            operators = getCriteriaOperators(criteria)
            fulfilled = isCriteriaFulfilled(criteria)
            description = getCriteriaDescription(criteria)
            weight = getCriteriaWeight(criteria)
            tr = soup.new_tag("tr")
            tbody.append(tr)

            td = soup.new_tag("td")

            b = soup.new_tag("b")
            b.string = label + " (weight = " + str(weight) + ")"
            td.append(b)

            br = soup.new_tag("br")
            td.append(br)

            em = soup.new_tag("em")
            em.string = description
            td.append(em)

            br = soup.new_tag("br")
            td.append(br)
            br = soup.new_tag("br")
            td.append(br)

            # First Subcriteria
            setSubcriteriaContent(soup, subcriterias[0], td)

            for index, subcriteria in enumerate(subcriterias[1:]):
                br = soup.new_tag("br")
                td.append(br)
                td.append(getOperatorType(operators[index]) + " ")
                setSubcriteriaContent(soup, subcriteria, td)

            tr.append(td)

            # Users
            # Construction liste user

            for criteria in data:
                if isContributorsModule(criteria):
                    username = getCriteriaUserInfo(criteria)[0]
                    if not users.__contains__(username):
                        users.append(username)

            td = soup.new_tag("td")
            tr.append(td)
            user_table = soup.new_tag("table")
            user_table["class"] = "userFulfilled"

            td.append(user_table)

            for user in users:
                username, name, avatar, url = getUserInfo(user, data)
                crit = getCriteriaFromUser(id, username, data)
                fulfilled = isCriteriaFulfilled(crit)
                subcriterias = getCriteriaSubcriterias(crit)
                tr = soup.new_tag("tr")
                user_table.append(tr)

                td = soup.new_tag("td")
                tr.append(td)

                # User Table

                tableUser = soup.new_tag("table")
                tableUser["class"] = "userCriteria"
                td.append(tableUser)

                trUser = soup.new_tag("tr")
                tableUser.append(trUser)

                tdUser = soup.new_tag("td")
                trUser.append(tdUser)

                img = soup.new_tag("img", src=str(avatar), alt="avatar", height="40")
                img["class"] = "avatar"
                tdUser.append(img)

                tdUser = soup.new_tag("td")
                trUser.append(tdUser)

                p = soup.new_tag("p")
                p.string = str(name)
                tdUser.append(p)

                p.append(soup.new_tag("br"))

                a = soup.new_tag("a", href=str(url), target="_blank")
                a["class"] = "userUrl"
                a.string = "@" + str(username)
                p.append(a)

                td = soup.new_tag("td")

                if fulfilled:
                    td["bgcolor"] = "lightgreen"
                else:
                    td["bgcolor"] = "#ff4d4d"

                keys = []

                key = getSubcriteriaKey(subcriterias[0])
                if key not in keys:
                    keys.append(key)
                    realvalue = getSubcriteriaRealValue(crit, subcriterias[0])
                    td.append(key + " = " + str(realvalue))

                for subcriteria in subcriterias[1:]:
                    key = getSubcriteriaKey(subcriteria)
                    if key not in keys:
                        keys.append(key)
                        realvalue = getSubcriteriaRealValue(crit, subcriteria)
                        br = soup.new_tag("br")
                        td.append(br)
                        td.append(key + " = " + str(realvalue))
                tr.append(td)
    return divModule


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/EvaluatorModule/html",
    methods=["POST"],
)
@jwt_required
def export_html_evaluator(workspaceId, repositoryId):
    """
    Generate a HTML report of the EvaluatorModule of a repository
    Returns a string containing the HTML.
    ---
      tags:
        - evaluator module
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to export
          required: true
          schema:
            type: integer
        - name: repositoryId
          in: path
          description: The id of the repository to export
          required: true
          schema:
            type: integer
      responses:
        '401':
          description: User not found
        '200':
          description: OK
          schema:
            type: string
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    ############################################################### With request
    #
    # # URL
    # apiUrl = request.host_url
    #
    # # Token
    # accessToken = request.form.get("token", None)
    #
    # headers = {'Authorization': 'Bearer ' + accessToken}
    #
    # # Request
    # url = apiUrl + 'workspaces/' + workspaceId + '/repositories/' + repositoryId + '/modules/EvaluatorModule/data'
    # data = requests.get(url=url, headers=headers).json()
    res = get_EvaluatorModule(workspaceId=workspaceId, repositoryId=repositoryId)[0]
    data = res.json
    soup = BeautifulSoup("", features="html5lib")
    div = soup.new_tag("div")
    div["id"] = "EvaluatorModule"
    div["class"] = "module"
    soup.body.append(div)
    set_basicmodule_content(soup, div, data)
    set_contributorsmodule_content(soup, div, data)
    return str(soup), 200
