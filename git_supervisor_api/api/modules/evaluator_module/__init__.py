from git_supervisor_api.api.models.module_model import Module
from git_supervisor_api.api.modules import update_workspaces_modules
from git_supervisor_api.api.modules.evaluator_module.model import EvaluatorModule

Module.createModule(EvaluatorModule().json())
update_workspaces_modules()
