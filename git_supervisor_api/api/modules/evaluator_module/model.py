from git_supervisor_api.api.models.module_model import Module


class EvaluationCriteria:
    """
    A criteria contains data, from 1 to several subcriterias and from 0 to several operators
    """

    def __init__(self, config):
        self.name = config["label"]
        self.description = config["description"]
        self.moduleName = config["value"]["moduleName"]
        self.subcriterias = []
        for subcriteria in config["value"]["subcriterias"]:
            self.subcriterias.append(EvaluationSubCriteria(subcriteria))
        self.operators = []
        for operator in config["value"]["operators"]:
            self.operators.append(EvaluationOperators(operator))


class EvaluationSubCriteria:
    """
    A subcriteria contains data, a value and a symbol for comparison
    """

    def __init__(self, config):
        self.id = config["id"]
        self.key = config["key"]
        self.keyType = config["keyType"]
        self.compareSymbol = config["compareSymbol"]
        self.value = config["value"]


class EvaluationOperators:
    """
    An operator contains the IDs of 2 subcriterias and a type (basically "AND" or "OR")
    """

    def __init__(self, config):
        self.id = config["id"]
        self.type = config["type"]
        self.firstId = config["firstId"]
        self.secondId = config["secondId"]


def get_previous_subcriteria(criteria, configurations):
    """
    Get the previous subcriteria from the list

    :param criteria: the current subcriteria
    :param configurations: the configuration containing all the subcriterias

    :return: the previous subcriteria
    """
    for operator in configurations.operators:
        if operator.secondId == criteria.id:
            id = operator.firstId
    for subcriteria in configurations.subcriterias:
        if operator.id == id:
            return subcriteria


def get_next_subcriteria(criteria, configurations):
    """
    Get the next subcriteria from the list

    :param criteria: the current subcriteria
    :param configurations: the configuration containing all the subcriterias

    :return: the next subcriteria
    """
    id = 0
    for operator in configurations.operators:
        if operator.firstId == criteria.id:
            id = operator.secondId
    for subcriteria in configurations.subcriterias:
        if subcriteria.id == id:
            return subcriteria


class EvaluatorModule(Module):
    name = "EvaluatorModule"
    description = "Allows to create various criteria depending on others modules data, to evaluate repositories"
    enabled = True
    configurations = []  # No default configurations for EvaluatorModule
    data = None  # No dataModel for EvaluatorModule

    def has_subcriteria(self, criteria, configurations):
        """
        Checks if a criteria is the first or last of a list of subcriterias

        :param criteria: the subcriteria to check
        :param configurations: the configuration containing all the subcriterias
        :return:
            - isFirst - true if the subcriteria is the first of the list (doesn't have a previous subcriteria)
            - isSecond - true if the subcriteria is the last of the list (doesn't have a next subcriteria)
        """
        isFirst = False
        isSecond = False
        for operator in configurations.operators:
            if operator.firstId == criteria.id:
                isFirst = True
            if operator.secondId == criteria.id:
                isSecond = True
        return isFirst, isSecond

    def are_all_subcriteria_fulfilled(self, value, listSubcriteriasValues):
        """
        Checks for a criteria if the combination of sucriterias with the operators is fulfilled or not

        :param value: JSON containing the list of subcriterias and list of operators
        :param listSubcriteriasValues: list of the data to compare to the subcriterias
        :return: if the criteria is fulfilled (the combination of all the subcriterias with the operators) or not
        """
        currentCriteria = value.subcriterias[0]
        for operator in value.operators:
            isFirst, isSecond = self.has_subcriteria(
                self.get_criteria_by_id(value.subcriterias, operator.firstId), value
            )
            if isFirst and not isSecond:
                firstCriteria = next(
                    (x for x in value.subcriterias if x.id == operator.firstId), None
                )
                break
        if len(value.operators) > 0:
            currentCriteria = firstCriteria
        expression = str(
            self.is_criteria_fulfilled(
                listSubcriteriasValues[currentCriteria.id], currentCriteria
            )
        )
        for i in range(len(value.subcriterias) - 1):
            nextCriteria = get_next_subcriteria(currentCriteria, value)
            curentOperator = self.get_operator_from_criteria(
                currentCriteria, nextCriteria, value.operators
            )
            # expression -> succession of operators and boolean values of subcriteria
            expression += (
                " "
                + curentOperator.type.lower()
                + " "
                + str(
                    self.is_criteria_fulfilled(
                        listSubcriteriasValues[nextCriteria.id], nextCriteria
                    )
                )
            )
            currentCriteria = nextCriteria
        return eval(expression)

    def is_criteria_fulfilled(self, value, evaluation_criteria):
        """
        Checks for a subcriteria if it is fulfilled or not

        :param value: the data to compare to the value of the subcriteria
        :param evaluation_criteria: the subcriteria

        :return: if the criteria is fulfilled or not
        """
        if evaluation_criteria.keyType == "boolean":
            if evaluation_criteria.compareSymbol == "=":
                return value == evaluation_criteria.value
            if evaluation_criteria.compareSymbol == "!=":
                return value != evaluation_criteria.value
        elif evaluation_criteria.keyType == "number":
            if evaluation_criteria.compareSymbol == "=":
                return value == int(evaluation_criteria.value)
            if evaluation_criteria.compareSymbol == "!=":
                return value != int(evaluation_criteria.value)
            if evaluation_criteria.compareSymbol == ">":
                return value > int(evaluation_criteria.value)
            if evaluation_criteria.compareSymbol == "<":
                return value < int(evaluation_criteria.value)
        elif evaluation_criteria.keyType == "string":
            if evaluation_criteria.compareSymbol == "=":
                return value == evaluation_criteria.value
            if evaluation_criteria.compareSymbol == "!=":
                return value != evaluation_criteria.value
        return False

    def get_criteria_by_id(self, criterias, id):
        """
        Get a subcriteria from its ID

        :param criterias: list containing the subcriterias
        :param id: ID of the subcriteria to get

        :return: the subcriteria corresponding to the ID
        """
        for criteria in criterias:
            if criteria.id == id:
                return criteria

    def get_operator_from_criteria(self, firstCriteria, secondCriteria, operators):
        """
        Get the operator that links to given subcriterias

        :param firstCriteria: the first subcriteria
        :param secondCriteria: the second subcriteria
        :param operators: list of the operators

        :return: the operator that links the two subcriterias
        """
        for operator in operators:
            if (
                firstCriteria.id == operator.firstId
                and secondCriteria.id == operator.secondId
            ):
                return operator
