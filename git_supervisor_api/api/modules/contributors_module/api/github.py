from git_supervisor_api.api.modules.contributors_module.model import (
    Contributor,
    ContributorsModuleData,
)
from git_supervisor_api.api.utils import (
    make_request_url_github,
    async_request_content,
    call_github_api,
)


def get_contributors_github(access_token, owner, repo_name):
    """Function to get all github contributors from a specific repository

    Args:
        repo_name (string): github repository name
        owner (string): github owner name
        access_token (string): github access token

    Returns:
        Contributor: list of Contributors
    """
    contributors_url = make_request_url_github(
        resource=f"repos/{owner}", sub_resource="contributors", git_id=repo_name
    )
    contributors = call_github_api(contributors_url, access_token=access_token)

    users = []
    for contributor in contributors:
        url = make_request_url_github(resource="users", git_id=contributor["login"])
        users.append(call_github_api(url, access_token=access_token))

    for contributor in users:
        if contributor["name"] is None:
            contributor["name"] = contributor["login"]

    return [
        Contributor(
            id=contributor["id"],
            name=contributor["name"],
            username=contributor["login"],
            email=contributor["email"],
            avatar=contributor["avatar_url"],
            web_url=contributor["html_url"],
        )
        for contributor in users
    ]


def get_contributors_info_github(access_token, owner, repo_name, branch, since, until):
    """Function to get github contributors information (commits and issues) from specific repository

    Args:
        access_token (string): github access token
        repo_name (string): github repository name
        owner (string): github owner name
        branch (string): name of the branch to get the information
        since (string): start date to get the information
        until (string): end date to get the information

    Returns:
        ContributorsData: a ContributorsData made of a list of contributors, a list of commits and a list of issues
    """
    contributors = get_contributors_github(
        access_token=access_token, owner=owner, repo_name=repo_name
    )

    commits_info = call_github_api(
        make_request_url_github(
            resource="repos",
            sub_resource=owner + "/" + repo_name + "/contributors",
        ),
        access_token=access_token,
    )
    all_issues = get_all_contributors_issues_github(
        access_token=access_token,
        owner=owner,
        repo_name=repo_name,
        since=since,
    )
    all_commits = get_all_contributors_commits_github(
        access_token=access_token,
        owner=owner,
        repo_name=repo_name,
        branch=branch,
        since=since,
        until=until,
    )
    contributors_info = []
    for contributor in contributors:
        contributor.email = [
            contributor.email
            for commit_info in commits_info
            if commit_info["login"] == contributor.username
        ]

        contributor.commits = [
            commit
            for commit in all_commits
            if commit["authorLogin"] == contributor.username
            or (contributor.email and contributor.email[0] == commit["authorEmail"])
        ]

        contributor.commitsCount = len(contributor.commits)

        contributor.issues = [
            issue
            for issue in all_issues
            if contributor.name in [i["login"] for i in issue["assignees"]]
        ]
        contributor.issuesCount = len(contributor.issues)

        contributor.openIssuesCount = len(
            [issue for issue in contributor.issues if issue["state"] == "open"]
        )

        contributors_info.append(contributor)

    return ContributorsModuleData(contributors_info)


def get_all_contributors_commits_github(
    access_token, owner, repo_name, branch, since, until
):
    import re

    all_commits = call_github_api(
        make_request_url_github(
            resource=f"repos/{owner}",
            sub_resource="commits",
            git_id=repo_name,
            sha=branch,
            since=since,
            until=until,
        ),
        access_token=access_token,
    )
    pattern = re.compile("^Merge")
    commits = [
        {
            "title": commit["commit"]["message"],
            "createdAt": commit["commit"]["author"]["date"],
            "authorLogin": commit["commit"]["author"]["name"],
            "authorEmail": commit["commit"]["author"]["email"],
        }
        for commit in all_commits
        if not pattern.match(commit["commit"]["message"])
    ]

    return commits


def get_all_contributors_issues_github(access_token, owner, repo_name, since):
    all_issues = call_github_api(
        make_request_url_github(
            resource=f"repos/{owner}",
            sub_resource="issues",
            git_id=repo_name,
            since=since,
            assignee="*",
        ),
        access_token=access_token,
    )
    issues = [
        {
            "title": issue["title"],
            "description": issue["body"],
            "state": issue["state"],
            "labels": issue["labels"],
            "milestone": issue["milestone"],
            "assignees": issue["assignees"],
        }
        for issue in all_issues
    ]

    return issues
