from git_supervisor_api.api.modules.contributors_module.model import (
    Contributor,
    ContributorsModuleData,
)
from git_supervisor_api.api.utils import make_request_url_gitlab, call_gitlab_api


def get_contributors(access_token, git_id):
    """Function to get all gitlab contributors from a specific repository

    Args:
        access_token (string): gitlab access token
        git_id (string): gitlab repository ID

    Returns:
        Contributor: list of Contributors
    """
    gitlab_users = call_gitlab_api(
        make_request_url_gitlab(
            resource="projects",
            sub_resource="search",
            scope="users",
            search=" ",
            git_id=git_id,
            access_token=access_token,
        )
    )

    return [
        Contributor(
            id=contributor["id"],
            name=contributor["name"],
            username=contributor["username"],
            avatar=contributor["avatar_url"],
            web_url=contributor["web_url"],
        )
        for contributor in gitlab_users
    ]


def get_contributors_info(access_token, git_id, branch, since, until):
    """Function to get gitlab contributors information (commits and issues) from specific repository

    Args:
        access_token (string): gitlab access token
        git_id (string): gitlab repository ID
        branch (string): name of the branch to get the information
        since (string): start date to get the information
        until (string): end date to get the information

    Returns:
        ContributorsData: a ContributorsData made of a list of contributors, a list of commits and a list of issues
    """
    contributors = get_contributors(access_token=access_token, git_id=git_id)

    commits_info = call_gitlab_api(
        make_request_url_gitlab(
            resource="projects",
            sub_resource="repository/contributors",
            git_id=git_id,
            access_token=access_token,
        )
    )

    all_issues = get_all_contributors_issues(
        access_token=access_token, git_id=git_id, since=since, until=until
    )

    all_commits = get_all_contributors_commits(
        access_token=access_token,
        git_id=git_id,
        branch=branch,
        since=since,
        until=until,
    )

    contributors_info = []
    for contributor in contributors:
        contributor.email = [
            commit_info["email"]
            for commit_info in commits_info
            if commit_info["name"] == contributor.name
        ]

        contributor.commits = [
            commit
            for commit in all_commits
            if commit["authorEmail"] in contributor.email
        ]

        contributor.commitsCount = len(contributor.commits)

        contributor.issues = [
            issue
            for issue in all_issues
            if contributor.name in [i["name"] for i in issue["assignees"]]
        ]
        contributor.issuesCount = len(contributor.issues)

        contributor.openIssuesCount = len(
            [issue for issue in contributor.issues if issue["state"] == "opened"]
        )

        contributors_info.append(contributor)

    return ContributorsModuleData(contributors_info)


def get_all_contributors_commits(access_token, git_id, branch, since, until):
    import re

    all_commits = call_gitlab_api(
        make_request_url_gitlab(
            resource="projects",
            sub_resource="repository/commits",
            git_id=git_id,
            access_token=access_token,
            ref_name=branch,
            since=since,
            until=until,
        )
    )

    pattern = re.compile("^Merge")
    commits = [
        {
            "title": commit["title"],
            "createdAt": commit["created_at"],
            "authorEmail": commit["author_email"],
        }
        for commit in all_commits
        if not pattern.match(commit["title"])
    ]

    return commits


def get_all_contributors_issues(access_token, git_id, since, until):
    all_issues = call_gitlab_api(
        make_request_url_gitlab(
            resource="projects",
            sub_resource="issues",
            git_id=git_id,
            created_after=since,
            created_before=until,
            access_token=access_token,
            assignee_id="Any",
        )
    )

    issues = [
        {
            "title": issue["title"],
            "description": issue["description"],
            "state": issue["state"],
            "labels": issue["labels"],
            "milestone": issue["milestone"],
            "assignees": issue["assignees"],
        }
        for issue in all_issues
    ]

    return issues
