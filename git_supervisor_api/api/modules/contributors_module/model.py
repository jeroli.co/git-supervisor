from git_supervisor_api.api.models.module_model import Module, ModuleConfiguration
from git_supervisor_api.api.utils import get_configuration


class Contributor:
    def __init__(
        self,
        id=None,
        name="",
        username="",
        email=None,
        avatar=None,
        web_url=None,
        commits=None,
        commits_count=0,
        issues=None,
        issues_count=0,
        open_issues_count=0,
    ):
        self.id = id
        self.name = name
        self.username = username
        self.email = email
        self.avatar = avatar
        self.webUrl = web_url
        self.commits = commits
        self.commitsCount = commits_count
        self.issues = issues
        self.issuesCount = issues_count
        self.openIssuesCount = open_issues_count


class ContributorsModuleData:
    def __init__(self, contributors=Contributor()):
        self.contributors = contributors

    def json(self):
        return [contributor.__dict__ for contributor in self.contributors]

    def apply_configurations(self, configs):
        if not get_configuration(configs, "inactiveUsers"):
            self.contributors[:] = [
                contributor
                for contributor in self.contributors
                if contributor.commitsCount != 0 or contributor.issuesCount != 0
            ]


class ContributorsModule(Module):
    name = "ContributorsModule"
    description = "Shows the number of commits and issues by contributor, within specified date range and on specified branch"
    enabled = True
    configurations = [
        ModuleConfiguration(
            id="inactiveUsers",
            label="Show inactive users (no commits and issues)",
            value=False,
        )
    ]
    data = ContributorsModuleData().contributors.__dict__
