import inspect

from git_supervisor_api.api.models.module_model import Module
from git_supervisor_api.api.modules import update_workspaces_modules, modules_dict
from git_supervisor_api.api.modules.contributors_module import routes
from git_supervisor_api.api.modules.contributors_module.model import ContributorsModule

Module.createModule(ContributorsModule().json())
update_workspaces_modules()
modules_dict["functions"] += inspect.getmembers(routes, inspect.isfunction)
