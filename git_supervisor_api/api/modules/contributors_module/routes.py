import datetime
import re

from bs4 import BeautifulSoup
from flask import jsonify, request
from flask_jwt_extended import jwt_required

from git_supervisor_api.api.models.module_model import Module
from git_supervisor_api.api.models.repository_model import Repository
from git_supervisor_api.api.modules import routes_module
from git_supervisor_api.api.modules.contributors_module.api.gitlab import (
    get_contributors_info,
)
from git_supervisor_api.api.utils import verify_user
from .api.github import get_contributors_info_github
from .model import ContributorsModule
from ...models.user_model import Vcs
from ...models.workspace_model import Workspace


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/ContributorsModule/data",
    methods=["GET"],
)
@jwt_required
def get_ContributorsModule(workspaceId, repositoryId):
    """
    Gets repository's contributors information
    Returns all contributors information for a repository (number of commits per contributor, commit messages, ...) if the specified workspace contains the repository.
    Some parameters can be passed to have more precise information (period of time, branch name).
    ---
      tags:
        - contributors module
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository from which to get contributors informations
          required: true
          type: string
        - name: branch
          in: query
          description: The branch to inspect
          type: string
        - name: since
          in: query
          description: The date from which you want to inspect the commits
          type: string
          format: date
        - name: until
          in: query
          description: The date until which the commits are to be inspected
          type: string
          format: date
      responses:
        '401':
          description: User not found
        '404':
          description: Repository not found
        '200':
          description: OK
          schema:
            type: array
            items:
                type: object
                properties:
                    id:
                        type: integer
                    name:
                        type: string
                    username:
                        type: string
                    email:
                        type: string
                        format: email
                    avatar:
                        type: string
                        format: binary
                    webUrl:
                        type: string
                        format: url
                    commits:
                        type: array
                        items:
                            type: object
                            properties:
                                title:
                                    type: string
                                createdAt:
                                    type: string
                                    format: date
                                authorEmail:
                                    type: string
                                    format: email
                    commitsCount:
                        type: integer
                    issues:
                        type: array
                        items:
                            type: object
                            properties:
                                title:
                                    type: string
                                description:
                                    type: string
                                state:
                                    type: string
                                labels:
                                    type: array
                                    items:
                                        type: string
                                milestone:
                                    type: object
                                assignees:
                                    type: array
                                    items:
                                        type: object
                    issuesCount:
                        type: integer
                    openIssuesCount:
                        type: integer
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    configs = Module.getModule(
        userId=user.id,
        workspaceId=workspaceId,
        module_name=ContributorsModule.name,
    )["configurations"]
    repository = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if repository is None:
        return jsonify({"message": "Repository not found"}), 404
    git_id = str(repository.gitId)
    branch = request.args.get("branch")
    since = repository.created_at
    until = request.args.get("until", None)

    workspace = Workspace.getWorkspace(
        userId=user.id,
        workspaceId=workspaceId,
    )
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    if workspace.vcs == Vcs.GITLAB:
        access_token = user.access_token_gitlab
        data = get_contributors_info(
            access_token=access_token,
            git_id=git_id,
            branch=branch,
            since=since,
            until=until,
        )
    elif workspace.vcs == Vcs.GITHUB:
        access_token = user.access_token_github
        data = get_contributors_info_github(
            access_token=access_token,
            owner=repository.owner,
            repo_name=repository.name,
            branch=branch,
            since=since,
            until=until,
        )
    else:
        access_token = None
    if access_token is None:
        return jsonify({"message": "Token not found"}), 404

    data.apply_configurations(configs)
    if data.contributors is None:
        return jsonify({"message": "Repository not found"}), 404

    return jsonify(data.json()), 200


def getContributorsData(workspaceId, repositoryId, branch, since, until):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    access_token = user.access_token_gitlab
    configs = Module.getModule(
        userId=user.id,
        workspaceId=workspaceId,
        module_name=ContributorsModule.name,
    )["configurations"]
    repository = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if repository is None:
        return jsonify({"message": "Repository not found"}), 404
    git_id = str(repository.gitId)
    # branch = request.args.get("branch")
    today = datetime.datetime.now().date()
    # since = request.args.get("since", str(today - datetime.timedelta(weeks=16)))
    # until = request.args.get("until", None)

    if since == "":
        since = None
    if until == "":
        until = None

    data = get_contributors_info(
        access_token=access_token,
        git_id=git_id,
        branch=branch,
        since=since,
        until=until,
    )

    data.apply_configurations(configs)
    if data.contributors is None:
        return jsonify({"message": "Repository not found"}), 404

    return jsonify(data.json()), 200


def retrieveDateTime(orig):
    """
    Get a datetime values from a string

    :param orig: the string from which is extracted the datetime value

    :return: a string containing the datetime value
    """
    match = re.search(r"\d{4}-\d{2}-\d{2}", orig)
    date = datetime.datetime.strptime(match.group(), "%Y-%m-%d").strftime("%d/%m/%Y")

    match = re.search(r"\d{2}:\d{2}:\d{2}", orig)

    time = datetime.datetime.strptime(match.group(), "%H:%M:%S").strftime("%H:%M")
    res = str(date) + " " + str(time)
    return res


def contributors_content(data, soup, divModule, branch):
    """
    Get the content for the ContributorModule
    :param data: the JSON data from which are extracted the information to export
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param divModule: the BeautifulSoup object referencing the HTML div of the ContributorsModule
    :param branch: the branch
    :return: the BeautifulSoup object referencing the HTML div of the ContributorsModule
    """
    h3 = soup.new_tag("h3")
    h3.string = branch + " branch"
    divModule.append(h3)

    # Doughnut chart
    script = soup.new_tag("script")
    script.string = generate_doughnut(data, branch)
    divModule.append(script)

    varName = re.sub(r"[^A-Za-z]", "", branch)
    canvas = soup.new_tag("canvas", id=varName, height="400")

    div = soup.new_tag("div")
    div.append(canvas)
    divModule.append(div)

    div = soup.new_tag("div")
    div["class"] = "userDiv"
    divModule.append(div)

    # Contributors details
    for user in data:
        tableUser = soup.new_tag("table")
        tableUser["class"] = "user"
        tr = soup.new_tag("tr")
        tableUser.append(tr)

        td = soup.new_tag("td")
        tr.append(td)

        img = soup.new_tag(
            "img", src=user["avatar"], alt="avatar", height="60", width="60"
        )
        img["class"] = "avatar"
        td.append(img)

        td = soup.new_tag("td")
        tr.append(td)

        p = soup.new_tag("p")
        p["class"] = "userName"
        p.string = user["name"]
        td.append(p)

        a = soup.new_tag("a", href=user["webUrl"], target="_blank")
        a["class"] = "userUrl"
        a.string = "@" + user["username"]
        td.append(a)

        div.append(tableUser)

        table = soup.new_tag("table")
        table["class"] = "customTables"
        div.append(table)

        thead = soup.new_tag("thead")
        table.append(thead)

        tr = soup.new_tag("tr")
        tr["class"] = "firstLine"
        thead.append(tr)

        th = soup.new_tag("th")
        th["class"] = "left"
        th.string = "Commits"
        tr.append(th)

        th = soup.new_tag("th")
        th["class"] = "right"
        th.string = "Date"
        tr.append(th)

        tbody = soup.new_tag("tbody")
        table.append(tbody)

        # Commits
        for commit in user["commits"]:
            tr = soup.new_tag("tr")
            tbody.append(tr)

            td = soup.new_tag("td")
            td["class"] = "left"
            td.string = commit["title"]
            tr.append(td)

            td = soup.new_tag("td")
            td["class"] = "right"
            td.string = retrieveDateTime(commit["createdAt"])
            tr.append(td)

    return divModule


def generate_doughnut(data, branch):
    """
    Generates a JavaScript doughnut displaying the commits count by contributor on a precise branch
    :param data: the data from which the doughnut is created
    :param branch: the branch name
    :return: a string containing the JavaScript of the doughnut
    """
    varName = re.sub(r"[^A-Za-z]", "", branch)
    temp = (
        "var "
        + varName
        + "Ctx = document.getElementById('"
        + varName
        + "').getContext('2d'); "
        "" + "var " + varName + " = new Chart(" + varName + "Ctx, { type: "
        "'doughnut', "
        "data: { labels: [ "
    )
    users = []
    commitsCount = []
    for user in data:
        users.append(user["name"])
        commitsCount.append(user["commitsCount"])

    bg = "backgroundColor: ["
    hoverbg = "hoverBackgroundColor: ["
    for name in users:
        temp += '"' + name + '", '
        bg += "getRandomColor(),"
        hoverbg += "'lightgrey',"
    bg += "],"
    hoverbg += "]"

    temp += "], datasets: [{ label: 'Dataset ', data: ["

    # Ajout counts
    for count in commitsCount:
        temp += str(count) + ", "
    temp += "], fill: false, "
    temp += bg
    temp += hoverbg
    temp += (
        "}] }, options: {legend:  {labels: {fontColor: 'black', fontSize: 16, fontFamily: 'Arial',}}, title: { display: true, text: 'Contributors Commits', fontColor: 'black', fontFamily: "
        "'Arial', fontSize: 16 }, maintainAspectRatio: false, plugins: { labels: [{ render: 'value', "
        "fontSize: 15, fontStyle: 'bold', fontColor: 'black', }, { render: 'label', position: 'outside',  "
        "fontColor: 'black',  fontFamily: 'Arial', fontSize: 15}] } } }); "
    )
    return temp


@routes_module.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/modules/ContributorsModule/html",
    methods=["POST"],
)
@jwt_required
def export_html_contributors(workspaceId, repositoryId, since, until, branches):
    """
    Generate a HTML report of the ContributorsModule of a repository
    Returns a string containing the HTML.
    ---
      tags:
        - contributors module
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to export
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository to export
          required: true
          type: string
      responses:
        '401':
          description: User not found
        '200':
          description: OK
          schema:
            type: string
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    ############################################################### With request

    # # URL
    # apiUrl = request.host_url
    #
    # # Token
    # accessToken = request.form.get("token", None)
    #
    # # Request
    # url = apiUrl + 'workspaces/' + workspaceId + '/repositories/' + repositoryId + '/modules/ContributorsModule/data'
    #
    # since = request.form.get('since', None)
    # until = request.form.get('until', None)
    # branches = request.form.getlist('branches', None)
    # headers = {'Authorization': 'Bearer ' + accessToken}
    #
    # param = {}
    #
    # if since is not None and until is not None:
    #     param['since'] = since
    #     param['until'] = until
    #
    # if not branches:
    #     branches = ['master']

    ###############################################################

    soup = BeautifulSoup("", features="html5lib")
    div = soup.new_tag("div")
    div["id"] = "ContributorsModule"
    div["class"] = "module"
    soup.body.append(div)

    if not branches:
        branches.append("master")

    for branch in branches:
        # param['branch'] = branch
        # data = requests.get(url=url, headers=headers, params=param).json()
        data = getContributorsData(
            workspaceId=workspaceId,
            repositoryId=repositoryId,
            since=since,
            until=until,
            branch=branch,
        )[0]
        contributors_content(data.json, soup, div, branch)
        div.append(soup.new_tag("br"))

    script = soup.new_tag("script")
    script.string = (
        'const colorArray = [ "AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige", '
        '"Bisque", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", '
        '"Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", '
        '"DarkCyan", "DarkGoldenRod", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", '
        '"DarkOrange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", '
        '"DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", '
        '"DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", '
        '"Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", '
        '"HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", '
        '"LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", '
        '"LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", '
        '"LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", '
        '"LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", '
        '"MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", '
        '"MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", '
        '"OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", '
        '"PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", '
        '"PowderBlue", "Purple", "RebeccaPurple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", '
        '"Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", '
        '"SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", '
        '"Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen", '
        "]; function getRandomColor() { return colorArray[Math.floor(Math.random() * colorArray.length)]; "
        "} "
    )
    div.append(script)

    script = soup.new_tag(
        "script",
        src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js",
    )
    div.append(script)

    script = soup.new_tag(
        "script", src="https://cdn.jsdelivr.net/npm/chart.js@2/dist/Chart.min.js"
    )
    div.append(script)

    script = soup.new_tag(
        "script", src="https://cdn.jsdelivr.net/npm/chartjs-plugin-colorschemes"
    )
    div.append(script)

    return str(soup), 200
