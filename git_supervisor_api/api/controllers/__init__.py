from flask import Blueprint

routes = Blueprint("controllers", __name__)

from . import (
    auth_controller,
    module_controller,
    repository_controller,
    workspace_controller,
)
