import json

from flask import jsonify, request
from flask_jwt_extended import jwt_required

from git_supervisor_api.api.controllers import routes
from git_supervisor_api.api.models.workspace_model import Workspace
from git_supervisor_api.api.utils import verify_user


@routes.route("/workspaces", methods=["GET"])
@jwt_required
def get_workspaces():
    """
    Gets all user workspaces
    Returns all workspaces for the authenticated user.
    ---
      tags:
        - workspace
      security:
        - oauth: []
      responses:
        '401':
          description: User not found
        '500':
          description: Internal Error
        '201':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Workspace'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    workspaces = Workspace.getWorkspaces(userId=user.id)
    if workspaces is None:
        return jsonify({"message": "Internal Error"}), 500
    return jsonify(workspaces), 200


@routes.route("/workspaces", methods=["POST"])
@jwt_required
def add_workspace():
    """
    Adds a new workspace to logged in user
    Returns the added workspace if successful.
    ---
      tags:
        - workspace
      security:
        - oauth: []
      parameters:
        - name: workspace
          in: formData
          description: The json representation of the workspace to add
          required: true
          schema:
            $ref: '#/definitions/Workspace'
      responses:
        '404':
          description: User not found
        '500':
          description: Workspace not added
        '201':
          description: OK
          schema:
            $ref: '#/definitions/Workspace'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    w = json.loads(request.form["workspace"])
    workspace = Workspace(**w)
    addedWorkspace = Workspace.addWorkspace(userId=user.id, workspace=workspace)
    if addedWorkspace is None:
        return jsonify({"message": "Workspace not added"}), 500
    return jsonify(addedWorkspace.json()), 201


@routes.route("/workspaces/<workspaceId>", methods=["GET"])
@jwt_required
def get_workspace(workspaceId):
    """
    Gets a workspace
    Returns a workspace thanks to its id.
    ---
    tags:
        - workspace
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace to get
          required: true
          type: string
    responses:
        '401':
          description: User not found
        '404':
          description: Workspace not found
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Workspace'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404
    full_workspace = workspace.full()
    return jsonify(full_workspace.json()), 200


@routes.route("/workspaces/<workspaceId>", methods=["PUT"])
@jwt_required
def update_workspace(workspaceId):
    """
    Updates a workspace
    Updates and returns the new workspace identified by its id.
    ---
      tags:
        - workspace
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace to get
          required: true
          type: string
      responses:
        '401':
          description: User not found
        '500':
          description: Workspace not updated
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Workspace'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    w = json.loads(request.form["workspace"])
    updated_workspace = Workspace.updateWorkspace(
        userId=user.id, workspaceId=workspaceId, updatedWorkspace=Workspace(**w)
    )
    if updated_workspace is None:
        return jsonify({"message": "Workspace not updated"}), 500
    return jsonify(updated_workspace.json()), 200


@routes.route("/workspaces/<workspaceId>/configurations", methods=["POST"])
@jwt_required
def update_workspace_settings(workspaceId):
    """
    Updates a workspace settings
    Updates and returns the new workspace settings.
    ---
      tags:
        - workspace
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace to get
          required: true
          type: string
      responses:
        '401':
          description: User not found
        '500':
          description: Workspace settings not updated
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Workspace'
    """
    valid_json = False
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    workspace_settings = json.loads(request.form["modules"])

    # Check data integrity
    valid_json = checkImport(workspace_settings)

    if not valid_json:
        return jsonify({"message": "JSON structure invalid"}), 406
    updated_settings = Workspace.updateWorkspaceModules(
        workspaceId=workspaceId, modules=workspace_settings, userId=user.id
    )
    if updated_settings is None:
        return jsonify({"message": "Workspace settings not updated"}), 500
    return jsonify(updated_settings), 200


@routes.route("/workspaces/<workspaceId>", methods=["DELETE"])
@jwt_required
def delete_workspace(workspaceId):
    """
    Deletes a workspace
    Returns the workspace that has been deleted thanks to its id.
    ---
    tags:
        - workspace
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace to delete
          required: true
          type: string
    responses:
        '401':
          description: User not found
        '500':
          description: Workspace not deleted
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Workspace'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    deleted_workspace = Workspace.deleteWorkspace(
        userId=user.id, workspaceId=workspaceId
    )
    if deleted_workspace is None:
        return jsonify({"message": "Workspace not deleted"}), 500
    return jsonify(deleted_workspace), 200


def checkImport(data):
    # data = json.load(content)

    if data == "" or not isinstance(data, list):
        return False
    else:
        for module in data:
            if (
                "configurations" not in module
                or "data" not in module
                or "description" not in module
                or "enabled" not in module
                or "name" not in module
            ):
                return False
            else:
                name = module["name"]
                config = module["configurations"]
                data = module["data"]
                enabled = module["enabled"]
                # if not isinstance(enabled, bool):
                #     return False
                if name == "EvaluatorModule":
                    if not isinstance(config, list):
                        return False
                if name == "ContributorsModule":
                    if not isinstance(config, list):
                        return False
    return True
