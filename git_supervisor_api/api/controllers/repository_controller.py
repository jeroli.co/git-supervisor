import json
import re

import requests
from bson import json_util
from flask import jsonify, request
from flask_jwt_extended import jwt_required

from git_supervisor_api.api.config import GITLAB_API_URL, GITHUB_API_URL
from git_supervisor_api.api.controllers import routes
from git_supervisor_api.api.models.repository_model import Repository
from git_supervisor_api.api.models.workspace_model import Workspace
from git_supervisor_api.api.utils import (
    async_request_content,
    make_request_url_gitlab,
    verify_user,
    make_request_url_github,
    call_github_api,
)


@routes.route("/workspaces/<workspaceId>/search/repositories", methods=["GET"])
@jwt_required
def search_repo(workspaceId):
    """
    Searches for a Github/Gitlab repository
    Returns an array with the 5 most relevant repositories. If the search string is an URL, only 1 repository will be sent in the array.
    ---
        tags:
            - repository
        security:
            - oauth: []
        parameters:
            - name: search
              in: query
              description: The search string. Can be a repository name or URL
              required: true
              type: string
        responses:
            'Any but 200':
              description: Error provided by Github/Gitlab
            '200':
              description: OK
              schema:
                type: array
                items:
                  $ref: '#/definitions/Repository'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    if workspace.vcs == "GitLab":
        return search_repo_gitlab()
    elif workspace.vcs == "GitHub":
        return search_repo_github()
    else:
        return jsonify({"message": "VCS not found"}), 404


def search_repo_gitlab():
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_gitlab
    search = request.args.get("search")
    regex = re.compile(
        "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
    )

    if re.match(regex, search):
        repo = "%2F".join(search.split("/")[3:])

        r = requests.get(
            GITLAB_API_URL + "/projects/" + repo + "?access_token=" + accessToken
        )
        if r.status_code != 200:
            return jsonify({}), r.status_code

        repo_model_array = make_repo_model_array([r.json()])
        return jsonify(repo_model_array)
    else:
        r = requests.get(
            GITLAB_API_URL
            + "/projects?access_token="
            + accessToken
            + "&search="
            + search
            + "&per_page=5"
        )
    if r.status_code != 200:
        return jsonify({}), r.status_code
    repo_model_array = make_repo_model_array(r.json())
    return jsonify(repo_model_array), 200


def search_repo_github():
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_github
    search = request.args.get("search")
    regex = re.compile(
        "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
    )
    if re.match(regex, search):
        user_repo = "/".join(search.split("/")[3:])
        repo_search = call_github_api(
            GITHUB_API_URL + "/repos/" + user_repo, access_token=accessToken
        )
        repo_model_array = make_repo_model_array_github([repo_search])
        return jsonify(repo_model_array)
    else:
        r = call_github_api(
            GITHUB_API_URL + "/search/repositories?q=" + search + "&per_page=5",
            access_token=accessToken,
        )

    repo_model_array = create_repo_model_search_from_github(r)
    return jsonify(repo_model_array), 200


@routes.route("/workspaces/<workspaceId>/repositories", methods=["GET"])
@jwt_required
def get_repos(workspaceId):
    """
    Gets all repositories for a Github/Gitlab user
    Returns an array with the 5 most relevant repositories. If the search string is an URL, only 1 repository will be sent in the array.
    ---
        tags:
            - repository
        security:
            - oauth: []
        parameters:
            - name: search
              in: query
              description: The search string. Can be a repository name or URL
              required: true
              type: string
        responses:
            '401':
              description: User not found
            '200':
              description: OK
              schema:
                type: array
                items:
                  $ref: '#/definitions/Repository'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    if workspace.vcs == "GitLab":
        return get_gitlab_repos()
    elif workspace.vcs == "GitHub":
        return get_github_repos()
    else:
        return jsonify({"message": "VCS not found"}), 404


def get_gitlab_repos():

    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_gitlab
    urls = [
        make_request_url_gitlab(
            resource="projects",
            sub_resource="",
            owned="true",
            simple="true",
            access_token=accessToken,
        ),
        make_request_url_gitlab(
            resource="projects",
            sub_resource="",
            starred="true",
            access_token=accessToken,
            simple="true",
        ),
        make_request_url_gitlab(
            resource="projects",
            sub_resource="",
            membership="true",
            access_token=accessToken,
            simple="true",
        ),
    ]
    res = async_request_content(urls)
    repo_owned = res[0]
    repo_starred = res[1]
    repo_member = res[2]

    for repo in repo_owned:
        if repo["id"] in [repo_["id"] for repo_ in repo_member]:
            repo_member.remove(repo)
        if repo["id"] in [repo_["id"] for repo_ in repo_starred]:
            repo_starred.remove(repo)
    for repo in repo_member:
        if repo["id"] in [repo_["id"] for repo_ in repo_starred]:
            repo_starred.remove(repo)

    repo_owned_model_array = make_repo_model_array(repo_owned)
    repo_starred_model_array = make_repo_model_array(repo_starred)
    repo_member_model_array = make_repo_model_array(repo_member)

    return (
        jsonify(
            {
                "owned": repo_owned_model_array,
                "starred": repo_starred_model_array,
                "memberOf": repo_member_model_array,
            }
        ),
        200,
    )


def get_github_repos():
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_github

    repo_owned_url = make_request_url_github(
        resource="user",
        sub_resource="repos",
        type="owner",
    )
    repo_starred_url = make_request_url_github(
        resource="user",
        sub_resource="starred",
    )
    repo_member_url = make_request_url_github(
        resource="user",
        sub_resource="repos",
        type="member",
    )
    repo_owned = call_github_api(repo_owned_url, access_token=accessToken)
    repo_starred = call_github_api(repo_starred_url, access_token=accessToken)
    repo_member = call_github_api(repo_member_url, access_token=accessToken)

    for repo in repo_owned:
        if repo["id"] in [repo_["id"] for repo_ in repo_member]:
            repo_member.remove(repo)
        if repo["id"] in [repo_["id"] for repo_ in repo_starred]:
            repo_starred.remove(repo)
    for repo in repo_member:
        if repo["id"] in [repo_["id"] for repo_ in repo_starred]:
            repo_starred.remove(repo)

    repo_owned_model_array = make_repo_model_array_github(repo_owned)
    repo_starred_model_array = make_repo_model_array_github(repo_starred)
    repo_member_model_array = make_repo_model_array_github(repo_member)

    return (
        jsonify(
            {
                "owned": repo_owned_model_array,
                "starred": repo_starred_model_array,
                "memberOf": repo_member_model_array,
            }
        ),
        200,
    )


def make_repo_model_array(repos):
    repo_model_array = []
    for repo in repos:
        repo_model = create_repo_model_from_gitlab(repo)
        repo_object = Repository(**repo_model)
        repo_model_array.append(repo_object.json())
    return repo_model_array


def make_repo_model_array_github(repos):
    repo_model_array = []
    for repo in repos:
        repo_model = create_repo_model_from_github(repo)
        repo_object = Repository(**repo_model)
        repo_model_array.append(repo_object.json())
    return repo_model_array


def create_repo_model_from_gitlab(repo):
    return {
        "gitId": repo["id"],
        "owner": repo["namespace"]["path"],
        "name": repo["name"],
        "description": repo["description"],
        "userId": repo["namespace"]["id"],
        "created_at": repo["created_at"],
    }


def create_repo_model_from_github(repo):
    return {
        "gitId": repo["id"],
        "owner": repo["owner"]["login"],
        "name": repo["name"],
        "description": repo["description"],
        "userId": repo["owner"]["id"],
        "created_at": repo["created_at"],
    }


def create_repo_model_search_from_github(repo):
    repos = [
        {
            "gitId": repo["items"][i]["id"],
            "owner": repo["items"][i]["owner"]["login"],
            "name": repo["items"][i]["name"],
            "description": repo["items"][i]["description"],
            "userId": repo["items"][i]["owner"]["id"],
            "created_at": repo["items"][i]["created_at"],
        }
        for i in range(0, len(repo["items"]))
    ]

    return repos


@routes.route("/workspaces/<workspaceId>/repositories/<repositoryId>", methods=["GET"])
@jwt_required
def get_repository(workspaceId, repositoryId):
    """
    Gets a repository
    Returns a repository thanks to its id and the id of the workspace which contains the repository.
    ---
    tags:
        - repository
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to get
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository to get
          required: true
          type: string
    responses:
        '401':
          description: User not found
        '404':
          description: Repository not found
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Repository'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    repo = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if repo is None:
        return jsonify({"message": "Repository not found"}), 404
    return jsonify(repo.json()), 200


@routes.route("/workspaces/<workspaceId>/repositories", methods=["POST"])
@jwt_required
def add_repository(workspaceId):
    """
    Adds a new repository to a specified workspace and the logged in user
    Adds the repository sent in body to the workspace specified by its id in path and the logged user. Returns the added repository if successful.
    ---
      tags:
        - repository
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which will contain the repository to add
          required: true
          type: string
        - name: repository
          in: formData
          description: The json representation of the workspace to add
          required: true
          schema:
            $ref: '#/definitions/Workspace'
      responses:
        '401':
          description: User not found
        '500':
          description: Repository not added
        '201':
          description: OK
          schema:
            $ref: '#/definitions/Repository'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    repo = Repository(**json.loads(request.form["repository"]))
    repo_added = Repository.addRepository(
        userId=user.id, workspaceId=workspaceId, repository=repo
    )
    if repo_added is None:
        return jsonify({"message": "Repository not added"}), 500
    return jsonify(repo_added.json()), 201


@routes.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>", methods=["DELETE"]
)
@jwt_required
def delete_repository(workspaceId, repositoryId):
    """
    Deletes a repository
    Deletes the repository specified by its id if it exists in the specified workspace. If successful, returns the repository that has been deleted thanks to its id.
    ---
    tags:
        - repository
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to delete
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository to delete
          required: true
          type: string
    responses:
        '401':
          description: User not found
        '404':
          description: Workspace not found
        '500':
          description: Repository was not deleted
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Repository'
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404
    deleted_repository = Repository.deleteRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if deleted_repository is None:
        return jsonify({"message": "Repository was not deleted"}), 500
    return jsonify(deleted_repository)


@routes.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/branches", methods=["GET"]
)
@jwt_required
def get_branches(workspaceId, repositoryId):
    """
    Gets all branches of a repository
    Returns an array of the branches of a repository contained in a workspace, specified by their id.
    ---
    tags:
        - repository
    security:
        - oauth: []
    parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository
          required: true
          type: string
    responses:
        'Any but 200':
          description: Error provided by Github/Gitlab
        '200':
          description: OK
          schema:
            type: array
            items:
              type: string
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    if workspace.vcs == "GitLab":
        return get_branches_gitlab(workspaceId=workspaceId, repositoryId=repositoryId)
    elif workspace.vcs == "GitHub":
        return get_branches_github(workspaceId=workspaceId, repositoryId=repositoryId)
    else:
        return jsonify({"message": "VCS not found"}), 404


def get_branches_gitlab(workspaceId, repositoryId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_gitlab
    repo = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if repo is None:
        return jsonify({"message": "Repository not found"}), 404
    r = requests.get(
        GITLAB_API_URL + "/projects/" + str(repo.gitId) + "/repository/branches"
        "?access_token=" + accessToken
    )
    if r.status_code != 200:
        return jsonify({}), r.status_code

    branches = r.json()
    for index, branch in enumerate(branches):
        if branch["default"] is True:
            branches.insert(0, branches.pop(index))

    branches = [branch["name"] for branch in branches]
    return jsonify(branches), 200


def get_branches_github(workspaceId, repositoryId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    accessToken = user.access_token_github
    repo = Repository.getRepository(
        userId=user.id, workspaceId=workspaceId, repositoryId=repositoryId
    )
    if repo is None:
        return jsonify({"message": "Repository not found"}), 404

    branches = call_github_api(
        GITHUB_API_URL + "/repos/" + repo.owner + "/" + repo.name + "/branches",
        access_token=accessToken,
    )

    for index, branch in enumerate(branches):
        if branch["name"] == "master":
            branches.insert(0, branches.pop(index))

    branches = [branch["name"] for branch in branches]
    return jsonify(branches), 200
