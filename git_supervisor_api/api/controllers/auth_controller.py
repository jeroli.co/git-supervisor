import os
from flask import current_app, jsonify, request
from flask_jwt_extended import create_access_token
from requests_oauthlib import OAuth2Session

from git_supervisor_api.api.controllers import routes
from git_supervisor_api.api.models.user_model import User, Vcs

REDIRECT_URI = current_app.config.get("REDIRECT_URI") or os.environ.get(
    "REDIRECT_URI", ""
)

"""GITLAB"""

GITLAB_CLIENT_ID = current_app.config.get("GITLAB_CLIENT_ID") or os.environ.get(
    "GITLAB_CLIENT_ID", ""
)
GITLAB_CLIENT_SECRET = current_app.config.get("GITLAB_CLIENT_SECRET") or os.environ.get(
    "GITLAB_CLIENT_SECRET", ""
)

GITLAB_AUTHORIZATION_BASE_URL = "https://gitlab.com/oauth/authorize"
GITLAB_TOKEN_URL = "https://gitlab.com/oauth/token"
GITLAB_USER_URL = "https://gitlab.com/api/v4/user"
GITLAB_USER_NAME = "username"

"""GITHUB"""

GITHUB_CLIENT_ID = current_app.config.get("GITHUB_CLIENT_ID") or os.environ.get(
    "GITHUB_CLIENT_ID", ""
)
GITHUB_CLIENT_SECRET = current_app.config.get("GITHUB_CLIENT_SECRET") or os.environ.get(
    "GITHUB_CLIENT_SECRET", ""
)

GITHUB_AUTHORIZATION_BASE_URL = "https://github.com/login/oauth/authorize"
GITHUB_TOKEN_URL = "https://github.com/login/oauth/access_token"
GITHUB_USER_URL = "https://api.github.com/user"
GITHUB_USER_NAME = "login"

os.environ["OAUTHLIB_RELAX_TOKEN_SCOPE"] = "1"


@routes.route("/gitlab_login", methods=["GET"])
def gitlab_login():
    """
    Returns the Gitlab login adress
    Returns the Gitlab login adress to the front, after signing in, Gitlab will call the callback route.
    ---
    tags:
        - authentication
    responses:
        '200':
          description: OK
          schema:
              type: string
              format: url
              example: https://gitlab.com/users/sign_in
    """

    return login(GITLAB_CLIENT_ID, GITLAB_AUTHORIZATION_BASE_URL, None)


@routes.route("/github_login", methods=["GET"])
def github_login():
    """
    Returns the Github login adress
    Returns the Github login adress to the front, after signing in, Github will call the callback route.
    ---
    tags:
        - authentication
    responses:
        '200':
          description: OK
          schema:
              type: string
              format: url
              example: https://github.com/login
    """
    return login(GITHUB_CLIENT_ID, GITHUB_AUTHORIZATION_BASE_URL, "repo")


def login(client_id, authorization_url, scope):
    oauth = OAuth2Session(client_id=client_id, redirect_uri=REDIRECT_URI, scope=scope)
    authorization_url, state = oauth.authorization_url(authorization_url)

    return jsonify(authorization_url)


@routes.route("/gitlab_callback", methods=["GET"])
def gitlab_callback():
    """
    Returns the jwt token authenticating the user
    Retrieves the token from Gitlab, creates the jwt token, creates the user if doesn't exist, and returns the jwt token.
    ---
    tags:
        - authentication
    produces:
        - application/json
    responses:
        '200':
          description: OK
          schema:
              type: object
              properties:
                accessToken:
                  type: string
                  format: byte
                  example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c

    """
    return callback(
        GITLAB_CLIENT_ID,
        GITLAB_TOKEN_URL,
        GITLAB_CLIENT_SECRET,
        GITLAB_USER_URL,
        GITLAB_USER_NAME,
        Vcs.GITLAB,
        None,
    )


@routes.route("/github_callback", methods=["GET"])
def github_callback():
    """
    Returns the jwt token authenticating the user
    Retrieves the token from Gitlab, creates the jwt token, creates the user if doesn't exist, and returns the jwt token.
    ---
    tags:
        - authentication
    produces:
        - application/json
    responses:
        '200':
          description: OK
          schema:
              type: object
              properties:
                accessToken:
                  type: string
                  format: byte
                  example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c

    """
    return callback(
        GITHUB_CLIENT_ID,
        GITHUB_TOKEN_URL,
        GITHUB_CLIENT_SECRET,
        GITHUB_USER_URL,
        GITHUB_USER_NAME,
        Vcs.GITHUB,
        "repo",
    )


def callback(client_id, token, secret, user_url, user_name, vcs: Vcs, scope):
    oauth = OAuth2Session(client_id=client_id, redirect_uri=REDIRECT_URI, scope=scope)
    token = oauth.fetch_token(
        token,
        client_secret=secret,
        authorization_response=request.url,
    )
    user = oauth.get(user_url).json()
    username = user[user_name]
    created_user = User.getUserByUsername(username)
    if created_user is None:
        created_user = User(username)

    if vcs == Vcs.GITHUB:
        created_user.access_token_github = token["access_token"]
    elif vcs == Vcs.GITLAB:
        created_user.access_token_gitlab = token["access_token"]

    User.saveUser(created_user)

    token = create_access_token(identity=username)
    return jsonify(token=token)
