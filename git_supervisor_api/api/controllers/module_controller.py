import json

import requests
from bs4 import BeautifulSoup, Doctype
from flask import jsonify, request
from flask_jwt_extended.view_decorators import jwt_required

from git_supervisor_api.api.controllers import routes
from git_supervisor_api.api.controllers.repository_controller import get_repository
from git_supervisor_api.api.models.module_model import Module
from git_supervisor_api.api.models.workspace_model import Workspace
from git_supervisor_api.api.modules.basic_module.routes import export_html_basic
from git_supervisor_api.api.modules.contributors_module.routes import (
    export_html_contributors,
)
from git_supervisor_api.api.modules.evaluator_module.routes import export_html_evaluator

from git_supervisor_api.api.utils import verify_user


@routes.route("/modules", methods=["GET"])
@jwt_required
def get_modules():
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    modules = Module.getModules()
    return jsonify(modules), 200


@routes.route("/workspaces/<workspaceId>/modules", methods=["GET"])
@jwt_required
def get_workspace_modules(workspaceId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    workspace_modules = Workspace.getWorkspaceModules(
        userId=user.id, workspaceId=workspaceId
    )
    if workspace_modules is None:
        return jsonify({"message": "No workspace modules"}), 500

    return jsonify(workspace_modules), 200


@routes.route("/workspaces/<workspaceId>/modules/<moduleName>", methods=["GET"])
@jwt_required
def get_workspace_module(workspaceId, moduleName):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    module = Module.getModule(
        userId=user.id, workspaceId=workspaceId, module_name=moduleName
    )
    if module is None:
        return jsonify({"message": "No module configurations "}), 500
    return jsonify(module), 200


@routes.route("/workspaces/<workspaceId>/modules", methods=["POST"])
@jwt_required
def add_module_to_workspace(workspaceId):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    module = json.loads(request.form["module"])
    added_module = Module.addModule(
        userId=user.id, workspaceId=workspaceId, module=module
    )
    if added_module is None:
        return jsonify({"message": "Module not added to workspace"}), 500

    return jsonify(module), 201


@routes.route("/workspaces/<workspaceId>/modules/<moduleName>", methods=["DELETE"])
@jwt_required
def delete_workspace_module(workspaceId, moduleName):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401
    deleted_module = Module.deleteModule(
        userId=user.id, workspaceId=workspaceId, moduleName=moduleName
    )
    if deleted_module is None:
        return jsonify({"message": "Module not deleted in workspace"}), 500

    return jsonify(deleted_module), 200


@routes.route("/workspaces/<workspaceId>/modules/<moduleName>", methods=["PATCH"])
@jwt_required
def update_workspace_module(workspaceId, moduleName):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    enabled = json.loads(request.form["enabled"])
    if not isinstance(enabled, bool):
        return jsonify({"message": "Wrong value for enabled"}), 403

    updated_module = Module.updateModuleEnabled(
        userId=user.id,
        workspaceId=workspaceId,
        moduleName=moduleName,
        enabled=enabled,
    )
    if updated_module is None:
        return jsonify({"message": "Module not updated"}), 500

    return jsonify(updated_module), 200


@routes.route(
    "/workspaces/<workspaceId>/modules/<moduleName>/configurations", methods=["GET"]
)
@jwt_required
def get_module_configurations(workspaceId, moduleName):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    module = Module.getModule(
        userId=user.id, workspaceId=workspaceId, module_name=moduleName
    )
    if module is None:
        return jsonify({"message": "No module configurations "}), 500

    configurations = module["configurations"]

    return jsonify(configurations), 200


@routes.route(
    "/workspaces/<workspaceId>/modules/<moduleName>/configurations", methods=["PUT"]
)
@jwt_required
def update_module_configurations(workspaceId, moduleName):
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    configs = json.loads(request.form["configurations"])
    updated_configurations = Module.updateModuleConfigurations(
        userId=user.id,
        workspaceId=workspaceId,
        module_name=moduleName,
        configs=configs,
    )
    if updated_configurations is None:
        return jsonify({"message": "Module configurations not updated"}), 500

    return jsonify(updated_configurations), 200


def fetch_BasicModule(soup, workspaceId, repositoryId):
    """
    Exports a HTML page of the BasicModule of a repository
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param workspaceId: The id of the workspace which contains the repository to export
    :param repositoryId: The id of the repository to export
    :return: the BeautifulSoup object used for the HTML parsing
    """
    # BasicModule
    response = export_html_basic(workspaceId=workspaceId, repositoryId=repositoryId)[0]
    tempSoup = BeautifulSoup(response, features="html5lib")

    div = tempSoup.body.find("div", id="BasicModule")
    h2 = soup.new_tag("h2")
    h2.string = "BasicModule"
    div.insert(0, h2)
    soup.body.append(div)
    soup.body.append(soup.new_tag("br"))
    return soup


def fetch_ContributorsModule(soup, workspaceId, repositoryId, branches, since, until):
    """
    Exports a HTML page of the ContributorsModule of a repository
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param workspaceId: The id of the workspace which contains the repository to export
    :param repositoryId: The id of the repository to export
    :return: the BeautifulSoup object used for the HTML parsing
    """
    response = export_html_contributors(
        workspaceId=workspaceId,
        repositoryId=repositoryId,
        branches=branches,
        since=since,
        until=until,
    )[0]
    tempSoup = BeautifulSoup(response, features="html5lib")

    div = tempSoup.body.find("div", id="ContributorsModule")
    h2 = soup.new_tag("h2")
    h2.string = "ContributorsModule"
    div.insert(0, h2)
    soup.body.append(div)
    soup.body.append(soup.new_tag("br"))
    return soup


def fetch_EvaluatorModule(soup, workspaceId, repositoryId):
    """
    Exports a HTML page of the EvaluatorModule of a repository
    :param soup: the BeautifulSoup object used for the HTML parsing
    :param workspaceId: The id of the workspace which contains the repository to export
    :param repositoryId: The id of the repository to export
    :return: the BeautifulSoup object used for the HTML parsing
    """
    response = export_html_evaluator(
        workspaceId=workspaceId, repositoryId=repositoryId
    )[0]
    tempSoup = BeautifulSoup(response, features="html5lib")

    div = tempSoup.body.find("div", id="EvaluatorModule")
    h2 = soup.new_tag("h2")
    h2.string = "EvaluatorModule"
    div.insert(0, h2)
    soup.body.append(div)
    soup.body.append(soup.new_tag("br"))
    return soup


@routes.route(
    "/workspaces/<workspaceId>/repositories/<repositoryId>/html", methods=["POST"]
)
@jwt_required
def export_html_global(workspaceId, repositoryId):
    """
    Generate a HTML report of a repository
    Returns a string containing the HTML.
    ---
      tags:
        - repository
      security:
        - oauth: []
      parameters:
        - name: workspaceId
          in: path
          description: The id of the workspace which contains the repository to export
          required: true
          type: string
        - name: repositoryId
          in: path
          description: The id of the repository to export
          required: true
          type: string
      responses:
        '401':
          description: User not found
          '404':
          description: Workspace not found
        '500':
          description : Module export failed
        '200':
          description: OK
          schema:
            type: string
    """
    user = verify_user()
    if user is None:
        return jsonify({"message": "User not found"}), 401

    workspace = Workspace.getWorkspace(userId=user.id, workspaceId=workspaceId)
    if workspace is None:
        return jsonify({"message": "Workspace not found"}), 404

    # URL
    apiUrl = request.host_url

    # Token
    username = user.username
    token = user.access_token_gitlab
    res = requests.get(
        url=apiUrl + "get_token", data={"username": username, "token": token}
    ).json()
    accessToken = res.get("token")

    headers = {"Authorization": "Bearer " + accessToken}

    # Form-data
    since = request.form.get("since", None)
    until = request.form.get("until", None)
    branches = request.form.getlist("branches", None)
    modules = request.form.getlist("modules", None)

    repo = get_repository(workspaceId, repositoryId)[0].get_json()

    soup = BeautifulSoup("", features="html5lib")

    doctype = Doctype("html")
    soup.insert(0, doctype)

    soup.html["lang"] = "en"

    meta = soup.new_tag("meta", charset="utf-8")
    soup.head.append(meta)

    style = soup.new_tag("style")
    style.string = (
        "html * { font-family: Arial, Helvetica, sans-serif; } .module h3 { margin-top: 0; text-align: "
        "center; border-style: solid; border-color: #1F335B; /* display: inline-block; */ border-width: "
        "1px; /* border-radius: 4px 4px 4px 4px; */ color: white; background-color: #1F335B; } "
        ".repoDescription { text-align: center; margin: 25px; } .module { margin: 10px; border-style: "
        "solid; border-color: #1F335B; border-width: thick; border-radius: 4px 4px 4px 4px; } .userName { "
        "color: #1F335B; background-color: white; margin: auto; padding: 5px; margin: 5px; border-style: "
        "solid; border-width: 1px; border-radius: 4px 4px 4px 4px; font-weight: bold; align-items: left; } "
        ".user { margin: auto; } .userUrl { font-size: small; color: grey; } .user td { text-align: "
        "center; } .avatar { border-radius: 50%; } .userFulfilled td { min-width: 250px; } .userCriteria "
        "td { display: inline-block; min-width: unset; max-width: unset; width: unset; } .description { "
        "text-align: center; } /* canvas { align-items: center; } */ .module h2, h1 { text-align: center; "
        "color: #1F335B; } /* TABLE COMMITS */ .customTables { margin: auto; table-layout: auto; "
        "font-family: arial; border-collapse: separate; border-radius: 6px 6px 6px 6px; border: 1px solid "
        "#1F335B; border-spacing: 0; /* margin: 10px; */ max-width: 1000px; margin-bottom: 40px; "
        "min-width: 50%; } .customTables .left { text-align: left; } .customTables .right { text-align: "
        "right; } .customTables tr:first-child td:first-child { border-top-left-radius: 5px; } "
        ".customTables tr:first-child td:last-child { border-top-right-radius: 5px; } .customTables "
        "tr:last-child td:first-child { border-bottom-left-radius: 5px; } .customTables tr:last-child "
        "td:last-child { border-bottom-right-radius: 5px; } /* optional setting to pad the table cells */ "
        ".customTables th, td { padding: 10px 10px 10px 10px; } /* optional setting to set the background "
        "color and font color of the first row - behave like a header */ .customTables th { "
        "background-color: #1F335B; color: #FFF; } .customTables tr:nth-child(even) { background-color: "
        "#e6f3ff; } .customTables tr:nth-child(odd) { background-color: #ffffff; } .customTables "
        "tr:only-child { background-color: transparent; } "
    )
    soup.head.append(style)

    title = soup.new_tag("title")
    title.string = repo["name"] + " HTML Report"

    h1 = soup.new_tag("h1")
    h1.string = repo["name"] + " HTML Report"
    soup.body.append(h1)

    p = soup.new_tag("p")
    p["class"] = "repoDescription"
    p.string = repo["description"]
    soup.body.append(p)

    # form = {'since': since, 'until': until, 'branches': branches, 'token': accessToken}

    modules.sort()

    ######################################################## Fonctionnel
    # for module in modules:
    #     url = apiUrl + 'workspaces/' + workspaceId + '/repositories/' + repositoryId + '/modules/' + module + '/html'
    #     response = requests.Kpost(url=url, headers=headers, data=form).text
    #     if response is None:
    #         return jsonify({"message": "Request failed (" + module + ")"}), 512
    #     tempSoup = BeautifulSoup(response, features="html5lib")
    #     if tempSoup is None:
    #         return jsonify({"message": "Parsing failed (" + module + ")"}), 513
    #     div = tempSoup.body.findAll("div", {'class': 'module'})[0]
    #     h2 = soup.new_tag('h2')
    #     h2.string = module
    #     div.insert(0, h2)
    #     soup.body.append(div)
    #     soup.body.append(soup.new_tag('br'))
    #     # soup.body.append(soup.new_tag('hr'))

    ######################################################### To refactor

    if "BasicModule" in modules:
        soup = fetch_BasicModule(
            soup=soup, workspaceId=workspaceId, repositoryId=repositoryId
        )
        if soup is None:
            return jsonify({"message": "BasicModule export failed"}), 500

    if "ContributorsModule" in modules:
        soup = fetch_ContributorsModule(
            soup=soup,
            workspaceId=workspaceId,
            repositoryId=repositoryId,
            branches=branches,
            since=since,
            until=until,
        )
        if soup is None:
            return jsonify({"message": "ContributorsModule export failed"}), 500

    if "EvaluatorModule" in modules:
        soup = fetch_EvaluatorModule(
            soup=soup, workspaceId=workspaceId, repositoryId=repositoryId
        )

    #########################################################

    for s in soup.find_all("script"):
        soup.body.insert_after(s)

    html = str(soup)

    ########################################################

    return jsonify(html), 200
