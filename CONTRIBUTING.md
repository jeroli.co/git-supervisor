# Contributing

## Submitting a Pull Request

1.  Open an issue (with a concise and explicit title, a clear description, the steps to reproduce if it is a ~bug, the features to add if it is an ~enhancement).
2.  [Fork the repository](https://help.github.com/articles/fork-a-repo).
3.  [Create a topic branch](https://help.github.com/articles/fork-a-repo#create-branches)  directly from the issue. If the branch name exceeds 25 characters, summarize it.
4.  Set up your development environment. (See [Getting Started](https://gitlab.com/jeroli.co/git-supervisor/wikis/%5BDEV%5D-Getting-Started))
5.  Add tests for your unimplemented feature or bug fix. (See [Writing and Executing Tests](https://gitlab.com/jeroli.co/git-supervisor/blob/106-ecrire-un-contributung/CONTRIBUTING.md#writing-and-executing-tests))
6.  Run `python -m pytest git_supervisor_api/` to run the tests and verify they fail.
7.  Implement your feature or bug fix.
8.  Add documentation **(100% of your changes must be documented)** and be sure the documentation build passes. (See [Writing documentation](https://gitlab.com/jeroli.co/git-supervisor/blob/106-ecrire-un-contributung/CONTRIBUTING.md#writing-documentation))
9.  Run `python -m pytest git_supervisor_api/` to run the tests and verify they pass.   
10.  Add, commit, and push your changes.
11.  [Submit a pull request](https://help.github.com/articles/using-pull-requests).


### Writing and Executing Tests

For writing tests, there are 3 packages :
- *tests.functional* : for testing back-end endpoints
- *tests.unit* : for testing model classes
- *tests.modules* : for testing modules endpoints

When creating a new module, tests covering at least 80% of the added changes must be written in the file named *test_<module_name>.py* under *tests.modules* package

We are using pytest. There are 3 fixtures available in the *conftest.py* :
- *login* : logins to Gitlab and returns the jwt token
- *client* : runs and returns a testing flask app
- *init_db* : initializes data in the test database

Here is an example of testing *my_module* :
```python
def test_my_module_success(init_db, login, client):
    response = client.get("/myresource/3452",
                          headers={"Authorization": "Bearer " + login})
    assert response.status_code == 200
```

To execute all tests, run : 

    python -m pytest git_supervisor_api/

To execute all modules tests, run :

    python -m pytest git_supervisor_api/api/tests/modules/

To execute tests on a single module, run :

    python -m pytest git_supervisor_api/api/tests/modules/test_my_module.py

And finally, to execute one test method, run :

    python -m pytest git_supervisor_api/api/tests/modules/test_my_module.py::test_my_module_success

Before submitting a pull request, the changes you made will have to be at least 80% covered. You can know the coverage by right clicking on the tests concerning your changes and click on `Run 'pytest test_my_module...' with Coverage`.

### Writing Documentation

For every endpoint, a docstring for [flasgger](https://github.com/rochacbruno/flasgger) must be written :

```
"""
A brief sentence about the module
A more detailed description concerning the module, including special behaviours.
---
tags:
    - module
security:
    - oauth: []
parameters:
    - name: workspaceId
        in: path
        description: The id of the workspace in which the configurations are updated
        required: true
        type: integer
        format: int32
responses:
    '404':
        description: Module not found
    '200':
        description: OK
        type: string
"""
```

In the package *git_supervisor_api*, a file named *template.json* describes the model classes under JSON format and following [Swagger](https://swagger.io/docs/specification/basic-structure/) specifications.

The Web Service documentation will be generated when the server is launched and it will be available at this URL :

`https://<server_adress>/apidocs` so for the deployed application : https://git-supervisor-api.herokuapp.com/apidocs

For internal functions, docstring must be written as following :

```
""" Text describing the function

Args:
    parameter (type): its description

Returns:
    Type: a description about the function return
"""
```


# Styleguide

Usage of [this code formatter](https://github.com/ambv/black?fbclid=IwAR0FeAYI15nN4cKkHzTBS1gtgwcK1vQbmIKFcbtBHuzHUhVNr7Wi0ch0MRA) is strongly recommended to keep code clear to keep the code clear and consistent throughout the application.

For the code, please follow [PEP 20](https://www.python.org/dev/peps/pep-0020/) and [PEP 8](https://www.python.org/dev/peps/pep-0008/).

For the docstring, follow [Google styleguide](https://google.github.io/styleguide/pyguide.html#383-functions-and-methods).
